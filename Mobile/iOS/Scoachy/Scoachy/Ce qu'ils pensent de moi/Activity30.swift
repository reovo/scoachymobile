//
//  Activity30.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 17/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import ContactsUI
import MessageUI

class Activity30: UIViewController, CNContactPickerDelegate, MFMessageComposeViewControllerDelegate, UITableViewDataSource, UITableViewDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtEmail: UITextField!
    
    //var arrayName = ["Erick Yetes", "Greg Ferguson"]
    //var userslist = [(Name: String, Phone: String)]()
    
    //var arrayFriendUserId = [String]()
    //var arrayPhone = [String]()
    var arrayEmail = [String]()
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

    var flag = 0
    var refresher: UIRefreshControl!
    
    var arrayInviteStatus = [String]()
    var arrayProfilePicture: [UIImage] = []
    
    var arrayInvitedOn = [String]()
    var arrayDate = [String]()
    
    //var arrayProfilePicture = [UIImage(named:"PICTOS_SCOACHY-22"), UIImage(named:"PICTOS_SCOACHY-22")]
    
    var arrayInvitedUser = [(name: String, phone: String, status: String)]()
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        fetchContacts()
        refresher = UIRefreshControl()
        refresher.attributedTitle  = NSAttributedString(string: "Pull to refresh")
        refresher.addTarget(self, action: #selector(Activity30.fetchContacts), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresher)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    @IBAction func closeButton(_ sender: Any) {
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(2, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
    }
    
    @objc func fetchContacts () {
        
        guard let url = URL(string: "http://192.168.6.57:8088/invite/list/\((usersprofileid)!)") else { return }
        
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, _, _) in
            guard let data = data else { return }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                let users = try! JSONDecoder().decode(User.self, from: data)
                print(users)
                

                DispatchQueue.main.async(execute: {
                    
                    self.arrayEmail.removeAll()
                    self.arrayDate.removeAll()
                    self.arrayInviteStatus.removeAll()
                    self.arrayInvitedOn.removeAll()
                    self.arrayProfilePicture.removeAll()
                    
                    for user in (users.ContactsDto)! {
                        
                        self.arrayEmail.append(user.Email!)
                        self.arrayInviteStatus.append(user.InviteStatus!)
                        self.arrayInvitedOn.append(user.InvitedOn!)
                        
                        
                    }
                    
                    for item in self.arrayInviteStatus {
                        
                        if item == "accepted"{
                            self.arrayProfilePicture.append(UIImage(named: "PICTOS_SCOACHY-22.png")!)
                        }
                            
                        else {
                            self.arrayProfilePicture.append(UIImage(named: "PICTOS_SCOACHY-24.png")!)
                        }
                    }
                    
                    for item in self.arrayInvitedOn{
                        
                        let dateFormatterGet = DateFormatter()
                        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        let dateFormatterPrint = DateFormatter()
                        dateFormatterPrint.dateFormat = "yyyy/MM/dd"
                        
                        if let date = dateFormatterGet.date(from: item){
                            //print(dateFormatterPrint.string(from: date))
                            self.arrayDate.append(dateFormatterPrint.string(from: date))
                        }
                        else {
                            print("There was an error decoding the string")
                        }
                        
                    }
                    
                    
                    
                    self.tableView.reloadData()
                    self.refresher.endRefreshing()
                    
                })
                
            }catch {}
        }
        
        task.resume()
    
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    
    
    @IBAction func contactList(_ sender: Any) {
        
        let entityType = CNEntityType.contacts
        let authStatus = CNContactStore.authorizationStatus(for: entityType)
        
        if authStatus == CNAuthorizationStatus.notDetermined{
            
            let contactStore = CNContactStore.init()
            contactStore.requestAccess(for: entityType, completionHandler: { (success, nil) in
                
                if success {
                    
                    self.openContacts()
                }
                    
                else {
                    
                    print("Not authorised")
                }
            })
            
        }
            
        else if authStatus == CNAuthorizationStatus.authorized{
            
            self.openContacts()
            
        }
        
    }
    
    func openContacts(){
        
        let contactPicker = CNContactPickerViewController.init()
        contactPicker.delegate = self
        self.present(contactPicker, animated: true, completion: nil)
        
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        
        picker.dismiss(animated: true) {
            
            self.tableView.reloadData()
            
        }
        
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        if (contact.emailAddresses.count) > 0 {
            
            flag = 0
            
            var userEmail = contact.emailAddresses[0].value
            print("email: \(userEmail)")
            
            //Send Invite to User
            
            
            guard let url = URL(string: "http://192.168.6.57:8088/invite/send/") else { return }
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let parameters: [String: Any] = [
            "UserId": (usersprofileid)!,
            "ContactsDto": [
                [
                    "FirstName": "",
                    "LastName": "",
                    "Phone": "",
                    "Email": userEmail,
                    "UserStatus": "15911129-33ee-11e7-9426-c03fd56ee88c",
                    "PhonePrefix": "+230"
                    ]
                ]
            ]
            
            do {
                let jsonBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                request.httpBody = jsonBody
                } catch {}
            
                let session = URLSession.shared
                let task = session.dataTask(with: request) { (data, _, _) in
                guard let data = data else { return }
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
            
                    } catch {}
                    }
            
            task.resume()
        }
        
        else {
            
            flag = 1
        }
    
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        // Check the result or perform other tasks.
        
        // Dismiss the message compose view controller.
        controller.dismiss(animated: true, completion: nil)
        
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrayEmail.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell30
        
        //cell.profilePicture.image = arrayInviteStatus[indexPath.row]
        cell.name.text = arrayEmail[indexPath.row]
        cell.userStatus.image = arrayProfilePicture[indexPath.row]
        cell.invitedDate.text = arrayDate[indexPath.row]

        
        return cell
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if flag == 1 {
            
            flag = 0
            let alertController = UIAlertController(title: "Scoachy", message: "Ce contact n'a pas d'adresse mail", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
        }
    
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z._-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
     @IBAction func addContact(_ sender: Any) {
        
        if txtEmail.text == "" {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez remplir tous les champs", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
        }
            
        else {
            
            let providedEmailAddress = txtEmail.text
            
            let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
            
            if isEmailAddressValid
            {
                guard let url = URL(string: "http://192.168.6.57:8088/invite/send/") else { return }
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                
                let parameters: [String: Any] = [
                    "UserId": (usersprofileid)!,
                    "ContactsDto": [
                        [
                            "FirstName": "",
                            "LastName": "",
                            "Phone": "",
                            "Email": txtEmail.text!,
                            "UserStatus": "15911129-33ee-11e7-9426-c03fd56ee88c",
                            "PhonePrefix": "+230"
                        ]
                    ]
                ]
                
                do {
                    let jsonBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                    request.httpBody = jsonBody
                } catch {}
                
                let session = URLSession.shared
                let task = session.dataTask(with: request) { (data, _, _) in
                    guard let data = data else { return }
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print(json)
                        
                    } catch {}
                }
                
                txtEmail.text = nil
                
                task.resume()
            }
            
            else {
                let alertController = UIAlertController(title: "Scoachy", message: "Veuillez entrer une adresse email valide", preferredStyle: . alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                
                present(alertController, animated: true, completion: nil)
                }
            
            
        }

        
    }
    
    
        
        
        
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
