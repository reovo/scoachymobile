//
//  Activity37.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 20/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity37: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    @IBOutlet var button4: UIButton!
    
    var selonMoi = [String] ()
    var selonEux = [String] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1900)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        performSegue(withIdentifier: "next4", sender: self)
    }
    
    @IBAction func rightSwipe(_ sender: UISwipeGestureRecognizer) {
        performSegue(withIdentifier: "back3", sender: self)
    }
    
    @IBAction func closeButton(_ sender: Any) {
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(2, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        var userDefaults = UserDefaults.standard
        var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            let stmt1 = try db.run("select r.answerId as answerId, a.description as description FROM answerresult r, answer a where r.userId = '\((usersprofileid)!)' and answerId = a.id and r.questionId = '83ff34c3-60b6-11e7-a7e0-484d7ee0cd26'")
            
            let stmt2 = try db.run("SELECT  r.answerId AS answerId, r.questionId AS questionId ,q.question AS QuestionName,a.description AS Answer,q.ratingtypeId as ratingtypeId, COUNT(a.id) AS cntresult FROM  resultsummary r, allquestion_other q, answer a WHERE r.questionId = q.id AND AnswerId  = a.id AND  r.userId = '\((usersprofileid)!)' AND q.ratingTypeId ='7de83d99-5cb0-11e7-a11e-7a791928939c2' GROUP BY answerId ORDER BY cntresult DESC")
            
            for row in stmt1 {
                
                let answer = row[1] as! String
                selonMoi.append(answer)
            }
            
            for row2 in stmt2 {
                
                let answer1 = row2[4] as! String
                selonEux.append(answer1)
            }
        }
        
        catch {
            print("Connection to database failed")
        }
        
        //
        var selonMoiCount = selonMoi.count
        
        if (selonMoiCount == 1){
            var text = selonMoi[0]
            button1.setTitle(text, for: .normal)
        }
        //
        var selonEuxCount = selonEux.count
        
        if (selonEuxCount == 1){
            var text1 = selonEux[0]
            button2.setTitle(text1, for: .normal)
        }
        
        if (selonEuxCount == 2){
            var text1 = selonEux[0]
            var text2 = selonEux[1]
            button2.setTitle(text1, for: .normal)
            button3.setTitle(text2, for: .normal)
        }
        
        if (selonEuxCount == 3){
            var text1 = selonEux[0]
            var text2 = selonEux[1]
            var text3 = selonEux[2]
            button2.setTitle(text1, for: .normal)
            button3.setTitle(text2, for: .normal)
            button4.setTitle(text3, for: .normal)
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
