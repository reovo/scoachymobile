//
//  Activity34.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 18/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity34: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    @IBOutlet var button4: UIButton!
    @IBOutlet var button5: UIButton!
    @IBOutlet var button6: UIButton!
    @IBOutlet var button7: UIButton!
    @IBOutlet var button8: UIButton!
    
    @IBOutlet var button9: UIButton!
    @IBOutlet var button10: UIButton!
    @IBOutlet var button11: UIButton!
    @IBOutlet var button12: UIButton!
    @IBOutlet var button13: UIButton!
    @IBOutlet var button14: UIButton!
    @IBOutlet var button15: UIButton!
    @IBOutlet var button16: UIButton!
    
    @IBOutlet var button17: UIButton!
    @IBOutlet var button18: UIButton!
    @IBOutlet var button19: UIButton!
    @IBOutlet var button20: UIButton!
    @IBOutlet var button21: UIButton!
    @IBOutlet var button22: UIButton!
    @IBOutlet var button23: UIButton!
    @IBOutlet var button24: UIButton!
    
    @IBOutlet var button25: UIButton!
    @IBOutlet var button26: UIButton!
    @IBOutlet var button27: UIButton!
    @IBOutlet var button28: UIButton!
    @IBOutlet var button29: UIButton!
    @IBOutlet var button30: UIButton!
    @IBOutlet var button31: UIButton!
    @IBOutlet var button32: UIButton!
    
    var fortSelonMoi = [String]()
    var fortSelonEux = [String]()
    var faiblesSelonMoi = [String]()
    var faiblesSelonEux = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 4500)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        
        performSegue(withIdentifier: "next1", sender: self)
    }
    
    @IBAction func rightSwipe(_ sender: UISwipeGestureRecognizer) {
        
        performSegue(withIdentifier: "back0", sender: self)
    }
    
    @IBAction func closeButton(_ sender: Any) {
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(2, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        var userDefaults = UserDefaults.standard
        var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)

            let stmt1 = try db.run("select  r.questionId as QuestionId, r.userId as User ,q.ratingtypeId as ratingtypeId, q.description as description , q.question as questions, r.answerId as Answerid, a.description as ansdesc , a.weight as weight from  answerresult r,answer a,allquestion q where  r.questionId = q.id and r.answerId = a.id and r.userId ='\((usersprofileid)!)' and q.attributeId = '24551cf0-5cb4-11e7-a11e-7a791928939c' and q.ratingtypeId ='529510c6-233d-11e7-b36e-c03fd56ee88c' group by questionId order by a.weight  DESC, description ASC LIMIT 8")
            
            let stmt2 = try db.run("SELECT r.answerId AS answerId ,r.questionId AS questionId,  q.ratingtypeId AS ratingtypeId , q.description AS description ,answer.weight ,AVG(answer.weight) AS Average FROM resultsummary r, allquestion_other q ,answer WHERE r.questionId = q.id AND r.answerId = answer.id AND r.userid ='\((usersprofileid)!)' AND q.ratingtypeId = '529510c6-233d-11e7-b36e-c03fd56ee88c' AND q.attributeId ='91c2ee2e-5c94-11e7-a11e-7a791928939c' GROUP BY questionId ORDER BY  Average DESC ,description ASC LIMIT 8")
            
            
            let stmt3 = try db.run("select  r.questionId as QuestionId, r.userId as User ,q.ratingtypeId as ratingtypeId, q.description as description , q.question as questions, r.answerId as Answerid, a.description as ansdesc , a.weight as weight from  answerresult r,answer a,allquestion q where  r.questionId = q.id and r.answerId = a.id and r.userId ='\((usersprofileid)!)' and q.attributeId = '29c8ffa7-5cb4-11e7-a11e-7a791928939c' and q.ratingtypeId ='529510c6-233d-11e7-b36e-c03fd56ee88c' group by questionId order by a.weight  DESC, description ASC LIMIT 8")
            
            
            let stmt4 = try db.run("SELECT r.answerId AS answerId ,r.questionId AS questionId,  q.ratingtypeId AS ratingtypeId , q.description AS description ,answer.weight ,AVG(answer.weight) AS Average FROM resultsummary r, allquestion_other q ,answer WHERE r.questionId = q.id AND r.answerId = answer.id AND r.userid ='\((usersprofileid)!)' AND q.ratingtypeId = '529510c6-233d-11e7-b36e-c03fd56ee88c' AND q.attributeId ='6001c111-5cb3-11e7-a11e-7a791928939c' GROUP BY questionId ORDER BY  Average DESC ,description ASC LIMIT 8")
 
            for row in stmt1 {
                
                let answer = row[3] as! String
                fortSelonMoi.append(answer)
            }
            
            for row2 in stmt2 {
                
                let answer1 = row2[3] as! String
                fortSelonEux.append(answer1)
            }
            
            for row3 in stmt3 {
                
                let answer2 = row3[3] as! String
                faiblesSelonMoi.append(answer2)
            }
            
            for row4 in stmt4 {
                
                let answer3 = row4[3] as! String
                faiblesSelonEux.append(answer3)
            }
            
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
        //Set Text

        var fortSelonMoiCount = fortSelonMoi.count
        
        if (fortSelonMoiCount == 1){
            var text = fortSelonMoi[0]
            button1.setTitle(text, for: .normal)
        }
        
        if (fortSelonMoiCount == 2){
            var text = fortSelonMoi[0]
            var text1 = fortSelonMoi[1]
            button1.setTitle(text, for: .normal)
            button2.setTitle(text1, for: .normal)
        }
        
        if (fortSelonMoiCount == 3){
            var text = fortSelonMoi[0]
            var text1 = fortSelonMoi[1]
            var text2 = fortSelonMoi[2]
            button1.setTitle(text, for: .normal)
            button2.setTitle(text1, for: .normal)
            button3.setTitle(text2, for: .normal)
        }
        
        if (fortSelonMoiCount == 4){
            var text = fortSelonMoi[0]
            var text1 = fortSelonMoi[1]
            var text2 = fortSelonMoi[2]
            var text3 = fortSelonMoi[3]
            button1.setTitle(text, for: .normal)
            button2.setTitle(text1, for: .normal)
            button3.setTitle(text2, for: .normal)
            button4.setTitle(text3, for: .normal)
        }
        
        if (fortSelonMoiCount == 5){
            var text = fortSelonMoi[0]
            var text1 = fortSelonMoi[1]
            var text2 = fortSelonMoi[2]
            var text3 = fortSelonMoi[3]
            var text4 = fortSelonMoi[4]
            button1.setTitle(text, for: .normal)
            button2.setTitle(text1, for: .normal)
            button3.setTitle(text2, for: .normal)
            button4.setTitle(text3, for: .normal)
            button5.setTitle(text4, for: .normal)
        }
        
        if (fortSelonMoiCount == 6){
            var text = fortSelonMoi[0]
            var text1 = fortSelonMoi[1]
            var text2 = fortSelonMoi[2]
            var text3 = fortSelonMoi[3]
            var text4 = fortSelonMoi[4]
            var text5 = fortSelonMoi[5]
            button1.setTitle(text, for: .normal)
            button2.setTitle(text1, for: .normal)
            button3.setTitle(text2, for: .normal)
            button4.setTitle(text3, for: .normal)
            button5.setTitle(text4, for: .normal)
            button6.setTitle(text5, for: .normal)
        }
        
        if (fortSelonMoiCount == 7){
            var text = fortSelonMoi[0]
            var text1 = fortSelonMoi[1]
            var text2 = fortSelonMoi[2]
            var text3 = fortSelonMoi[3]
            var text4 = fortSelonMoi[4]
            var text5 = fortSelonMoi[5]
            var text6 = fortSelonMoi[6]
            button1.setTitle(text, for: .normal)
            button2.setTitle(text1, for: .normal)
            button3.setTitle(text2, for: .normal)
            button4.setTitle(text3, for: .normal)
            button5.setTitle(text4, for: .normal)
            button6.setTitle(text5, for: .normal)
            button7.setTitle(text6, for: .normal)
        }
        
        if (fortSelonMoiCount == 8){
            var text = fortSelonMoi[0]
            var text1 = fortSelonMoi[1]
            var text2 = fortSelonMoi[2]
            var text3 = fortSelonMoi[3]
            var text4 = fortSelonMoi[4]
            var text5 = fortSelonMoi[5]
            var text6 = fortSelonMoi[6]
            var text7 = fortSelonMoi[7]
            button1.setTitle(text, for: .normal)
            button2.setTitle(text1, for: .normal)
            button3.setTitle(text2, for: .normal)
            button4.setTitle(text3, for: .normal)
            button5.setTitle(text4, for: .normal)
            button6.setTitle(text5, for: .normal)
            button7.setTitle(text6, for: .normal)
            button8.setTitle(text7, for: .normal)
        }
        
        //
        var fortSelonEuxCount = fortSelonEux.count
        
        if (fortSelonEuxCount == 1){
            var text8 = fortSelonEux[0]
            button9.setTitle(text8, for: .normal)
        }
        
        if (fortSelonEuxCount == 2){
            var text8 = fortSelonEux[0]
            var text9 = fortSelonEux[1]
            button9.setTitle(text8, for: .normal)
            button10.setTitle(text9, for: .normal)
        }
        
        if (fortSelonEuxCount == 3){
            var text8 = fortSelonEux[0]
            var text9 = fortSelonEux[1]
            var text10 = fortSelonEux[2]
            button9.setTitle(text8, for: .normal)
            button10.setTitle(text9, for: .normal)
            button11.setTitle(text10, for: .normal)
        }
        
        if (fortSelonEuxCount == 4){
            var text8 = fortSelonEux[0]
            var text9 = fortSelonEux[1]
            var text10 = fortSelonEux[2]
            var text11 = fortSelonEux[3]
            button9.setTitle(text8, for: .normal)
            button10.setTitle(text9, for: .normal)
            button11.setTitle(text10, for: .normal)
            button12.setTitle(text11, for: .normal)
        }
        
        if (fortSelonEuxCount == 5){
            var text8 = fortSelonEux[0]
            var text9 = fortSelonEux[1]
            var text10 = fortSelonEux[2]
            var text11 = fortSelonEux[3]
            var text12 = fortSelonEux[4]
            button9.setTitle(text8, for: .normal)
            button10.setTitle(text9, for: .normal)
            button11.setTitle(text10, for: .normal)
            button12.setTitle(text11, for: .normal)
            button13.setTitle(text12, for: .normal)
        }
        
        if (fortSelonEuxCount == 6){
            var text8 = fortSelonEux[0]
            var text9 = fortSelonEux[1]
            var text10 = fortSelonEux[2]
            var text11 = fortSelonEux[3]
            var text12 = fortSelonEux[4]
            var text13 = fortSelonEux[5]
            button9.setTitle(text8, for: .normal)
            button10.setTitle(text9, for: .normal)
            button11.setTitle(text10, for: .normal)
            button12.setTitle(text11, for: .normal)
            button13.setTitle(text12, for: .normal)
            button14.setTitle(text13, for: .normal)
        }
        
        if (fortSelonEuxCount == 7){
            var text8 = fortSelonEux[0]
            var text9 = fortSelonEux[1]
            var text10 = fortSelonEux[2]
            var text11 = fortSelonEux[3]
            var text12 = fortSelonEux[4]
            var text13 = fortSelonEux[5]
            var text14 = fortSelonEux[6]
            button9.setTitle(text8, for: .normal)
            button10.setTitle(text9, for: .normal)
            button11.setTitle(text10, for: .normal)
            button12.setTitle(text11, for: .normal)
            button13.setTitle(text12, for: .normal)
            button14.setTitle(text13, for: .normal)
            button15.setTitle(text14, for: .normal)
        }
        
        if (fortSelonEuxCount == 8){
            var text8 = fortSelonEux[0]
            var text9 = fortSelonEux[1]
            var text10 = fortSelonEux[2]
            var text11 = fortSelonEux[3]
            var text12 = fortSelonEux[4]
            var text13 = fortSelonEux[5]
            var text14 = fortSelonEux[6]
            var text15 = fortSelonEux[7]
            button9.setTitle(text8, for: .normal)
            button10.setTitle(text9, for: .normal)
            button11.setTitle(text10, for: .normal)
            button12.setTitle(text11, for: .normal)
            button13.setTitle(text12, for: .normal)
            button14.setTitle(text13, for: .normal)
            button15.setTitle(text14, for: .normal)
            button16.setTitle(text15, for: .normal)
        }
        
        //
        
        var faiblesSelonMoiCount = faiblesSelonMoi.count
        
        if (faiblesSelonMoiCount == 1){
            var text16 = faiblesSelonMoi[0]
            button17.setTitle(text16, for: .normal)
        }
        
        if (faiblesSelonMoiCount == 2){
            var text16 = faiblesSelonMoi[0]
            var text17 = faiblesSelonMoi[1]
            button17.setTitle(text16, for: .normal)
            button18.setTitle(text17, for: .normal)
        }
        
        if (faiblesSelonMoiCount == 3){
            var text16 = faiblesSelonMoi[0]
            var text17 = faiblesSelonMoi[1]
            var text18 = faiblesSelonMoi[2]
            button17.setTitle(text16, for: .normal)
            button18.setTitle(text17, for: .normal)
            button19.setTitle(text18, for: .normal)
        }
        
        if (faiblesSelonMoiCount == 4){
            var text16 = faiblesSelonMoi[0]
            var text17 = faiblesSelonMoi[1]
            var text18 = faiblesSelonMoi[2]
            var text19 = faiblesSelonMoi[3]
            button17.setTitle(text16, for: .normal)
            button18.setTitle(text17, for: .normal)
            button19.setTitle(text18, for: .normal)
            button20.setTitle(text19, for: .normal)
        }
        
        if (faiblesSelonMoiCount == 5){
            var text16 = faiblesSelonMoi[0]
            var text17 = faiblesSelonMoi[1]
            var text18 = faiblesSelonMoi[2]
            var text19 = faiblesSelonMoi[3]
            var text20 = faiblesSelonMoi[4]
            button17.setTitle(text16, for: .normal)
            button18.setTitle(text17, for: .normal)
            button19.setTitle(text18, for: .normal)
            button20.setTitle(text19, for: .normal)
            button21.setTitle(text20, for: .normal)
        }
        
        if (faiblesSelonMoiCount == 6){
            var text16 = faiblesSelonMoi[0]
            var text17 = faiblesSelonMoi[1]
            var text18 = faiblesSelonMoi[2]
            var text19 = faiblesSelonMoi[3]
            var text20 = faiblesSelonMoi[4]
            var text21 = faiblesSelonMoi[5]
            button17.setTitle(text16, for: .normal)
            button18.setTitle(text17, for: .normal)
            button19.setTitle(text18, for: .normal)
            button20.setTitle(text19, for: .normal)
            button21.setTitle(text20, for: .normal)
            button22.setTitle(text21, for: .normal)
        }
        
        if (faiblesSelonMoiCount == 7){
            var text16 = faiblesSelonMoi[0]
            var text17 = faiblesSelonMoi[1]
            var text18 = faiblesSelonMoi[2]
            var text19 = faiblesSelonMoi[3]
            var text20 = faiblesSelonMoi[4]
            var text21 = faiblesSelonMoi[5]
            var text22 = faiblesSelonMoi[6]
            button17.setTitle(text16, for: .normal)
            button18.setTitle(text17, for: .normal)
            button19.setTitle(text18, for: .normal)
            button20.setTitle(text19, for: .normal)
            button21.setTitle(text20, for: .normal)
            button22.setTitle(text21, for: .normal)
            button23.setTitle(text22, for: .normal)
        }
        
        if (faiblesSelonMoiCount == 8){
            var text16 = faiblesSelonMoi[0]
            var text17 = faiblesSelonMoi[1]
            var text18 = faiblesSelonMoi[2]
            var text19 = faiblesSelonMoi[3]
            var text20 = faiblesSelonMoi[4]
            var text21 = faiblesSelonMoi[5]
            var text22 = faiblesSelonMoi[6]
            var text23 = faiblesSelonMoi[7]
            button17.setTitle(text16, for: .normal)
            button18.setTitle(text17, for: .normal)
            button19.setTitle(text18, for: .normal)
            button20.setTitle(text19, for: .normal)
            button21.setTitle(text20, for: .normal)
            button22.setTitle(text21, for: .normal)
            button23.setTitle(text22, for: .normal)
            button24.setTitle(text23, for: .normal)
        }
        
        //
        
        var faiblesSelonEuxCount = faiblesSelonEux.count
        
        if (faiblesSelonEuxCount == 1){
            var text24 = faiblesSelonEux[0]
            button25.setTitle(text24, for: .normal)
        }
        
        if (faiblesSelonEuxCount == 2){
            var text24 = faiblesSelonEux[0]
            var text25 = faiblesSelonEux[1]
            button25.setTitle(text24, for: .normal)
            button26.setTitle(text25, for: .normal)
        }
        
        if (faiblesSelonEuxCount == 3){
            var text24 = faiblesSelonEux[0]
            var text25 = faiblesSelonEux[1]
            var text26 = faiblesSelonEux[2]
            button25.setTitle(text24, for: .normal)
            button26.setTitle(text25, for: .normal)
            button27.setTitle(text26, for: .normal)
        }
        
        if (faiblesSelonEuxCount == 4){
            var text24 = faiblesSelonEux[0]
            var text25 = faiblesSelonEux[1]
            var text26 = faiblesSelonEux[2]
            var text27 = faiblesSelonEux[3]
            button25.setTitle(text24, for: .normal)
            button26.setTitle(text25, for: .normal)
            button27.setTitle(text26, for: .normal)
            button28.setTitle(text27, for: .normal)
        }
        
        if (faiblesSelonEuxCount == 5){
            var text24 = faiblesSelonEux[0]
            var text25 = faiblesSelonEux[1]
            var text26 = faiblesSelonEux[2]
            var text27 = faiblesSelonEux[3]
            var text28 = faiblesSelonEux[4]
            button25.setTitle(text24, for: .normal)
            button26.setTitle(text25, for: .normal)
            button27.setTitle(text26, for: .normal)
            button28.setTitle(text27, for: .normal)
            button29.setTitle(text28, for: .normal)
        }
        
        if (faiblesSelonEuxCount == 6){
            var text24 = faiblesSelonEux[0]
            var text25 = faiblesSelonEux[1]
            var text26 = faiblesSelonEux[2]
            var text27 = faiblesSelonEux[3]
            var text28 = faiblesSelonEux[4]
            var text29 = faiblesSelonEux[5]
            button25.setTitle(text24, for: .normal)
            button26.setTitle(text25, for: .normal)
            button27.setTitle(text26, for: .normal)
            button28.setTitle(text27, for: .normal)
            button29.setTitle(text28, for: .normal)
            button30.setTitle(text29, for: .normal)
        }
        
        if (faiblesSelonEuxCount == 7){
            var text24 = faiblesSelonEux[0]
            var text25 = faiblesSelonEux[1]
            var text26 = faiblesSelonEux[2]
            var text27 = faiblesSelonEux[3]
            var text28 = faiblesSelonEux[4]
            var text29 = faiblesSelonEux[5]
            var text30 = faiblesSelonEux[6]
            button25.setTitle(text24, for: .normal)
            button26.setTitle(text25, for: .normal)
            button27.setTitle(text26, for: .normal)
            button28.setTitle(text27, for: .normal)
            button29.setTitle(text28, for: .normal)
            button30.setTitle(text29, for: .normal)
            button31.setTitle(text30, for: .normal)
        }
        
        if (faiblesSelonEuxCount == 8){
            var text24 = faiblesSelonEux[0]
            var text25 = faiblesSelonEux[1]
            var text26 = faiblesSelonEux[2]
            var text27 = faiblesSelonEux[3]
            var text28 = faiblesSelonEux[4]
            var text29 = faiblesSelonEux[5]
            var text30 = faiblesSelonEux[6]
            var text31 = faiblesSelonEux[7]
            button25.setTitle(text24, for: .normal)
            button26.setTitle(text25, for: .normal)
            button27.setTitle(text26, for: .normal)
            button28.setTitle(text27, for: .normal)
            button29.setTitle(text28, for: .normal)
            button30.setTitle(text29, for: .normal)
            button31.setTitle(text30, for: .normal)
            button32.setTitle(text31, for: .normal)
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
