//
//  Activity31.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 17/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import CircleProgressBar

class Activity31: UIViewController {

    @IBOutlet weak var progressBar: CircleProgressBar!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPending: UILabel!
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
    
    var arrayInviteStatus = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        fetchContacts ()
        print(usersprofileid)
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        
        if let date = dateFormatterGet.date(from: "\(Date())"){
            //print(dateFormatterPrint.string(from: date))
            lblDate.text = "\(dateFormatterPrint.string(from: date))"
        }
        else {
            print("There was an error decoding the string")
        }
        
        
    }
    
    @IBAction func relancerButton(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Scoachy", message: "Demande(s) relancée(s)", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
            //self.performSegue(withIdentifier: "backToLogin", sender: self)
            guard let url = URL(string: "http://192.168.6.57:8088/invite/resend/\((self.usersprofileid)!)") else { return }
            let session = URLSession.shared
            let task = session.dataTask(with: url) { (data, _, _) in
                guard let data = data else { return }
                
                do {}
                catch{}
            
            }
            task.resume()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
   
        
    }
    
    
    
    @objc func fetchContacts () {
        
        guard let url = URL(string: "http://192.168.6.57:8088/invite/list/\((usersprofileid)!)") else { return }
        
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, _, _) in
            guard let data = data else { return }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                let users = try! JSONDecoder().decode(User.self, from: data)
                //print(users)
                
                
                DispatchQueue.main.async(execute: {
                    
                    self.arrayInviteStatus.removeAll()
                
                    for user in (users.ContactsDto)! {
        
                        self.arrayInviteStatus.append(user.InviteStatus!)
                    
                    }
                    
                    var counts = [String: Int]()
                    var pending : Int = 0
                    var accepted : Int = 0
                    
                    if (self.arrayInviteStatus.count == 0) {
                        
                        self.lblPending.text = "0"
                        self.progressBar.setHintTextGenerationBlock { (progress) -> String? in
                            return String.init("0% Personnes Atteintes")
                        }
                        self.progressBar.hintTextFont = UIFont.boldSystemFont(ofSize: 10)
                        self.progressBar.setProgress(0.0, animated: true, duration: 3.0)
                        
                    }
                    
                    
                    // Count the values with using forEach
                    self.arrayInviteStatus.forEach { counts[$0] = (counts[$0] ?? 0) + 1 }
                    
                    // Find the most frequent value and its count with max(isOrderedBefore:)
                    if let (value, count) = counts.max(by: {$0.1 < $1.1}) {
                        print("\(value) occurs \(count) times")
                        if value == "pending"{
                            pending = count
                            accepted = self.arrayInviteStatus.count - count
                        }
                        else if value == "accepted"{
                            accepted = count
                            pending = self.arrayInviteStatus.count - count
                        }
                        

                        self.lblPending.text = String(pending)
                        
                        var y = Double(accepted)
                        var x = Double(self.arrayInviteStatus.count)
                        var total = (y/x)*100
                        print(CGFloat(total))
     
                        self.progressBar.setHintTextGenerationBlock { (progress) -> String? in
                        return String.init("\(Int(total))% Personnes Atteintes")
                        }
                        self.progressBar.hintTextFont = UIFont.boldSystemFont(ofSize: 10)
                        self.progressBar.setProgress((CGFloat(total))/100, animated: true, duration: 1.5)
                        
                    }
                })
                
            }catch {}
        }
       
        
        task.resume()
        
    }

    
    
    @IBAction func closeButton(_ sender: Any) {
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(2, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
