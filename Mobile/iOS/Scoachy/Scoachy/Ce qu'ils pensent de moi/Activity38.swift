//
//  Activity38.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 20/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity38: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var myLabel1: UILabel!
    @IBOutlet weak var myLabel2: UILabel!
    @IBOutlet weak var myLabel3: UILabel!
    @IBOutlet weak var myLabel4: UILabel!
    @IBOutlet weak var myLabel5: UILabel!
    @IBOutlet weak var myLabel6: UILabel!
    
    @IBOutlet weak var myLabel7: UILabel!
    @IBOutlet weak var myLabel8: UILabel!
    @IBOutlet weak var myLabel9: UILabel!
    @IBOutlet weak var myLabel10: UILabel!
    @IBOutlet weak var myLabel11: UILabel!
    @IBOutlet weak var myLabel12: UILabel!
    
    var selonMoi = [String] ()
    var selonEux = [String] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1800)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        performSegue(withIdentifier: "next5", sender: self)
    }
    
    @IBAction func rightSwipe(_ sender: UISwipeGestureRecognizer) {
        performSegue(withIdentifier: "back4", sender: self)
    }
    
    @IBAction func closeButton(_ sender: Any) {
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(2, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        var userDefaults = UserDefaults.standard
        var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            let stmt1 = try db.run(" select  r.questionId as QuestionId, r.userId as User ,q.ratingtypeId as ratingtypeId, q.description as description ,q.question as questions, r.answerId as Answerid, a.description as ansdesc , a.weight as weight from  answerresult r,answer a,allquestion q where  r.questionId = q.id and r.answerId = a.id and r.userId ='\((usersprofileid)!)' and q.ratingtypeId ='8276630d-5cb0-11e7-a11e-7a791928939c' GROUP BY questionId order by a.weight  DESC,description ASC LIMIT 6")
            
//            let stmt1 = try db.run("select r.answerId as answerId ,r.questionId as questionId,  o.ratingtypeId as ratingtypeId , o.description as description,  a.description as rate FROM answerresult r, allquestion o, answer a WHERE r.questionId = o.id AND r.userid = '\(usersprofileid)' AND o.ratingtypeId = '8276630d-5cb0-11e7-a11e-7a791928939c' AND o.ratingtypeId = a.ratingtypeId GROUP BY r.answerId order by rate desc limit 6")
            
            let stmt2 = try db.run("SELECT r.answerId AS answerId ,r.questionId AS questionId,  q.ratingtypeId AS ratingtypeId , q.description AS description ,answer.weight ,AVG(answer.weight) AS Average FROM resultsummary r, allquestion_other q ,answer WHERE r.questionId = q.id AND r.answerId = answer.id AND r.userid ='\((usersprofileid)!)' AND q.ratingtypeId = '8276630d-5cb0-11e7-a11e-7a791928939c' GROUP BY questionId,answer.weight ORDER BY answer.weight DESC ,description ASC LIMIT 6")
            
            for row in stmt1 {
                
                let answer = row[3] as! String
                selonMoi.append(answer)
            }
            
            for row2 in stmt2 {
                
                let answer1 = row2[3] as! String
                selonEux.append(answer1)
            }
            
        }
            
        catch {
            print("Connection to database failed")
            
        }
        

        
        var selonMoiCount = selonMoi.count
        
        if (selonMoiCount == 1){
            var text = selonMoi[0]
            myLabel1.text = text
        }
        
        if (selonMoiCount == 2){
            var text = selonMoi[0]
            var text1 = selonMoi[1]
            myLabel1.text = text
            myLabel2.text = text1
        }
        
        if (selonMoiCount == 3){
            var text = selonMoi[0]
            var text1 = selonMoi[1]
            var text2 = selonMoi[2]
            myLabel1.text = text
            myLabel2.text = text1
            myLabel3.text = text2
        }
        
        if (selonMoiCount == 4){
            var text = selonMoi[0]
            var text1 = selonMoi[1]
            var text2 = selonMoi[2]
            var text3 = selonMoi[3]
            myLabel1.text = text
            myLabel2.text = text1
            myLabel3.text = text2
            myLabel4.text = text3
        }
        
        if (selonMoiCount == 5){
            var text = selonMoi[0]
            var text1 = selonMoi[1]
            var text2 = selonMoi[2]
            var text3 = selonMoi[3]
            var text4 = selonMoi[4]
            myLabel1.text = text
            myLabel2.text = text1
            myLabel3.text = text2
            myLabel4.text = text3
            myLabel5.text = text4
        }
        
        if (selonMoiCount == 6){
            var text = selonMoi[0]
            var text1 = selonMoi[1]
            var text2 = selonMoi[2]
            var text3 = selonMoi[3]
            var text4 = selonMoi[4]
            var text5 = selonMoi[5]
            myLabel1.text = text
            myLabel2.text = text1
            myLabel3.text = text2
            myLabel4.text = text3
            myLabel5.text = text4
            myLabel6.text = text5
        }
        
        //
        
        
        var selonEuxCount = selonEux.count
        
        if (selonEuxCount == 1){
            var text6 = selonEux[0]
            myLabel7.text = text6
        }
        
        if (selonEuxCount == 2){
            var text6 = selonEux[0]
            var text7 = selonEux[1]
            myLabel7.text = text6
            myLabel8.text = text7
        }
        
        if (selonEuxCount == 3){
            var text6 = selonEux[0]
            var text7 = selonEux[1]
            var text8 = selonEux[2]
            myLabel7.text = text6
            myLabel8.text = text7
            myLabel9.text = text8
        }
        
        if (selonEuxCount == 4){
            var text6 = selonEux[0]
            var text7 = selonEux[1]
            var text8 = selonEux[2]
            var text9 = selonEux[3]
            myLabel7.text = text6
            myLabel8.text = text7
            myLabel9.text = text8
            myLabel10.text = text9
        }
        
        if (selonEuxCount == 5){
            var text6 = selonEux[0]
            var text7 = selonEux[1]
            var text8 = selonEux[2]
            var text9 = selonEux[3]
            var text10 = selonEux[4]
            myLabel7.text = text6
            myLabel8.text = text7
            myLabel9.text = text8
            myLabel10.text = text9
            myLabel11.text = text10
        }
        
        if (selonEuxCount == 6){
            var text6 = selonEux[0]
            var text7 = selonEux[1]
            var text8 = selonEux[2]
            var text9 = selonEux[3]
            var text10 = selonEux[4]
            var text11 = selonEux[5]
            myLabel7.text = text6
            myLabel8.text = text7
            myLabel9.text = text8
            myLabel10.text = text9
            myLabel11.text = text10
            myLabel12.text = text11
        }
        
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
