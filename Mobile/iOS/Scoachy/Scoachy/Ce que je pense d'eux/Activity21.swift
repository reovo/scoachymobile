//
//  Activity21.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 10/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity21: UIViewController {
    
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAttribute: UILabel!
    
    
    var attributesQuestion = [(questionid:String, question: String, description: String, attribute: String)]()
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(_ sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(1, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
    }
    
    func redirect(action: UIAlertAction){
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func answer_1(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "b7541f37-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: attributesQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        attributesQuestion.remove(at: 0)
        
        if (attributesQuestion.isEmpty) {
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: redirect)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else{
            lblQuestion.text = attributesQuestion[0].question
            lblDescription.text = attributesQuestion[0].description
            lblAttribute.text = attributesQuestion[0].attribute
            
        }
        
    }
    
    @IBAction func answer_2(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "b75607a3-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: attributesQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        attributesQuestion.remove(at: 0)
        
        if (attributesQuestion.isEmpty) {
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: redirect)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else{
            lblQuestion.text = attributesQuestion[0].question
            lblDescription.text = attributesQuestion[0].description
            lblAttribute.text = attributesQuestion[0].attribute
        }
        
    }
    
    @IBAction func answer_3(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "b75618f8-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: attributesQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        attributesQuestion.remove(at: 0)
        
        if (attributesQuestion.isEmpty) {
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: redirect)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else{
            lblQuestion.text = attributesQuestion[0].question
            lblDescription.text = attributesQuestion[0].description
            lblAttribute.text = attributesQuestion[0].attribute
        }
        
    }
    
    @IBAction func answer_4(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "b7562603-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: attributesQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        attributesQuestion.remove(at: 0)
        
        if (attributesQuestion.isEmpty) {
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: redirect)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else{
            lblQuestion.text = attributesQuestion[0].question
            lblDescription.text = attributesQuestion[0].description
            lblAttribute.text = attributesQuestion[0].attribute
        }
        
    }
    
    @IBAction func answer_5(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "b75633c9-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: attributesQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        attributesQuestion.remove(at: 0)
        
        if (attributesQuestion.isEmpty) {
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: redirect)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else{
            lblQuestion.text = attributesQuestion[0].question
            lblDescription.text = attributesQuestion[0].description
            lblAttribute.text = attributesQuestion[0].attribute
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            var userDefaults = UserDefaults.standard
            var friendUserId = userDefaults.string(forKey: "friendUserId")!
            
            let stmt = try db.prepare("SELECT question.id, question.description AS DESCRIPTION, question.question, attribute.name AS ATTRIBUTENAME, activitycategory.id, ratingtype.name AS RNAME FROM question INNER JOIN ratingtype ON question.ratingTypeId = ratingtype.id INNER JOIN activity ON question.activityId = activity.id INNER JOIN attribute ON question.attributeId = attribute.id INNER JOIN activitycategory ON question.activityCategoryId = activitycategory.id WHERE activitycategory.id ='f33bfd80-2331-11e7-b36e-c03fd56ee88c' AND RNAME = 'attributeChoice' AND question.id NOT IN(select answerresult.questionId FROM answerresult where answerresult.friendUserId='\(friendUserId)') ORDER BY ATTRIBUTENAME, DESCRIPTION")
                        
            for row in stmt {
                
                //let orderlist = row[4] as! Int64
                let questionid = row[0] as! String
                let question = row[2] as! String
                let description = row[1] as! String
                let attribute = row[3] as! String
                
                attributesQuestion.append((questionid: questionid, question: question, description: description, attribute: attribute))
            }
            print("Attribute Question Success")
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
        lblQuestion.text = attributesQuestion[0].question
        lblDescription.text = attributesQuestion[0].description
        lblAttribute.text = attributesQuestion[0].attribute
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
