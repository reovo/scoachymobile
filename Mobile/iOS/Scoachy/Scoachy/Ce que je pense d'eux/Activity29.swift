//
//  Activity29.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 13/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class Activity29: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(_ sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(1, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
