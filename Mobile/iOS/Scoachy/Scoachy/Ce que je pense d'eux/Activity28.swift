//
//  Activity28.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 17/11/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import ContactsUI
import PopupDialog

class Activity28: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
     @IBOutlet weak var tableView: UITableView!

    var arrayFriendUserId = [String]()
    var arrayUserId = [String]()
    var arrayEmail = [String]()
    
    //var userslist = [(Name: String, Phone: String)]()
    var arrayProfilePicture: [UIImage] = []
    
    var arrayInviteStatus = [String]()
    var arrayInvitedOn = [String]()
    var arrayDate = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        fetchContacts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(_ sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(1, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
    }
    
    func fetchContacts () {
        
        var userDefaults = UserDefaults.standard
        var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
        
        guard let url = URL(string: "http://192.168.6.57:8088/invite/friendlist/\((usersprofileid)!)") else { return }
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, _, _) in
            guard let data = data else { return }
            
            let users = try! JSONDecoder().decode(User.self, from: data)
            print(users)
            
            do {
                
                DispatchQueue.main.async(execute: {
                    
                    self.arrayEmail.removeAll()
                    self.arrayFriendUserId.removeAll()
                    self.arrayInvitedOn.removeAll()
                    self.arrayFriendUserId.removeAll()
                    self.arrayInviteStatus.removeAll()
                    self.arrayProfilePicture.removeAll()
                    self.arrayDate.removeAll()
                    
                    for user in (users.ContactsDto)! {
                        
                        self.arrayEmail.append(user.Email!)
                        self.arrayFriendUserId.append(user.UserId!)
                        self.arrayInviteStatus.append(user.InviteStatus!)
                        self.arrayInvitedOn.append(user.InvitedOn!)
                        
                    }
                    //PICTOS_SCOACHY-18 pending
                    //cartouches-6 accepted
                    for item in self.arrayInviteStatus {
                        
                        if item == "accepted"{
                            self.arrayProfilePicture.append(UIImage(named: "cartouches-6.png")!)
                        }
                        
                        else {
                            self.arrayProfilePicture.append(UIImage(named: "PICTOS_SCOACHY-18.png")!)
                        }
                    }
                    
                    for item in self.arrayInvitedOn{
                        
                        let dateFormatterGet = DateFormatter()
                        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        let dateFormatterPrint = DateFormatter()
                        dateFormatterPrint.dateFormat = "yyyy/MM/dd"
                        
                        if let date = dateFormatterGet.date(from: item){
                            //print(dateFormatterPrint.string(from: date))
                            self.arrayDate.append(dateFormatterPrint.string(from: date))
                        }
                        else {
                            print("There was an error decoding the string")
                        }
                        
                    }
                    print(self.arrayEmail)
                    print(self.arrayProfilePicture)
                    print(self.arrayDate)
                    self.tableView.reloadData()
                
                })
                
            } catch {}
        }
        
        task.resume()
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrayEmail.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell28
        
        //cell.profilePicture.image = arrayProfilePicture[indexPath.row]
        cell.name.text = arrayEmail[indexPath.row]
        cell.userStatus.image = arrayProfilePicture[indexPath.row]
        cell.invitedDate.text = arrayDate[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
//        print(arrayEmail[indexPath.row])
//        print(arrayFriendUserId[indexPath.row])
        
        var userDefaults = UserDefaults.standard
        var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
        
        
        guard let url = URL(string: "http://192.168.6.57:8088/invite/changestatus/") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let parameters: [String: Any] = [
            "UserId": (usersprofileid)!,
            "FriendUserId": arrayFriendUserId[indexPath.row]
        ]
        
        do {
            let jsonBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            request.httpBody = jsonBody
        } catch {}
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, _, _) in
            guard let data = data else { return }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
                
            } catch {}
        }
        
        task.resume()
        
        userDefaults.set(arrayEmail[indexPath.row], forKey: "friendUserName")
        userDefaults.set(arrayFriendUserId[indexPath.row], forKey: "friendUserId")
        
        
        
        
        
        
        
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(1, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
    }

    
        
     override func viewDidAppear(_ animated: Bool) {
        
        fetchContacts()
    }
        

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
