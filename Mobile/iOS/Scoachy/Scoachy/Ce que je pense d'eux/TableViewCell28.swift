//
//  TableViewCell28.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 20/11/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class TableViewCell28: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var invitedDate: UILabel!
    @IBOutlet weak var userStatus: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
