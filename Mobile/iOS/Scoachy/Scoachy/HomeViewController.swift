//
//  HomeViewController.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 13/09/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite
import CircleProgressBar
import Foundation

//extension String {
//    func firstMatchIn(string: NSString!, atRangeIndex: Int!) -> String {
//        var error : NSError?
//        let re = NSRegularExpression(pattern: self, options: .CaseInsensitive, error: &error)
//        let match = re.firstMatchInString(string, options: .WithoutAnchoringBounds, range: NSMakeRange(0, string.length))
//        return string.substringWithRange(match.rangeAtIndex(atRangeIndex))
//    }
//}

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var progressBar1: CircleProgressBar!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblScoache1: UILabel!
    @IBOutlet weak var lblScoache2: UILabel!
    @IBOutlet weak var lblScoacheur: UILabel!
    
    private var rssItems: [RSSItem]?
    private var cellStates: [CellState]?
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 3000)
        
        
        tableView.estimatedRowHeight = 155.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        syncAnswers()
        fetchData()
        numberOfUsers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func statistics(_ sender: Any) {
        
        //performSegue(withIdentifier: "stats", sender: nil)
        
    }
    
    private func fetchData()
    {
        let feedParser = FeedParser()
        feedParser.parseFeed(url: "https://scoachy.com/feed/rss2") { (rssItems) in
            self.rssItems = rssItems
            self.cellStates = Array(repeating: .collapsed, count: rssItems.count)
            
            OperationQueue.main.addOperation {
                self.tableView.reloadSections(IndexSet(integer: 0), with: .left)
            }
        }
    }
    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // Return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        guard let rssItems = rssItems else {
            return 0
        }
        
        // rssItems
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NewsTableViewCell
        if let item = rssItems?[indexPath.item] {
            cell.item = item
            cell.selectionStyle = .none
            
            //print(rssItems?[indexPath.row].image)
            print((rssItems?[indexPath.row].image)!)
           
            if let cellStates = cellStates {
//                cell.descriptionLabel.numberOfLines = (cellStates[indexPath.row] == .expanded) ? 0 : 4
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! NewsTableViewCell
        
        UIApplication.shared.open(NSURL(string:"\((rssItems?[indexPath.row].link)!)")! as URL)
        
        
        
        tableView.beginUpdates()
        //cell.descriptionLabel.numberOfLines = (cell.descriptionLabel.numberOfLines == 0) ? 3 : 0
        
        //cellStates?[indexPath.row] = (cell.descriptionLabel.numberOfLines == 0) ? .expanded : .collapsed
        
        tableView.endUpdates()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {

        progressBar1.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar1.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar1.setProgress(0.25, animated: true, duration: 3.0)
    
        
    }
    
    func numberOfUsers () {
        
        guard let url = URL(string: "http://192.168.6.57:8088/scoachy/globalstats/") else { return }
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, _, _) in
            guard let data = data else { return }
            
            let users = try! JSONDecoder().decode(Scoacheur.self, from: data)
            //print(users)
            
            do {
                
                DispatchQueue.main.async(execute: {
                    
                    self.lblScoache1.text = String(users.Scoached!)
                    self.lblScoache2.text = String("\(users.Scoached!) SCOACHÉ(S)")
                    self.lblScoacheur.text = String("\(users.Scoachers!) SCOACHEURS")
                    
                })
                
            } catch {}
        }
        
        task.resume()
        
    }
    
    
    func syncAnswers () {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let newDate = dateFormatter.string(from: Date())
            print(newDate)
            
            let stmt = try db.run("SELECT * FROM answerresult WHERE sent IS NULL OR sent = ''")
            
            for row in stmt {
                
                var userId = row[1] as! String
                var answerId = row[2] as! String
                var lastAnsweredDate = row[3] as! String
                var questionId = row[4] as! String
                var friendUserId = ("\(row[5] ?? "" as! String)")
                
                if friendUserId == nil {
                    
                    print("nil updated")
                    friendUserId = ""
                    
                }
                
                print("\(userId) userid")
                print("\(answerId) answerid")
                print("\(lastAnsweredDate) lastanswereddate")
                print("\(questionId) questionid")
                print("\(friendUserId) FriendUser")
                
                
                //http://192.168.6.57:8088/results/send/
                //Send Results
                
                guard let url = URL(string: "http://192.168.6.57:8088/results/send/") else { return }
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                
                let stmt = try db.run("UPDATE answerresult SET sent = '1', sent_on = '\(newDate)' WHERE questionId = '\(questionId)'")
                
                let parameters: [String: Any] = [
                    
                       "ResultsList":[["UserId":"\((usersprofileid)!)","FriendUserId":"\(friendUserId)","TransferDate": "\(newDate)","UserAnswers":[["AnswerId":"\(answerId)","LastAnsweredDate": "\(lastAnsweredDate)","QuestionId":"\(questionId)"]]]]
                ]

                do {
                    let jsonBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                    request.httpBody = jsonBody
                } catch {}
                
                let session = URLSession.shared
                let task = session.dataTask(with: request) { (data, _, _) in
                guard let data = data else { return }
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print(json)
                
                    } catch {}
                }
                
                task.resume()
                
            }
            
        }
        
        catch {
            print(error)
            
        }
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
