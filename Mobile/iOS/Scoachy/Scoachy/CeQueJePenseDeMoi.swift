//
//  CeQueJePenseDeMoi.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 08/09/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import CircleProgressBar
import SQLite

class CeQueJePenseDeMoi: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var progressBar1: CircleProgressBar!
    @IBOutlet weak var progressBar2: CircleProgressBar!
    @IBOutlet weak var progressBar3: CircleProgressBar!
    @IBOutlet weak var progressBar4: CircleProgressBar!
    @IBOutlet weak var progressBar5: CircleProgressBar!
    @IBOutlet weak var progressBar6: CircleProgressBar!
    @IBOutlet weak var progressBar7: CircleProgressBar!
    @IBOutlet weak var progressBar8: CircleProgressBar!
    
    @IBOutlet weak var animal: UILabel!
    @IBOutlet weak var attributes: UILabel!
    @IBOutlet weak var element: UILabel!
    @IBOutlet weak var competences: UILabel!
    @IBOutlet weak var season: UILabel!
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var color: UILabel!
    @IBOutlet weak var motivation: UILabel!
    
    var animalQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var attributesQuestion = [(orderlist: Int, questionid:String, question: String, description: String, attribute: String)]()
    
    var elementQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var competencesQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var seasonQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var roleQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var colorQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var motivationQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 2300)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        
        var userDefaults = UserDefaults.standard
        var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
        
        //Circular Bar
        
        progressBar1.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar1.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar1.setProgress(0.01, animated: true, duration: 3.0)

        progressBar2.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar2.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar2.setProgress(0.01, animated: true, duration: 3.0)

        progressBar3.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar3.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar3.setProgress(0.01, animated: true, duration: 3.0)

        progressBar4.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar4.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar4.setProgress(0.01, animated: true, duration: 3.0)

        progressBar5.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar5.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar5.setProgress(0.01, animated: true, duration: 3.0)

        progressBar6.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar6.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar6.setProgress(0.01, animated: true, duration: 3.0)

        progressBar7.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar7.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar7.setProgress(0.01, animated: true, duration: 3.0)

        progressBar8.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar8.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar8.setProgress(0.01, animated: true, duration: 3.0)
        
        //Database Index
        
//        let animal1 = View("animal1")
//        let attributes1 = View("attributes1")
//        let element1 = View("element1")
//        let competences1 = View("competences1")
//        let season1 = View("season1")
//        let role1 = View("role1")
//        let color1 = View("color1")
//        let motivation1 = View("motivation1")
//        let orderlist = Expression<Int64>("orderList")
//        let question = Expression<String>("question")
//        let questionid = Expression<String>("id")
//        let description = Expression<String>("description")
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            for row in try db.prepare("SELECT * FROM allquestion where allquestion.id= 'ac627cc3-607d-11e7-a7e0-484d7ee0cd26' and allquestion.id  NOT IN (select answerresult.questionId FROM answerresult where answerresult.userId='\((usersprofileid)!)')"){
                
                let orderlist = row[4] as! Int64
                let questionid = row[0] as! String
                let question = row[6] as! String
                let description = row[7] as! String
                
                animalQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                
            }
        
            for row in try db.prepare("SELECT * FROM allquestion where allquestion.activityCategoryId = 'f4c0c96f-2331-11e7-b36e-c03fd56ee88c' and allquestion.name= 'attributeChoice' and allquestion.id  NOT IN (select answerresult.questionId FROM answerresult where answerresult.userId= '\((usersprofileid)!)')"){
                
                let orderlist = row[4] as! Int64
                let questionid = row[0] as! String
                let question = row[6] as! String
                let description = row[7] as! String
                let attribute = row[18] as! String
                
                attributesQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description, attribute: attribute))
                
            }
            
            for row in try db.prepare("SELECT * FROM allquestion where allquestion.activityCategoryId = 'f4c0c96f-2331-11e7-b36e-c03fd56ee88c' and allquestion.name= 'elementChoice' and allquestion.id  NOT IN (select answerresult.questionId FROM answerresult where answerresult.userId= '\((usersprofileid)!)')"){
                
                let orderlist = row[4] as! Int64
                let questionid = row[0] as! String
                let question = row[6] as! String
                let description = row[7] as! String
                
                elementQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                
            }
            
            for row in try db.prepare("SELECT * FROM allquestion where allquestion.activityCategoryId = 'f4c0c96f-2331-11e7-b36e-c03fd56ee88c' and allquestion.name= 'competenceChoice' and allquestion.id  NOT IN (select answerresult.questionId FROM answerresult where answerresult.userId= '\((usersprofileid)!)')"){
                
                let orderlist = row[4] as! Int64
                let questionid = row[0] as! String
                let question = row[6] as! String
                let description = row[7] as! String
                
                competencesQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                
            }
            
            for row in try db.prepare("SELECT * FROM allquestion where allquestion.activityCategoryId = 'f4c0c96f-2331-11e7-b36e-c03fd56ee88c' and allquestion.name= 'seasonChoice' and allquestion.id  NOT IN (select answerresult.questionId FROM answerresult where answerresult.userId= '\((usersprofileid)!)')"){
                
                let orderlist = row[4] as! Int64
                let questionid = row[0] as! String
                let question = row[6] as! String
                let description = row[7] as! String
                
                seasonQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                
            }

            for row in try db.prepare("SELECT * FROM allquestion where allquestion.activityCategoryId = 'f4c0c96f-2331-11e7-b36e-c03fd56ee88c' and allquestion.name= 'roleChoice' and allquestion.id  NOT IN (select answerresult.questionId FROM answerresult where answerresult.userId= '\((usersprofileid)!)')"){
                
                let orderlist = row[4] as! Int64
                let questionid = row[0] as! String
                let question = row[6] as! String
                let description = row[7] as! String
                
                roleQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                
            }

            for row in try db.prepare("SELECT * FROM allquestion where allquestion.activityCategoryId = 'f4c0c96f-2331-11e7-b36e-c03fd56ee88c' and allquestion.name= 'colorChoice' and allquestion.id  NOT IN (select answerresult.questionId FROM answerresult where answerresult.userId= '\((usersprofileid)!)')"){
                
                let orderlist = row[4] as! Int64
                let questionid = row[0] as! String
                let question = row[6] as! String
                let description = row[7] as! String
                
                colorQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                
            }
            
            for row in try db.prepare("SELECT * FROM allquestion where allquestion.activityCategoryId = 'f4c0c96f-2331-11e7-b36e-c03fd56ee88c' and allquestion.name= 'motivationChoice' and allquestion.id  NOT IN (select answerresult.questionId FROM answerresult where answerresult.userId= '\((usersprofileid)!)')"){
                
                let orderlist = row[4] as! Int64
                let questionid = row[0] as! String
                let question = row[6] as! String
                let description = row[7] as! String
                
                motivationQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                
            }

            
            print("Array Question Success")
            
        }
            
        catch {
            print("Database Error")
            
        }

        //UI Label Gesture Recognizer

        let tap1 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapAnimal))
        animal.isUserInteractionEnabled = true
        animal.addGestureRecognizer(tap1)

        let tap2 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapAttributes))
        attributes.isUserInteractionEnabled = true
        attributes.addGestureRecognizer(tap2)

        let tap3 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapElement))
        element.isUserInteractionEnabled = true
        element.addGestureRecognizer(tap3)

        let tap4 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapCompetences))
        competences.isUserInteractionEnabled = true
        competences.addGestureRecognizer(tap4)

        let tap5 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapSeason))
        season.isUserInteractionEnabled = true
        season.addGestureRecognizer(tap5)

        let tap6 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapRole))
        role.isUserInteractionEnabled = true
        role.addGestureRecognizer(tap6)

        let tap7 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapColor))
        color.isUserInteractionEnabled = true
        color.addGestureRecognizer(tap7)

        let tap8 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapMotivation))
        motivation.isUserInteractionEnabled = true
        motivation.addGestureRecognizer(tap8)
        
    }
    
    @objc func tapAnimal(sender:UITapGestureRecognizer) {
    
        if(animalQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            performSegue(withIdentifier: "animalChoice1", sender: self)
        }
    }
    
    @objc func tapAttributes(sender:UITapGestureRecognizer){
        
        if(attributesQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        else{
        performSegue(withIdentifier: "attributeChoice1", sender: self)
        }
    }
    
    @objc func tapElement(sender:UITapGestureRecognizer){
        
        if(elementQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else{
            performSegue(withIdentifier: "elementChoice1", sender: self)
        }
    }
    
    @objc func tapCompetences(sender:UITapGestureRecognizer){
        
        if(competencesQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else{
            performSegue(withIdentifier: "competenceChoice1", sender: self)
        }
        
    }
    
    @objc func tapSeason(sender:UITapGestureRecognizer){
        
        if(seasonQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else{
            performSegue(withIdentifier: "seasonChoice1", sender: self)
        }
        
    }
    
    @objc func tapRole(sender:UITapGestureRecognizer){
        
        if(roleQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else{
            performSegue(withIdentifier: "roleChoice1", sender: self)
        }
        
    }
    
    @objc func tapColor(sender:UITapGestureRecognizer){
        
        if(colorQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else{
            performSegue(withIdentifier: "colorChoice1", sender: self)
        }
        
    }
    
    @objc func tapMotivation(sender:UITapGestureRecognizer){
        
        if(motivationQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else{
            performSegue(withIdentifier: "motivationChoice1", sender: self)
        }
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
