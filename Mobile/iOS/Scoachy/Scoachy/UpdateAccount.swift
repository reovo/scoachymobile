//
//  UpdateAccount.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 05/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite
import CountryList

class UpdateAccount: UIViewController, CountryListDelegate {
    
    var userAccount = [(id: String, userProfile: String, nom: String, prenom: String, telephone: String, country: String, prefix: String, email: String, codepostal: String, gender: String, dob: String, newsletter: String)]()
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var handleCountryList: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var segmentedControlGender: UISegmentedControl!
    @IBOutlet weak var segmentedControlNewsletter: UISegmentedControl!
    
    @IBOutlet weak var txtFirstname: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPostal: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var lblEmail: UILabel!
    
    var countryList = CountryList()
    var dateSelected = ""
    var userGender = ""
    var newsletterSelected = ""
    var userCountry = ""
    var userPrefix = ""
    var salt = ""
    var encrpytedPassword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UpdateAccount.dismissKeyboard))
        
        view.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
        countryList.delegate = self
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1200)
        self.navigationController?.isNavigationBarHidden = true
    
        let userProfile = Table("userprofile")
        let upid = Expression<String>("profileId")
        let userProfileId = Expression<String>("userProfileId")
        let nom = Expression<String>("nom")
        let prenom = Expression<String>("prenom")
        let telephone = Expression<String>("telephone")
        let country = Expression<String>("country")
        let prefix = Expression<String>("prefix")
        let email = Expression<String>("email")
        let codepostal = Expression<String>("codepostal")
        let gender = Expression<String>("gender")
        let dob = Expression<String>("dob")
        let newsletter = Expression<String>("newsletter")
        
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            for x in try db.prepare(userProfile.order(upid)){
                userAccount.append(((String(x[upid])), x[userProfileId], x[nom], x[prenom], x[telephone], x[country], x[prefix], x[email], x[codepostal], x[gender], x[dob], x[newsletter]))
            }
            
            //var DBFirstName: String?
            
            var DBFirstName = userAccount[0].nom
            print("DBFN")
            
        
            
            var DBLastName = userAccount[0].prenom
            var DBEmail = userAccount[0].email
            var DBPostal = userAccount[0].codepostal
            var DBPhone = userAccount[0].telephone
            
//            txtFirstname.text = userAccount[0].prenom
//            txtLastName.text = userAccount[0].nom
//            txtEmail.text = userAccount[0].email
//            txtPostal.text = userAccount[0].codepostal
//            txtPhone.text = userAccount[0].telephone
//            lblEmail.text = userAccount[0].email
            
            
            txtFirstname.text = DBFirstName
            txtLastName.text = DBLastName
            txtEmail.text = DBEmail
            txtPostal.text = DBPostal
            txtPhone.text = DBPhone
            lblEmail.text = DBEmail
            
            dateSelected = userAccount[0].dob
            userGender = userAccount[0].gender
            newsletterSelected = userAccount[0].newsletter
            userCountry = userAccount[0].country
            userPrefix = userAccount[0].prefix
            
            if userCountry == "" {
                handleCountryList.setTitle("Veuillez choisir votre pays", for: .normal)
            }
            
            else {
                handleCountryList.setTitle(userAccount[0].country, for: .normal)
            }
            
            
            let dateString = userAccount[0].dob
            print("dob")
            print(dateString)
            var dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd"
            let date = dateFormatter.date(from: dateString)
            if date == nil {
                print("DOB Null")
            }
            
            else {
                datePicker.setDate(date!, animated: false)
            }
            
            var genderIndex = userAccount[0].gender
            if genderIndex == "M" {
                segmentedControlGender.selectedSegmentIndex = 0
            }
                
            else if genderIndex == "F" {
                segmentedControlGender.selectedSegmentIndex = 1
            }
            
            var newsletterIndex = userAccount[0].newsletter
            if newsletterIndex == "Optional(true)" {
                segmentedControlNewsletter.selectedSegmentIndex = 0
            }
                
            else if newsletterIndex == "Optional(false)" {
                segmentedControlNewsletter.selectedSegmentIndex = 1
            }
        
        }
            
        catch {
            print(error)
            
        }
  
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(_sender: Any){
        
        performSegue(withIdentifier: "home", sender: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func handleCountryList(_ sender: Any) {
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    
    
    func selectedCountry(country: Country) {
        
        self.handleCountryList.setTitle("\(country.flag!) \(country.name!), \(country.countryCode), \(country.phoneExtension)", for: .normal)
        userCountry = country.name!
        userPrefix = country.phoneExtension
        
    }
    
    @IBAction func datePickerChanged(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let selectedDate = dateFormatter.string(from: sender.date)
        dateSelected = selectedDate
        
    }
    
    @IBAction func indexChangedGender(sender: UISegmentedControl){
        switch segmentedControlGender.selectedSegmentIndex {
        case 0:
            userGender = "M"
        case 1:
            userGender = "F"
        default:
            break;
        }
    }
    
    @IBAction func indexChangedNewsletter(sender: UISegmentedControl){
        switch segmentedControlNewsletter.selectedSegmentIndex {
        case 0:
            newsletterSelected = "true"
        case 1:
            newsletterSelected = "false"
        default:
            break;
        }
    }
    
    @IBAction func updateAccount(_sender: Any){
        
        var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
        
        if !(txtPassword.text?.isEmpty ?? true) {
            var encrypt = Encryption()
            salt = encrypt.RandomGenerator(length: 18)
            encrpytedPassword = encrypt.Encrypt(plainText: txtPassword.text!, salt: salt)
        }
        
        guard let url = URL(string: "http://192.168.6.57:8088/account/update/") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let parameters: [String: Any] = [
            
            "Id": (usersprofileid)!,
            "Username": txtEmail.text!,
            "FirstName": txtFirstname.text,
            "LastName": txtLastName.text,
            "Gender": userGender,
            "BirthDate": dateSelected,
            "PostalCode": txtPostal.text,
            "Country": userCountry,
            "Email": txtEmail.text,
            "Language":"Francais",
            "Phone": txtPhone.text,
            "PhonePrefix": userPrefix,
            "EncryptedPassword": encrpytedPassword,
            "salt": salt,
            "ReceiveNewsletter": newsletterSelected,
            "RegistrationMode":"Email",
            "Os": UIDevice.current.model,
            "OsVersion": UIDevice.current.systemVersion,
            "UserStatus": "0ffc9801-33ee-11e7-9426-c03fd56ee88c",
            "UserType": "Paid"
        ]
        
        do {
            let jsonBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            
            request.httpBody = jsonBody
            
        } catch {}
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, _, _) in
            guard let data = data else { return }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
                
                var users = try! JSONDecoder().decode(CreateAccount.self, from: data)
                
                if users.ErrorCode == 0 {
                    
                    DispatchQueue.main.async(execute: {
                       DBUtil.sharedInstance.updateUser(inputNom: self.txtFirstname.text!, inputPrenom: self.txtLastName.text!, inputTelephone: self.txtPhone.text!, inputCountry: self.userCountry, inputPrefix: self.userPrefix, inputCodePostal: self.txtPostal.text!, inputGender: self.userGender, inputDob: self.dateSelected, inputNewsletter: self.newsletterSelected)
                        
                        self.performSegue(withIdentifier: "home", sender: nil)
                    })
                    
                    
                    
                }
                
                else {
                    
                    DispatchQueue.main.async(execute: {
                        let alertController = UIAlertController(title: "Scoachy", message: users.Message, preferredStyle: . alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(defaultAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    })
                }
    
            } catch {}
        }
        task.resume()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func logoutAccount(_sender: Any){
        
        var userDefaults = UserDefaults.standard
        userDefaults.set("loggedOut", forKey: "userLoggedIn")
        userDefaults.set(nil, forKey: "friendUserName")
        userDefaults.set(nil, forKey: "txtSaisie")
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(newViewController, animated: true, completion: nil)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

