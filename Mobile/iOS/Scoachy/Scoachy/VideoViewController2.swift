//
//  VideoViewController2.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 09/03/2018.
//  Copyright © 2018 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class VideoViewController2: UIViewController, AVAudioPlayerDelegate  {
    
    let playerController = AVPlayerViewController()
    
    var flag = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {

        // Play video
        if(flag == 0){
            flag = 1
            playVideo()
        }
            
        else{
            performSegue(withIdentifier: "redirectProfil", sender: self)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        print("Video Finished")
        
        self.playerController.dismiss(animated: true)
    }
    
    func playVideo() {
        guard let path = Bundle.main.path(forResource: "SCOACHY_Final", ofType:"mp4") else {
            debugPrint("Video not found")
            return
        }
        
        
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        
        playerController.player = player
        
        present(playerController, animated: true) {
            player.play()
            
            NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
