//
//  Activity420.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 15/03/2018.
//  Copyright © 2018 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity420: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedItems: [Int] = []
    var arrayVerb = [(verbid: String, verb:String, count: Int)]()
    var arrayVerbCount = [(verbid: String, verb:String, count: Int)]()
    
    var arrayVerbSelected = [String]()
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func suivant(_ sender: Any) {
        
        if arrayVerbSelected.count == 5 {
            
            for item in arrayVerbSelected{
                
                DBUtil.sharedInstance.addSelectedTalent(inputId: UUID().uuidString, inputTalentSelected: item, inputUserId: (usersprofileid)!)
            }
            print("success")
            performSegue(withIdentifier: "ledebrief", sender: self)
        }
        
        else{
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez choisir 5 talents", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrayVerbCount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MesTalentsChoixCell
        cell.verb!.text = arrayVerbCount[indexPath.row].verb

        
        if selectedItems.contains(indexPath.row) {
            cell.accessoryType = .checkmark
            arrayVerbSelected.append(arrayVerbCount[indexPath.row].verbid)
            print(arrayVerbSelected)

        } else {
            cell.accessoryType = .none
            print("deselected")
            if let idx = arrayVerbSelected.index(of:"\(arrayVerbCount[indexPath.row].verbid)") {
                arrayVerbSelected.remove(at: idx)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedItems.contains(indexPath.row) {
            let index = selectedItems.index(of: indexPath.row)
            selectedItems.remove(at: index!)
            if let idx = arrayVerbSelected.index(of:"\(arrayVerbCount[indexPath.row].verbid)") {
                arrayVerbSelected.remove(at: idx)
            }
            print(arrayVerbSelected)
            
        } else {
            selectedItems.append(indexPath.row)
        }
        
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            for row in try db.prepare("SELECT talentanswer.talentVerbId, talentverb.verb, talentanswer.value FROM talentverb INNER JOIN talentanswer ON talentverb.id = talentanswer.talentVerbId AND talentanswer.userId = '\((usersprofileid)!)'"){
                
                let talentverbid = row[0] as! String
                let talentverb = row[1] as! String
                let value = row[2] as! Int64
                
                arrayVerb.append((verbid: talentverbid, verb:talentverb, count: Int(value)))
                
            }
            
            for index in stride(from: 0, to: arrayVerb.count, by: 3) {
                
                //print("\(arrayVerb[index].verbid): \((arrayVerb[index].count * arrayVerb[index+1].count * arrayVerb[index+2].count))")
                arrayVerbCount.append((verbid: arrayVerb[index].verbid, verb: arrayVerb[index].verb, count:((arrayVerb[index].count * arrayVerb[index+1].count * arrayVerb[index+2].count))))
            }
            
            
            //arrayVerbCount = arrayVerbCount.sorted(by: {$0.count > $1.count})
            
            self.tableView.reloadData()
            
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
