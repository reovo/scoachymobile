//
//  MesModeles.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 19/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class MesModeles: UITableViewCell {
    
    @IBOutlet weak var modalImage: UIImageView!
    @IBOutlet weak var valueImage: UIImageView!
    
    @IBOutlet weak var modelName: UILabel!
    @IBOutlet weak var modelDescription: UILabel!
    @IBOutlet weak var modelValue: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
