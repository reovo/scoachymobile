//
//  Activity42.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 25/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite
import CircleProgressBar

class Activity42: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblLoisir: UILabel!
    @IBOutlet weak var lblSante: UILabel!
    @IBOutlet weak var lblArgent: UILabel!
    @IBOutlet weak var lblPersonnel: UILabel!
    @IBOutlet weak var lblAmour: UILabel!
    @IBOutlet weak var lblFamille: UILabel!
    @IBOutlet weak var lblAmi: UILabel!
    @IBOutlet weak var lblProfessionel: UILabel!
    @IBOutlet weak var lblProgress: UILabel?
    
    
    @IBOutlet weak var progressBar: CircleProgressBar!
    
    var myValues = [Int]()
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 3000)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        
        performSegue(withIdentifier: "next", sender: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)

            }
        }

        do {
            let db = try Connection(fullDestPathString, readonly:false)

            var userDefaults = UserDefaults.standard
            var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

            let stmt = try db.prepare("SELECT * FROM wheellife where userid ='\((usersprofileid)!)'")

            for row in stmt {

                let val = row[3] as! Int64
                myValues.append(Int(val))

            }

        }

        catch {
            print(error)

        }

        lblLoisir.text = String(myValues[0]) + " %"
        lblSante.text = String(myValues[1]) + " %"
        lblArgent.text = String(myValues[2]) + " %"
        lblPersonnel.text = String(myValues[3]) + " %"
        lblAmour.text = String(myValues[4]) + " %"
        lblFamille.text = String(myValues[5]) + " %"
        lblAmi.text = String(myValues[6]) + " %"
        lblProfessionel.text = String(myValues[7]) + " %"

        var sum = Double(myValues.reduce(0, { x, y in x + y}))
        print(sum)

        var average = Double((sum/800) * 100)
        print(average)

        lblProgress?.text = ("\(String(average)) +  %")

        progressBar.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("\(String(average)) %")
        }
        progressBar.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar.setProgress(0.7, animated: true, duration: 3.0)

        DBUtil.sharedInstance.addWheelofLife(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputDate: Date(), inputPercentageValue: Int(average))
        print("Insert Success")

    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
