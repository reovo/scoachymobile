//
//  MesTalentsHome1.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 26/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class MesTalentsHome1: UIViewController {
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            var talentSelected : Int64 = 0
            var talentAnswer : Int64 = 0 
            
            for row in try db.prepare("SELECT COUNT (id) FROM talentselected WHERE userId = '\((usersprofileid)!)'"){
                
                talentSelected = row[0] as! Int64
                //print("talent selected")
                //print(talentSelected)
        
            }
            
            for row1 in try db.prepare("SELECT COUNT (id) FROM talentanswer WHERE userId = '\((usersprofileid)!)'"){
                
                talentAnswer = row1[0] as! Int64
                //print("talent answer")
                //print(talentAnswer)
                
            }
            
            if talentSelected == Int64(5) {
                performSegue(withIdentifier: "ledebrief", sender: self)
            }
            
            else if talentAnswer == Int64(360) {
                performSegue(withIdentifier: "lechoix", sender: self)
            }
            
            else {
                performSegue(withIdentifier: "next0", sender: self)
            }
            
            print(talentSelected)
            print(talentAnswer)
            

        }
            
        catch {
            print("Connection to database failed")
            
        }
        
        
        
        
        
        //performSegue(withIdentifier: "next0", sender: self)
        //print("left swiipe")
    }
    
    @IBAction func description(_sender: Any) {
        
        performSegue(withIdentifier: "next", sender: self)
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
