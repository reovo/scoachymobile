//
//  Activity411ViewCell.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 28/03/2018.
//  Copyright © 2018 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class Activity411ViewCell: UITableViewCell {
    
    @IBOutlet weak var valueDescription: UILabel!
    @IBOutlet weak var count: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
