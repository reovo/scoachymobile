//
//  Activity412.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 22/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity412: UIViewController {
    
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    @IBOutlet var button4: UIButton!
    @IBOutlet var button5: UIButton!
    @IBOutlet var button6: UIButton!
    
    @IBOutlet weak var lblValue1: UILabel!
    @IBOutlet weak var lblValue2: UILabel!
    @IBOutlet weak var lblValue3: UILabel!
    @IBOutlet weak var lblValue4: UILabel!
    @IBOutlet weak var lblValue5: UILabel!
    @IBOutlet weak var lblValue6: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var arrayValueDescription = [String]()
    var arrayValues = [Int]()
    
    var arraySelected = [String]()
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1100)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    @IBAction func button_1(_ sender: Any) {
        
//        var itemSelected : String = ""
//
//        button1.backgroundColor = UIColor(red: 236/255.0, green: 102/255.0, blue: 8/255.0, alpha: 1.0)
//
//        if let buttonTitle = (sender as AnyObject).title(for: .normal) {
//            itemSelected = buttonTitle
//        }
//
//        if let index = arraySelected.index(of: itemSelected) {
//            arraySelected.remove(at: index)
//            button1.backgroundColor = UIColor(red: 168/255.0, green: 202/255.0, blue: 20/255.0, alpha: 1.0)
//        }
//
//        else {
//            arraySelected.append(itemSelected)
//        }
//
//        print(arraySelected)
    }
    
    @IBAction func button_2(_ sender: Any) {
        
//        var itemSelected : String = ""
//
//        button2.backgroundColor = UIColor(red: 236/255.0, green: 102/255.0, blue: 8/255.0, alpha: 1.0)
//
//        if let buttonTitle = (sender as AnyObject).title(for: .normal) {
//            itemSelected = buttonTitle
//        }
//
//        if let index = arraySelected.index(of: itemSelected) {
//            arraySelected.remove(at: index)
//            button2.backgroundColor = UIColor(red: 168/255.0, green: 202/255.0, blue: 20/255.0, alpha: 1.0)
//        }
//
//        else {
//            arraySelected.append(itemSelected)
//        }
//
//        print(arraySelected)
    }
    
    @IBAction func button_3(_ sender: Any) {
        
//        var itemSelected : String = ""
//
//        button3.backgroundColor = UIColor(red: 236/255.0, green: 102/255.0, blue: 8/255.0, alpha: 1.0)
//
//        if let buttonTitle = (sender as AnyObject).title(for: .normal) {
//            itemSelected = buttonTitle
//        }
//
//        if let index = arraySelected.index(of: itemSelected) {
//            arraySelected.remove(at: index)
//            button3.backgroundColor = UIColor(red: 168/255.0, green: 202/255.0, blue: 20/255.0, alpha: 1.0)
//        }
//
//        else {
//            arraySelected.append(itemSelected)
//        }
//
//        print(arraySelected)
    }
    
    @IBAction func button_4(_ sender: Any) {
        
//        var itemSelected : String = ""
//
//        button4.backgroundColor = UIColor(red: 236/255.0, green: 102/255.0, blue: 8/255.0, alpha: 1.0)
//
//        if let buttonTitle = (sender as AnyObject).title(for: .normal) {
//            itemSelected = buttonTitle
//        }
//
//        if let index = arraySelected.index(of: itemSelected) {
//            arraySelected.remove(at: index)
//            button4.backgroundColor = UIColor(red: 168/255.0, green: 202/255.0, blue: 20/255.0, alpha: 1.0)
//        }
//
//        else {
//            arraySelected.append(itemSelected)
//        }
//
//        print(arraySelected)
    }
    
    @IBAction func button_5(_ sender: Any) {
        
//        var itemSelected : String = ""
//
//        button5.backgroundColor = UIColor(red: 236/255.0, green: 102/255.0, blue: 8/255.0, alpha: 1.0)
//
//        if let buttonTitle = (sender as AnyObject).title(for: .normal) {
//            itemSelected = buttonTitle
//        }
//
//        if let index = arraySelected.index(of: itemSelected) {
//            arraySelected.remove(at: index)
//            button5.backgroundColor = UIColor(red: 168/255.0, green: 202/255.0, blue: 20/255.0, alpha: 1.0)
//        }
//
//        else {
//            arraySelected.append(itemSelected)
//        }
//
//        print(arraySelected)
    }
    
    @IBAction func button_6(_ sender: Any) {
        
//        var itemSelected : String = ""
//
//        button6.backgroundColor = UIColor(red: 236/255.0, green: 102/255.0, blue: 8/255.0, alpha: 1.0)
//
//        if let buttonTitle = (sender as AnyObject).title(for: .normal) {
//            itemSelected = buttonTitle
//        }
//
//        if let index = arraySelected.index(of: itemSelected) {
//            arraySelected.remove(at: index)
//            button6.backgroundColor = UIColor(red: 168/255.0, green: 202/255.0, blue: 20/255.0, alpha: 1.0)
//        }
//
//        else {
//            arraySelected.append(itemSelected)
//        }
//
//        print(arraySelected)
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        
//        if arraySelected.count > 3 {
//
//            for item in arraySelected{
//
//                DBUtil.sharedInstance.addSelectedValues(inputId: UUID().uuidString, inputValuesDescription: item, inputUserId: usersprofileid!)
//            }
//
//            performSegue(withIdentifier: "next", sender: self)
//
//        }
//
//        else {
//
//            let alertController = UIAlertController(title: "Scoachy", message: "Choississez entre 4 et 6 valeurs les plus importantes à vos yeux", preferredStyle: . alert)
//            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alertController.addAction(defaultAction)
//
//            present(alertController, animated: true, completion: nil)
//        }
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            var userDefaults = UserDefaults.standard
            
            let stmt = try db.prepare("select myvaluesdescription from myvaluesselected")
            
            for row in stmt {
                
                let valueDescription = row[0] as! String
                //let values = row[4] as! Int64
                arrayValueDescription.append(valueDescription)
                //arrayValues.append(Int(values))
                
            }
            
            button1.setTitle(arrayValueDescription[0], for: .normal)
            button2.setTitle(arrayValueDescription[1], for: .normal)
            button3.setTitle(arrayValueDescription[2], for: .normal)
            button4.setTitle(arrayValueDescription[3], for: .normal)
            button5.setTitle(arrayValueDescription[4], for: .normal)
            button6.setTitle(arrayValueDescription[5], for: .normal)
            
//            lblValue1.text = String(arrayValues[0])
//            lblValue2.text = String(arrayValues[1])
//            lblValue3.text = String(arrayValues[2])
//            lblValue4.text = String(arrayValues[3])
//            lblValue5.text = String(arrayValues[4])
//            lblValue6.text = String(arrayValues[5])
            
        }
            
        catch {
            print(error)
            
        }
        
        print(arrayValueDescription)
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
