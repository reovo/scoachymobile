//
//  Activity415.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 26/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class Activity415: UIViewController {
    
    @IBOutlet weak var txtMoteur: UITextField!
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        
        if txtMoteur.text == "" {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez remplir tous les champs", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        else {
            
            DBUtil.sharedInstance.addVerbMoteur(inputId: UUID().uuidString, inputMissionId: "ba311fa6-3bc4-11e8-b467-0ed5f89f718b", inputAnswer: txtMoteur.text!, inputUserId: usersprofileid!)
        
            performSegue(withIdentifier: "next", sender: self)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
