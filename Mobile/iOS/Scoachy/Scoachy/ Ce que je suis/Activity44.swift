//
//  Activity44.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 15/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity44: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var txtEmotion: UITextField!
    @IBOutlet weak var txtEventDescription: UITextField!
    
    var dateSelected = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func datePickerChanged(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let selectedDate = dateFormatter.string(from: sender.date)
        dateSelected = selectedDate
        
    }
    
    func saveImage(image: UIImage) -> String {
        
        let imageData = NSData(data: UIImagePNGRepresentation(image)!)
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory,  FileManager.SearchPathDomainMask.userDomainMask, true)
        let docs = paths[0] as NSString
        let uuid = NSUUID().uuidString + ".png"
        var savedPhoto = UserDefaults.standard.stringArray(forKey: "savedPhotoArray") ?? [String]()
        print(uuid)
        savedPhoto.append(uuid)
        UserDefaults.standard.set(savedPhoto, forKey: "savedPhotoArray")
        let fullPath = docs.appendingPathComponent(uuid)
        _ = imageData.write(toFile: fullPath, atomically: true)
        return uuid
    }
  

    
    @IBAction func importImage(_ sender: AnyObject){
        
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerControllerSourceType.photoLibrary
        image.allowsEditing = false
        
        self.present(image, animated: true){
            //AfTer it is complete
            print("success")
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[String: Any]){
        
        if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imageView.image = selectedImage
            
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
     @IBAction func saveEvent(_ sender: AnyObject){
        
        if imageView.image == nil {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez sélectionner une image", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        else if txtEmotion.text == "" {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez remplir tous les champs", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        else if txtEventDescription.text == "" {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez remplir tous les champs", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        else {
            
            let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
            print(bundlePath ?? "", "\n") //prints the correct path
            let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let fileManager = FileManager.default
            let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
            let fullDestPathString = fullDestPath!.path
            print(fileManager.fileExists(atPath: bundlePath!)) // prints true
            if fileManager.fileExists(atPath: fullDestPathString) {
                print("File is available")
            }else{
                do{
                    try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
                }catch{
                    print("\n")
                    print(error)
                    
                }
            }
            
            
            do {
                let db = try Connection(fullDestPathString, readonly:false)
                
                //var userDefaults = UserDefaults.standard
                //var friendUserId = userDefaults.string(forKey: "friendUserId")!
                
                saveImage(image: imageView.image!)
                
                DBUtil.sharedInstance.addLifeLine(inputId: UUID().uuidString, inputUserId: "123", inputEventDescription: txtEventDescription.text!, inputEventDate: dateSelected, inputCreationDate: Date(), inputEmotion: txtEmotion.text!)
                
                self.performSegue(withIdentifier: "next", sender: self)
                
            }
                
            catch {
                print("Connection to database failed")
                
            }
            
            
        }
        
    }
            

    
    override func viewDidAppear(_ animated: Bool) {
        
        //let array = UserDefaults.standard.array(forKey: "SavedPhoto") as? [NSData]
        
        
        //imageView.image = UIImage(data: array.last! as Data)
        
        
        
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
