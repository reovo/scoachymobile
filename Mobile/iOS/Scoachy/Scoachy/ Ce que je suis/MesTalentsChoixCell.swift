//
//  MesTalentsChoixCell.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 15/03/2018.
//  Copyright © 2018 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class MesTalentsChoixCell: UITableViewCell {
    
    @IBOutlet weak var verb: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
