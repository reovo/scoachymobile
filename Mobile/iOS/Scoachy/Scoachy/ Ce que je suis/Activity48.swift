//
//  Activity48.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 19/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class Activity48: UIViewController {
    
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet var button0: UIButton!
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    @IBOutlet var button4: UIButton!
    
    var savedImages: [UIImage] = [UIImage]()
    var values = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func answer_1(_ sender: AnyObject){
        
        button0.setImage(UIImage(named: "cartouches-orange-0.png"), for: .normal)
        button1.setImage(UIImage(named: "cartouches-9-1.png"), for: .normal)
        button2.setImage(UIImage(named: "cartouches-9-2.png"), for: .normal)
        button3.setImage(UIImage(named: "cartouches-9-3.png"), for: .normal)
        button4.setImage(UIImage(named: "cartouches-9-4.png"), for: .normal)
        
        values = "0"
        print(values)
    }
    
    @IBAction func answer_2(_ sender: AnyObject){
        
        button0.setImage(UIImage(named: "cartouches-9-0.png"), for: .normal)
        button1.setImage(UIImage(named: "cartouches-orange-1.png"), for: .normal)
        button2.setImage(UIImage(named: "cartouches-9-2.png"), for: .normal)
        button3.setImage(UIImage(named: "cartouches-9-3.png"), for: .normal)
        button4.setImage(UIImage(named: "cartouches-9-4.png"), for: .normal)
        
        values = "1"
        print(values)
    
    }
    
    @IBAction func answer_3(_ sender: AnyObject){
        
        button0.setImage(UIImage(named: "cartouches-9-0.png"), for: .normal)
        button1.setImage(UIImage(named: "cartouches-9-1.png"), for: .normal)
        button2.setImage(UIImage(named: "cartouches-orange-2.png"), for: .normal)
        button3.setImage(UIImage(named: "cartouches-9-3.png"), for: .normal)
        button4.setImage(UIImage(named: "cartouches-9-4.png"), for: .normal)
        
        values = "2"
        print(values)
    }
    
    @IBAction func answer_4(_ sender: AnyObject){
        
        button0.setImage(UIImage(named: "cartouches-9-0.png"), for: .normal)
        button1.setImage(UIImage(named: "cartouches-9-1.png"), for: .normal)
        button2.setImage(UIImage(named: "cartouches-9-2.png"), for: .normal)
        button3.setImage(UIImage(named: "cartouches-orange-3.png"), for: .normal)
        button4.setImage(UIImage(named: "cartouches-9-4.png"), for: .normal)
        
        values = "3"
        print(values)
    }
    
    @IBAction func answer_5(_ sender: AnyObject){
        
        button0.setImage(UIImage(named: "cartouches-9-0.png"), for: .normal)
        button1.setImage(UIImage(named: "cartouches-9-1.png"), for: .normal)
        button2.setImage(UIImage(named: "cartouches-9-2.png"), for: .normal)
        button3.setImage(UIImage(named: "cartouches-9-3.png"), for: .normal)
        button4.setImage(UIImage(named: "cartouches-orange-4.png"), for: .normal)
        
        values = "4"
        print(values)
    }
    
    func getImage(imagesNames: [String]) -> [UIImage]{
        
        
        
        for imageName in imagesNames{
            
            if let imagePath = getFilePath(fileName: imageName){
                savedImages.append(UIImage(contentsOfFile: imagePath)!)
            }
        }
        
        return savedImages
    }
    
    func getFilePath(fileName: String) -> String? {
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        var filePath: String?
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if paths.count > 0 {
            let dirPath = paths[0] as NSString
            filePath = dirPath.appendingPathComponent(fileName)
        }
        else {
            filePath = nil
        }
        
        return filePath
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        
        var modelName = UserDefaults.standard.string(forKey: "ModelName")
        
        DBUtil.sharedInstance.addModel(inputId: UUID().uuidString, inputName: modelName!, inputDescription: txtDescription.text!, inputValues: values)
        
        performSegue(withIdentifier: "next", sender: self)
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        var savedPhoto = UserDefaults.standard.stringArray(forKey: "savedPhotoModel") ?? [String]()
        getImage(imagesNames: savedPhoto)
        
        imageView.image = savedImages.last
        
        
        
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
