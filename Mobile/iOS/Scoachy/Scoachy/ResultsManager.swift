//
//  ResultsManager.swift
//  Scoachy
//
//  Created by Jean Paul on 24/11/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import Foundation


class ResultsManager
{
    let resultsSendService = "results/sendflat/"
    let resultsGetService = "resuls/summary/"
    var flatResults: [FlatResultsDto]
    
    private func FetchAnswerResultsForAllUsers()->[FlatResultsDto]
    {
        var flatResultsDto = new [FlatResultsDto]()
        let path = Bundle.main.path(forResource: "scoachy", ofType: "sqlite3")!
        do {
            let db = try Connection(path, readonly: true)
            
            for x in try db.prepare(answerresult).filter( sent = nil || sent = 0){
                var flatResult = FlatResultsDto()
                flatResult.UserId = 
                flatResultsDto.append(x[userId], x[friendUserId], x[answerId], x[lastAnsweredDate], x[questionId])
            }
            print("Fetch Results Successful")
        }
        catch {
            print("Connection to database failed")
        }
    
        return flatResultsDto
    }
    
    public func GetResultsSummaryForUser(userId: String)->[FlatResultsDto]
    {
        //call web service data
        guard let url = WebServiceManager.UrlBuilder(serviceCallAddress: resultsGetService + userId) else { return }
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, _, _) in
            guard let data = data else { return }
            
            do {
                
                let results = try! JSONDecoder().decode([FlatResultsDto].self, from: data)
                
                for res in (results)! {
                    let flatResult: FlatResultsDto
                    flatResult.Id = res.id!
                    flatResult.UserId = res.userId!
                    flatResult.AnswerId = res.answerId!
                    flatResult.LastAnsweredDate = res.lastAnsweredDate!
                    flatResult.QuestionId = res.questionId!
                    self.flatResults.append(flatResult)
                }
                
            } catch {}
        }
        task.resume()
        
        let saved = SaveFlatResultsToInternalDb(flatResults: flatResults)
        return self.flatResults
    }
    
    private func SaveFlatResultsToInternalDb(flatResults: [FlatResultsDto])->Bool
    {
        let path = Bundle.main.path(forResource: "scoachy", ofType: "sqlite3")!
        do {
            let db = try Connection(path, readonly: true)
            var results = db.prepare(answerresult)
            
            for res in flatResults
            {
                do {
                    let rowid =
                    try db.run(
                        answerresult.insert(
                            id <- UUID().uuidString,
                            userId <- res.UserId,
                            friendUserId <- res.FriendUserId,
                            answerId <- res.AnswerId,
                            lastAnsweredDate <- res.LastAnsweredDate,
                            questionId <- res.QuestionId
                        )
                    )
                }
                catch
                {
                    print("insertion failed: \(error)")
                }
            }
            print("Fetch Results Successful")
        }
        catch {
            print("Connection to database failed")
        }
        
        return true
    }
    
    public func SendResults()
    {
        //fetch results for that user from internal db
        let resultsTable = FetchAnswerResultsForAllUsers()
        
        //build object
        let result = WebServiceManager.PostWebServiceBuilder(serviceCallAddress: resultsSendService, jsonData: resultsTable )
        
    }
}
