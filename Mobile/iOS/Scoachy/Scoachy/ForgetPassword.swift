//
//  ForgetPassword.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 07/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class ForgetPassword: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func forgotAccount(_sender: Any){
        
        if txtEmail.text?.isEmpty ?? true {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez remplir tous les champs", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
            return
        }
        
        else{
            guard let url = URL(string: "http://192.168.6.57:8088/account/forgetpassword/") else { return }
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let parameters: [String: Any] = [
                
                "Username": txtEmail.text!,
                "Email": txtEmail.text!,
                "Password": "",
                "Salt": "",
                "ForgetPassword": "true",
                "UserStatus": "0ffc9801-33ee-11e7-9426-c03fd56ee88c",
                "DeviceToken": "",
                "Os": UIDevice.current.model,
                "OsVersion": UIDevice.current.systemVersion,
                
            ]
            
            do {
                let jsonBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                
                request.httpBody = jsonBody
                
            } catch {}
            
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data, _, _) in
                guard let data = data else { return }
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    
                    var users = try! JSONDecoder().decode(CreateAccount.self, from: data)
                    
                    if users.ErrorCode == 0 {
                        
                        DispatchQueue.main.async(execute: {
                            
                            self.performSegue(withIdentifier: "login", sender: self)
                        })
                    }
                        
                    else if users.ErrorCode == 11 {
                        
                        DispatchQueue.main.async(execute: {
                            
                            let alertController = UIAlertController(title: "Scoachy", message: "Cet utilisateur n'existe pas", preferredStyle: .alert)
                            
                            let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                //self.performSegue(withIdentifier: "backToLogin", sender: self)
                            }
                            alertController.addAction(OKAction)
                            
                            self.present(alertController, animated: true, completion:nil)
                            
                        })
                    }
                        
                    else if users.ErrorCode == 13 {
                        
                        DispatchQueue.main.async(execute: {
                            
                            let alertController = UIAlertController(title: "Scoachy", message: "Votre compte n'est pas activé. Veuillez vérifier votre e-mail", preferredStyle: .alert)
                            
                            let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                //self.performSegue(withIdentifier: "backToLogin", sender: self)
                            }
                            alertController.addAction(OKAction)
                            
                            self.present(alertController, animated: true, completion:nil)
                            
                        })
                    }
                        
                    else if users.ErrorCode == 14 {
                        
                        DispatchQueue.main.async(execute: {
                            
                            let alertController = UIAlertController(title: "Scoachy", message: "Votre compte n'est pas activé. Veuillez vérifier votre e-mail", preferredStyle: .alert)
                            
                            let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                //self.performSegue(withIdentifier: "backToLogin", sender: self)
                            }
                            alertController.addAction(OKAction)
                            
                            let ResendAction = UIAlertAction(title: "Ré-envoyer l'email", style: .default) { (action:UIAlertAction!) in
                                
                                print(users.Id)
                                guard let url = URL(string: "http://192.168.6.57:8088/account/resendlink/\((users.Id)!)") else { return }
                                let session = URLSession.shared
                                let task = session.dataTask(with: url) { (data, _, _) in
                                    guard let data = data else { return }
                                    
                                    
                                    do {}
                                    catch{}
                                    
                                    
                                }
                                task.resume()
                                
                            }
                            alertController.addAction(ResendAction)
                            
                            self.present(alertController, animated: true, completion:nil)
                            
                        })
                    }
                     
                
                        
                        
                    
                        
                    
                    else {
                        
                        DispatchQueue.main.async(execute: {
                            let alertController = UIAlertController(title: "Scoachy", message: users.Message, preferredStyle: . alert)
                            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                            alertController.addAction(defaultAction)
                            
                            self.present(alertController, animated: true, completion: nil)
                        })

                    }
                    
                } catch {}
            }
            task.resume()

        }
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
