//
//  SignUpViewController.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 27/11/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import CountryList
import SQLite

class SignUpViewController: UIViewController, CountryListDelegate {
    
    @IBOutlet var handleCountryList: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var segmentedControlGender: UISegmentedControl!
    @IBOutlet weak var segmentedControlNewsletter: UISegmentedControl!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var txtFirstname: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPostal: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
   
    var countryList = CountryList()
    var dateSelected = ""
    var userGender = ""
    var newsletterSelected = "false"
    var userCountry = ""
    var userPrefix = ""
    var userprofileid = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        countryList.delegate = self
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1200)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func handleCountryList(_ sender: Any) {
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    
    
    func selectedCountry(country: Country) {
      
        self.handleCountryList.setTitle("\(country.flag!) \(country.name!), \(country.countryCode), \(country.phoneExtension)", for: .normal)
        userCountry = country.name!
        userPrefix = country.phoneExtension
        
    }
    
    @IBAction func datePickerChanged(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let selectedDate = dateFormatter.string(from: sender.date)
        dateSelected = selectedDate
    
    }
    
    @IBAction func indexChangedGender(sender: UISegmentedControl){
        switch segmentedControlGender.selectedSegmentIndex {
        case 0:
            userGender = "M"
        case 1:
            userGender = "F"
        default:
            break;
        }
    }
    
    @IBAction func indexChangedNewsletter(sender: UISegmentedControl){
        switch segmentedControlNewsletter.selectedSegmentIndex {
        case 0:
            newsletterSelected = "true"
        case 1:
            newsletterSelected = "false"
        default:
            break;
        }
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z._-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    @IBAction func createAccount(_sender: Any){
        
        if txtEmail.text?.isEmpty ?? true {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Une adresse e-mail valide est requise", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
            return
        }
        
        if txtPassword.text?.isEmpty ?? true {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Un mot de passe est requis ", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
            return
        }
        
        if !(txtEmail.text == "") {
            
            let providedEmailAddress = txtEmail.text
            
            let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
            
            if isEmailAddressValid
            {
                let sv = UIViewController.displaySpinner(onView: self.view)
                var encrypt = Encryption()
                var salt = encrypt.RandomGenerator(length: 18)
                var encrpytedPassword = encrypt.Encrypt(plainText: txtPassword.text!, salt: salt)
                
                let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
                print(bundlePath ?? "", "\n") //prints the correct path
                let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
                let fileManager = FileManager.default
                let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
                let fullDestPathString = fullDestPath!.path
                print(fileManager.fileExists(atPath: bundlePath!)) // prints true
                if fileManager.fileExists(atPath: fullDestPathString) {
                    print("File is available")
                }else{
                    do{
                        try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
                    }catch{
                        print("\n")
                        print(error)
                        
                    }
                }
                
                do {
                    let db = try Connection(fullDestPathString, readonly:false)
                    let stmt = try db.run("DELETE FROM userprofile")
                }
                    
                catch {}
                
                guard let url = URL(string: "http://192.168.6.57:8088/account/register/") else { return }
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                
                let parameters: [String: Any] = [
                    
                    "FirstName": txtFirstname.text!,
                    "LastName": txtLastName.text!,
                    "Username": txtEmail.text!,
                    "Gender": userGender,
                    "BirthDate": dateSelected,
                    "PostalCode": txtPostal.text!,
                    "Country": userCountry,
                    "Email": txtEmail.text!,
                    "Language":"Francais",
                    "Phone": txtPhone.text!,
                    "PhonePrefix": userPrefix,
                    "EncryptedPassword": encrpytedPassword,
                    "salt": salt,
                    "ReceiveNewsletter": newsletterSelected,
                    "RegistrationMode":"Email",
                    "Os": UIDevice.current.model,
                    "OsVersion": UIDevice.current.systemVersion,
                    "UserStatus": "eba40fec-1625-11e8-9a8d-000c29a56235",
                    "UserType":"Paid",
                    "AppVersion": "1.01"
                ]
                
                do {
                    let jsonBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                    
                    request.httpBody = jsonBody
                    
                } catch {}
                
                let session = URLSession.shared
                let task = session.dataTask(with: request) { (data, _, _) in
                    guard let data = data else { return }
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print(json)
                        
                        var users = try! JSONDecoder().decode(CreateAccount.self, from: data)
                        

                        if users.ErrorCode == 0 {
                            
                            
                            DispatchQueue.main.async(execute: {
                                //                            self.userprofileid = users.Id!
                                //
                                //
                                //                            DBUtil.sharedInstance.addNewUser(inputId: UUID().uuidString, inputUserProfileId: self.userprofileid, inputNom: self.txtFirstname.text!, inputPrenom: self.txtLastName.text!, inputTelephone: self.txtPhone.text!, inputCountry: self.userCountry, inputPrefix: self.userPrefix, inputEmail: self.txtEmail.text!, inputCodePostal: self.txtPostal.text!, inputGender: self.userGender, inputDob: self.dateSelected, inputNewsletter: self.newsletterSelected)
                                //
                                //
                                //
                                //
                                //                            var userDefaults = UserDefaults.standard
                                //                            userDefaults.set(self.userprofileid, forKey: "userprofileid")
                                //                            var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
                                //
                                //                            userDefaults.set("loggedIn", forKey: "userLoggedIn")
                                UIViewController.removeSpinner(spinner: sv)
                                let alertController = UIAlertController(title: "Scoachy", message: "Veuillez vérifier votre e-mail", preferredStyle: .alert)
                                
                                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                    self.performSegue(withIdentifier: "backToLogin", sender: self)
                                }
                                alertController.addAction(OKAction)
                                
                                self.present(alertController, animated: true, completion:nil)
                                
                            })
                            
                        }
                            
                        else if users.ErrorCode == 15 {
                            
                            DispatchQueue.main.async(execute: {
                                
                                UIViewController.removeSpinner(spinner: sv)
                                let alertController = UIAlertController(title: "Scoachy", message: "Votre compte existe déja mais n'est pas activé. Veuillez vérifier votre e-mail", preferredStyle: .alert)
                                
                                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                    //self.performSegue(withIdentifier: "backToLogin", sender: self)
                                }
                                alertController.addAction(OKAction)
                                
                                self.present(alertController, animated: true, completion:nil)
                                
                            })
                        }
                            
                        else if users.ErrorCode == 8 {
                            
                            DispatchQueue.main.async(execute: {
                                
                                UIViewController.removeSpinner(spinner: sv)
                                let alertController = UIAlertController(title: "Scoachy", message: "Cet utilisateur existe déjà", preferredStyle: .alert)
                                
                                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                    //self.performSegue(withIdentifier: "backToLogin", sender: self)
                                }
                                alertController.addAction(OKAction)
                                
                                self.present(alertController, animated: true, completion:nil)
                                
                            })
                        }
                        
                        else if users.ErrorCode == 17 {
                            
                            DispatchQueue.main.async(execute: {
                                
                                UIViewController.removeSpinner(spinner: sv)
                                let alertController = UIAlertController(title: "Scoachy", message: "Veuillez entrer une adresse email valide", preferredStyle: .alert)
                                
                                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                    //self.performSegue(withIdentifier: "backToLogin", sender: self)
                                }
                                alertController.addAction(OKAction)
                                
                                self.present(alertController, animated: true, completion:nil)
                                
                            })
                        }
                            
                        else if users.ErrorCode == 18 {
                            
                            DispatchQueue.main.async(execute: {
                                
                                UIViewController.removeSpinner(spinner: sv)
                                let alertController = UIAlertController(title: "Scoachy", message: "Opération non autorisée", preferredStyle: .alert)
                                
                                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                    //self.performSegue(withIdentifier: "backToLogin", sender: self)
                                }
                                alertController.addAction(OKAction)
                                
                                self.present(alertController, animated: true, completion:nil)
                                
                            })
                        }
                            
                        else if users.ErrorCode == 16 {
                            
                            DispatchQueue.main.async(execute: {
                                
                                UIViewController.removeSpinner(spinner: sv)
                                let alertController = UIAlertController(title: "Scoachy", message: "Votre compte existe déja mais n'est pas activé. Veuillez vérifier votre e-mail", preferredStyle: .alert)
                                
                                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                    //self.performSegue(withIdentifier: "backToLogin", sender: self)
                                }
                                alertController.addAction(OKAction)
                                
                                let ResendAction = UIAlertAction(title: "Ré-envoyer l'email", style: .default) { (action:UIAlertAction!) in
                                    guard let url = URL(string: "http://192.168.6.57:8088/account/resendlink/\((users.Id)!)") else { return }
                                    let session = URLSession.shared
                                    let task = session.dataTask(with: url) { (data, _, _) in
                                        guard let data = data else { return }
                                        
                                        
                                        do {}
                                        catch{}
                                        
                                        
                                    }
                                    task.resume()
                                }
                                alertController.addAction(ResendAction)
                                
                                self.present(alertController, animated: true, completion:nil)
                                
                            })
                        }
                            
                        else {
                            
                            DispatchQueue.main.async(execute: {
                                
                                UIViewController.removeSpinner(spinner: sv)
                                let alertController = UIAlertController(title: "Scoachy", message: users.Message, preferredStyle: . alert)
                                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                alertController.addAction(defaultAction)
                                
                                self.present(alertController, animated: true, completion: nil)
                            })
                        }
                        
                    } catch {}
                }
                task.resume()
            } else {
                
                //UIViewController.removeSpinner(spinner: sv)
                let alertController = UIAlertController(title: "Scoachy", message: "Veuillez entrer une adresse email valide", preferredStyle: . alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                
                present(alertController, animated: true, completion: nil)
            }
            
        }
        

        
//        if txtFirstname.text?.isEmpty ?? true {
//
//            let alertController = UIAlertController(title: "Scoachy", message: "wawa", preferredStyle: . alert)
//            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alertController.addAction(defaultAction)
//
//            present(alertController, animated: true, completion: nil)
//
//            return
//        }
//
//        else if txtLastName.text?.isEmpty ?? true {
//
//            let alertController = UIAlertController(title: "Scoachy", message: "Last name empty", preferredStyle: . alert)
//            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alertController.addAction(defaultAction)
//
//            present(alertController, animated: true, completion: nil)
//
//            return
//        }

        

//        else if txtPostal.text?.isEmpty ?? true {
//
//            let alertController = UIAlertController(title: "Scoachy", message: "Postal empty", preferredStyle: . alert)
//            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alertController.addAction(defaultAction)
//
//            present(alertController, animated: true, completion: nil)
//
//            return
//        }
//
//        else if txtPhone.text?.isEmpty ?? true {
//
//            let alertController = UIAlertController(title: "Scoachy", message: "Phone empty", preferredStyle: . alert)
//            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alertController.addAction(defaultAction)
//
//            present(alertController, animated: true, completion: nil)
//
//            return
//        }


            
//        else if userCountry == "" {
//
//            let alertController = UIAlertController(title: "Scoachy", message: "Select a Country", preferredStyle: . alert)
//            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alertController.addAction(defaultAction)
//
//            present(alertController, animated: true, completion: nil)
//
//            return
//
//        }
            
//        else if segmentedControlGender.selectedSegmentIndex == UISegmentedControlNoSegment{
//
//            let alertController = UIAlertController(title: "Scoachy", message: "Select a Gender", preferredStyle: . alert)
//            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alertController.addAction(defaultAction)
//
//            present(alertController, animated: true, completion: nil)
//
//            return
//
//        }
            
//        else if segmentedControlNewsletter.selectedSegmentIndex == UISegmentedControlNoSegment{
//            
//            let alertController = UIAlertController(title: "Scoachy", message: "Select a Newsletter", preferredStyle: . alert)
//            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alertController.addAction(defaultAction)
//            
//            present(alertController, animated: true, completion: nil)
//            
//            return
//            
//        }

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
  


        

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}
