//
//  LoginViewController.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 11/09/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class LoginViewController: UIViewController {
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func loginAccount(_sender: Any){
        
        if txtUsername.text?.isEmpty ?? true {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez remplir tous les champs", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
            return
        }
            
        else if txtPassword.text?.isEmpty ?? true {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez remplir tous les champs", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
            return
        }
            
        else {
            
            guard var url = URL(string: "http://192.168.6.57:8088/account/login/") else { return }
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let parameters: [String: Any] = [
                
                "Username": txtUsername.text,
                "Password": txtPassword.text,
                "ForgetPassword": "false",
                "DeviceToken": "",
                "Os": UIDevice.current.model,
                "OsVersion": UIDevice.current.systemVersion,
                "AppVersion": "1.01"
            ]
            
            do {
                var jsonBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                
                request.httpBody = jsonBody
                
            } catch {}
            
            var session = URLSession.shared
            var task = session.dataTask(with: request) { (data, _, _) in
                guard var data = data else { return }
                do {
                    var json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    
                    var users = try! JSONDecoder().decode(CreateAccount.self, from: data)
                    
                    if users.ErrorCode == 0 {
                        
                        DispatchQueue.main.async(execute: {
                            var userDefaults = UserDefaults.standard
                            userDefaults.set(users.Id!, forKey: "userprofileid")
                            var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
                            
                            userDefaults.set("loggedIn", forKey: "userLoggedIn")
                            
                            //Sync User
                            self.syncUser()
                            
                            self.performSegue(withIdentifier: "videoPlayer", sender: self)
                        })
                    }
                        
                    else if users.ErrorCode == 11 {
                        
                        DispatchQueue.main.async(execute: {
                        
                        let alertController = UIAlertController(title: "Scoachy", message: "Cet utilisateur n'existe pas", preferredStyle: .alert)
                        
                        let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                            //self.performSegue(withIdentifier: "backToLogin", sender: self)
                        }
                        alertController.addAction(OKAction)
                        
                        self.present(alertController, animated: true, completion:nil)
                        
                        })
                    }
                        
                    else if users.ErrorCode == 5 {
                        
                        DispatchQueue.main.async(execute: {
                            
                            let alertController = UIAlertController(title: "Scoachy", message: "Cet utilisateur n'existe pas", preferredStyle: .alert)
                            
                            let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                //self.performSegue(withIdentifier: "backToLogin", sender: self)
                            }
                            alertController.addAction(OKAction)
                            
                            self.present(alertController, animated: true, completion:nil)
                            
                        })
                    }
                        
                    else if users.ErrorCode == 12 {
                        
                        DispatchQueue.main.async(execute: {
                            
                            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez mettre à jour votre application pour continuer", preferredStyle: .alert)
                            
                            let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                //self.performSegue(withIdentifier: "backToLogin", sender: self)
                            }
                            alertController.addAction(OKAction)
                            
                            self.present(alertController, animated: true, completion:nil)
                            
                        })
                    }
                        
                    else if users.ErrorCode == 13 {
                        
                        DispatchQueue.main.async(execute: {
                            
                            let alertController = UIAlertController(title: "Scoachy", message: "Votre compte n'est pas activé. Veuillez vérifier votre e-mail", preferredStyle: .alert)
                            
                            let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                //self.performSegue(withIdentifier: "backToLogin", sender: self)
                            }
                            alertController.addAction(OKAction)
                            
                            self.present(alertController, animated: true, completion:nil)
                     
                        })
                    }
                        
                    else if users.ErrorCode == 14 {
                        
                        DispatchQueue.main.async(execute: {
                            
                            let alertController = UIAlertController(title: "Scoachy", message: "Votre compte n'est pas activé. Veuillez vérifier votre e-mail", preferredStyle: .alert)
                            
                            let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                //self.performSegue(withIdentifier: "backToLogin", sender: self)
                            }
                            alertController.addAction(OKAction)
                            
                            let ResendAction = UIAlertAction(title: "Ré-envoyer l'email", style: .default) { (action:UIAlertAction!) in
                                
                                print(users.Id)
                                guard let url = URL(string: "http://192.168.6.57:8088/account/resendlink/\((users.Id)!)") else { return }
                                let session = URLSession.shared
                                let task = session.dataTask(with: url) { (data, _, _) in
                                    guard let data = data else { return }
                                    
                                    
                                    do {}
                                    catch{}
                                    
                                    
                                }
                                task.resume()
                                
                            }
                            alertController.addAction(ResendAction)
                            
                            self.present(alertController, animated: true, completion:nil)
                            
                        })
                    }
                        
                    else if users.ErrorCode == 17 {
                        
                        DispatchQueue.main.async(execute: {
                        
                            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez entrer une adresse email valide", preferredStyle: .alert)
                            
                            let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                //self.performSegue(withIdentifier: "backToLogin", sender: self)
                            }
                            alertController.addAction(OKAction)
                            
                            self.present(alertController, animated: true, completion:nil)
                            
                        })
                    }
                        
                    else if users.ErrorCode == 18 {
                        
                        DispatchQueue.main.async(execute: {
                            
                            let alertController = UIAlertController(title: "Scoachy", message: "Opération non autorisée", preferredStyle: .alert)
                            
                            let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                //self.performSegue(withIdentifier: "backToLogin", sender: self)
                            }
                            alertController.addAction(OKAction)
                            
                            self.present(alertController, animated: true, completion:nil)
                            
                        })
                    }
                        
                    else {
                        
                        DispatchQueue.main.async(execute: {
                            let alertController = UIAlertController(title: "Scoachy", message: users.Message, preferredStyle: . alert)
                            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                            alertController.addAction(defaultAction)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                    }
                    
                } catch {}
            }
            
            task.resume()
        }
        
    }
    
    func syncUser () {
        
        var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        
        guard let url = URL(string: "http://192.168.6.57:8088/scoachy/sync/\((usersprofileid)!)/1") else { return }
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, _, _) in
            guard let data = data else { return }
            
            do {
//                let json = try JSONSerialization.jsonObject(with: data, options: [])
//                print(json)
                
               
                let users = try JSONDecoder().decode(UserInformation.self, from: data)
                
                var dob = users.UserInfo?.BirthDate
                var dateOfBirth = dob?.replacingOccurrences(of: " 00:00:00", with: "")
                
                //Update Table User Profile
                let db = try Connection(fullDestPathString, readonly:false)
                let stmt = try db.run("DELETE FROM userprofile")
                //let stmt4 = try db.run("DELETE FROM answerresult")
                
                var userProfileId = users.UserInfo?.Id
                var userLastName = users.UserInfo?.LastName
                var userFirstName = users.UserInfo?.FirstName
                var userPhone = users.UserInfo?.Phone
                var userCountry = users.UserInfo?.Country
                var userPhonePrefix = users.UserInfo?.PhonePrefix
                var userUsername = users.UserInfo?.Username
                var userPostalCode = users.UserInfo?.PostalCode
                var userGender = users.UserInfo?.Gender
                var userReceiveNewsletter = users.UserInfo?.ReceiveNewsletter
                
                let stml1 = try db.run("INSERT INTO userprofile ('profileId', userProfileId, nom, prenom, telephone, country, prefix, email, codepostal, gender, dob, newsletter) VALUES ('1', '\((userProfileId)!)', '\(userFirstName ?? "")', '\(userLastName ?? "")', '\(userPhone ?? "")', '\(userCountry ?? "")', '\(userPhonePrefix ?? "")', '\((userUsername)!)', '\(userPostalCode ?? "")', '\(userGender ?? "")', '\(dateOfBirth ?? "")', '\((userReceiveNewsletter))')")
                
                for answer in (users.AnswerResults)!{
                    
                   let stmt2 = try db.run("INSERT INTO answerresult ('id', 'userId', 'answerId', 'lastAnsweredDate', 'questionId', 'friendUserId') VALUES ('\((answer.Id)!)', '\((usersprofileid)!)', '\((answer.AnswerId)!)', '\((answer.LastAnsweredDate)!)', '\((answer.QuestionId)!)', '\((answer.FriendUserId)!)')")
                }
            
            } catch {}
        }
        
        task.resume()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

