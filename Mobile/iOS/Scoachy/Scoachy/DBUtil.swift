//
//  DBUtil.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 30/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import Foundation
import SQLite

class DBUtil {
    
    static var sharedInstance = DBUtil()
    var db: Connection?
    
    let answerResult = Table("answerresult")
    let id = Expression<String>("id")
    let userId = Expression<String>("userId")
    let answerId = Expression<String>("answerId")
    let lastAnsweredDate = Expression<Date>("lastAnsweredDate")
    let questionId = Expression<String>("questionId")
    let friendUserId = Expression<String>("friendUserId")
    
    let userProfile = Table("userprofile")
    let upid = Expression<String>("profileId")
    let userProfileId = Expression<String>("userProfileId")
    let nom = Expression<String>("nom")
    let prenom = Expression<String>("prenom")
    let telephone = Expression<String>("telephone")
    let country = Expression<String>("country")
    let prefix = Expression<String>("prefix")
    let email = Expression<String>("email")
    let codepostal = Expression<String>("codepostal")
    let gender = Expression<String>("gender")
    let dob = Expression<String>("dob")
    let newsletter = Expression<String>("newsletter")
    
    let wheelhistory = Table("wheelhistory")
    let wid = Expression<String>("id")
    let wuserid = Expression<String>("userid")
    let wdate = Expression<Date>("date")
    let wpercentagevalue = Expression<Int>("percentagevalue")
    
    let lifeline = Table("lifeline")
    let lid = Expression<String>("id")
    let luserid = Expression<String>("userid")
    let leventdescription = Expression<String>("eventDescription")
    let leventdate = Expression<String>("eventDate")
    let lcreationdate = Expression<Date>("creationDate")
    let lemotion = Expression<String>("emotion")
    
    let model = Table("model")
    let mid = Expression<String>("id")
    let mname = Expression<String>("name")
    let mdescription = Expression<String>("description")
    let mvalues = Expression<String>("values")
    
    let talent = Table("talentanswer")
    let tid = Expression<String>("id")
    let talentVerbId = Expression<String>("talentVerbId")
    let tuserId = Expression<String>("userId")
    let tquestionid = Expression<String>("questionId")
    let tvalue = Expression<Int>("value")
    
    let talentSelected = Table("talentselected")
    let talentSelectedId = Expression<String>("id")
    let talentSelectedVerbId = Expression<String>("verbId")
    let talentSelectedUserId = Expression<String>("userId")
    
    let valuesSelected = Table("myvaluesselected")
    let valuesId = Expression<String>("id")
    let valuesDescription = Expression<String>("myvaluesdescription")
    let valuesUserId = Expression<String>("userId")
    
    let mission = Table("missionanswer")
    let missionId = Expression<String>("id")
    let missionAnswer = Expression<String>("missionid")
    let missionAnswer1 = Expression<String>("answer")
    let missionUserId = Expression<String>("userId")
    
    init() {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do{
            db = try Connection(fullDestPathString, readonly:false)
            print("Connection to database successful")
            
        }
            
        catch{
            
            print(error)
        }
    }
    
    
    func addAnswer(inputId: String, inputUserId: String, inputAnswerId: String, inputQuestionId: String, inputLastAnsweredDate: Date) -> AnyObject {
        
        do{
            let insert = answerResult.insert(id <- inputId, userId <- inputUserId, answerId <- inputAnswerId, questionId <- inputQuestionId, lastAnsweredDate <- inputLastAnsweredDate)
            let xid = try db!.run(insert)
            return xid as AnyObject
        }
            
        catch{
            print("Cannot insert to db")
            return (error as! AnyObject)
        }
        
    }
    
    func addAnswer1(inputId: String, inputUserId: String, inputAnswerId: String, inputQuestionId: String, inputLastAnsweredDate: Date, inputfriendUserId: String) -> AnyObject{
        
        do{
            let insert = answerResult.insert(id <- inputId, userId <- inputUserId, answerId <- inputAnswerId, questionId <- inputQuestionId, lastAnsweredDate <- inputLastAnsweredDate, friendUserId <- inputfriendUserId)
            let xid = try db!.run(insert)
            return xid as AnyObject
        }
            
        catch{
            print("Cannot insert to db")
            return (error as! AnyObject)
        }
        
    }
    
    func addNewUser(inputId: String, inputUserProfileId: String, inputNom: String, inputPrenom: String, inputTelephone: String, inputCountry: String, inputPrefix: String, inputEmail: String, inputCodePostal: String, inputGender: String, inputDob: String, inputNewsletter: String) -> AnyObject {
        
        do{
            let insert = userProfile.insert(upid <- inputId, userProfileId <- inputUserProfileId, nom <- inputNom, prenom <- inputPrenom, telephone <- inputTelephone, country <- inputCountry, prefix <- inputPrefix, email <- inputEmail, codepostal <- inputCodePostal, gender <- inputGender, dob <- inputDob, newsletter <- inputNewsletter)
            let xid = try db!.run(insert)
            print("Insert Success")
            return xid as AnyObject
        }
            
        catch{
            print(error)
            return (error as! AnyObject)
        }
        
    }
    
    func updateUser(inputNom: String, inputPrenom: String, inputTelephone: String, inputCountry: String, inputPrefix: String, inputCodePostal: String, inputGender: String, inputDob: String, inputNewsletter: String) -> AnyObject {
        
        do {
            let update = userProfile.update(nom <- inputNom, prenom <- inputPrenom, telephone <- inputTelephone, country <- inputCountry, prefix <- inputPrefix, codepostal <- inputCodePostal, gender <- inputGender, dob <- inputDob, newsletter <- inputNewsletter)
            let xid = try db!.run(update)
            return xid as AnyObject
        }
        
        catch{
            print(error)
            return (error as! AnyObject)
        }
        
    }
    
    func addWheelofLife(inputId: String, inputUserId: String, inputDate: Date, inputPercentageValue: Int) -> AnyObject{
        
        do{
            let insert = wheelhistory.insert(wid <- inputId, wuserid <- inputUserId, wdate <- inputDate, wpercentagevalue <- inputPercentageValue)
            let xid = try db!.run(insert)
            return xid as AnyObject
        }
            
        catch{
            print("Cannot insert to db")
            return (error as! AnyObject)
        }
        
    }
    
    func addLifeLine(inputId: String, inputUserId: String, inputEventDescription: String, inputEventDate: String, inputCreationDate: Date, inputEmotion: String) -> AnyObject{
        
        do{
            let insert = lifeline.insert(lid <- inputId, luserid <- inputUserId, leventdescription <- inputEventDescription, leventdate <- inputEventDate, lcreationdate <- inputCreationDate, lemotion <- inputEmotion)
            let xid = try db!.run(insert)
            return xid as AnyObject
        }
            
        catch{
            print("Cannot insert to db")
            return (error as! AnyObject)
        }
        
    }
    
    func addTalent(inputTId: String, inputTalentVerbId: String, inputTUserId: String, inputTQuestionId: String, inputValue: Int) -> AnyObject {
        
        do {
            let insert = talent.insert(tid <- inputTId, talentVerbId <- inputTalentVerbId, tuserId <- inputTUserId, tquestionid <- inputTQuestionId, tvalue <- inputValue)
            let xid = try db!.run(insert)
            return xid as AnyObject
        }
        
        catch {
            print("Cannot insert to db")
            return (error as! AnyObject)
        }
    }
    
    func addSelectedTalent(inputId: String, inputTalentSelected: String, inputUserId: String) -> AnyObject {
        
        do {
            let insert = talentSelected.insert(talentSelectedId <- inputId, talentSelectedVerbId <- inputTalentSelected, talentSelectedUserId <- inputUserId)
            let xid = try db!.run(insert)
            return xid as AnyObject
        }
        
        catch {
            print("Cannot insert to db")
            return (error as! AnyObject)
        }
    }
    
    func addSelectedValues(inputId: String, inputValuesDescription: String, inputUserId: String) -> AnyObject{
        
        do{
            
            let insert = valuesSelected.insert(valuesId <- inputId, valuesDescription <- inputValuesDescription, valuesUserId <- inputUserId)
            let xid = try db!.run(insert)
            return xid as AnyObject
            
        }
        
        catch{
            print("Cannot insert to db")
            return (error as! AnyObject)
        }
    }
    
    func addVerbMoteur(inputId: String, inputMissionId: String, inputAnswer: String, inputUserId: String) -> AnyObject {
        
        do {
            let insert = mission.insert(missionId <- inputId, missionAnswer <- inputMissionId, missionAnswer1 <- inputAnswer, missionUserId <- inputUserId)
            let xid = try db!.run(insert)
            return xid as AnyObject
        }
        
        catch {
            print("Cannot insert to db")
            return (error as! AnyObject)
        }
    }
    
    func addModel(inputId: String, inputName: String, inputDescription: String, inputValues: String) -> AnyObject{
        
        do{
            let insert = model.insert(mid <- inputId, mname <- inputName, mdescription <- inputDescription, mvalues <- inputValues)
            let xid = try db!.run(insert)
            return xid as AnyObject
        }
            
        catch{
            print("Cannot insert to db")
            return (error as! AnyObject)
        }
        
    }
    
    
    
}
