//
//  Activity14.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 20/09/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity14: UIViewController {
    
    @IBOutlet weak var lblQuestion: UILabel!
    
    var seasonQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(_ sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(0, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
    }
    
    
    func redirect(action: UIAlertAction){
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func answer_1(_sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "d71db85b-6155-11e7-a7e0-484d7ee0cd26", inputQuestionId: seasonQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
         seasonQuestion.remove(at: 0)
        
        if (seasonQuestion.isEmpty) {
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: redirect)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            lblQuestion.text = seasonQuestion[0].question
        }
        
    }
    
    @IBAction func answer_2(_sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "cc849a33-6152-11e7-a7e0-484d7ee0cd26", inputQuestionId: seasonQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
    
        seasonQuestion.remove(at: 0)
        
        if (seasonQuestion.isEmpty) {
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: redirect)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            lblQuestion.text = seasonQuestion[0].question
        }
        
    }
    
    @IBAction func answer_3(_sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "cc8481fa-6152-11e7-a7e0-484d7ee0cd26", inputQuestionId: seasonQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        seasonQuestion.remove(at: 0)
        
        if (seasonQuestion.isEmpty) {
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: redirect)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            lblQuestion.text = seasonQuestion[0].question
        }
        
    }
    
    @IBAction func answer_4(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "cc849040-6152-11e7-a7e0-484d7ee0cd26", inputQuestionId: seasonQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        seasonQuestion.remove(at: 0)
        
        if (seasonQuestion.isEmpty) {
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: redirect)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            lblQuestion.text = seasonQuestion[0].question
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let season1 = View("season1")
        let orderlist = Expression<Int64>("orderList")
        let question = Expression<String>("question")
        let questionid = Expression<String>("id")
        let description = Expression<String>("description")
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            for row in try db.prepare("SELECT * FROM allquestion where allquestion.activityCategoryId = 'f4c0c96f-2331-11e7-b36e-c03fd56ee88c' and allquestion.name= 'seasonChoice' and allquestion.id  NOT IN (select answerresult.questionId FROM answerresult where answerresult.userId= '\((usersprofileid)!)')"){
                
                let orderlist = row[4] as! Int64
                let questionid = row[0] as! String
                let question = row[6] as! String
                let description = row[7] as! String
                
                seasonQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                
            }
            
            print("Season Question Success")
            
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
        lblQuestion.text = seasonQuestion[0].question
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
