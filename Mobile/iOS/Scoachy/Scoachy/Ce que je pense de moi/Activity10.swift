//
//  Activity10.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 20/09/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity10: UIViewController {
    
    var animalQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblQuestion: UILabel!
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 2600)
        self.navigationController?.isNavigationBarHidden = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(_ sender: Any) {
        
    }
    
    @IBAction func closeButton(_ sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(0, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    @IBAction func animal1(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fc9ba0-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal2(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25f0e740-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal3(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25f2df80-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
    
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal4(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25f30092-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal5(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25f3101f-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal6(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25f31c43-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
    
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal7(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fc82bc-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal8(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fc89be-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal9(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fc8db7-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal10(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fc913c-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal11(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fca995-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal12(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fcb892-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal13(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fcc7cc-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
      
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal14(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fcd5ee-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal15(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fce38a-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal16(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fcf3f2-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal17(_ sender: Any) {
      
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd0121-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal18(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd0eac-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal19(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd1ec0-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal20(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd2c79-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal21(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd39e5-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal22(_ sender: Any) {
    
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd4764-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal23(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd53d2-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal24(_ sender: Any) {
    
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd5ada-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal25(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd6117-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
       
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal26(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd6755-6154-11e7-a7e0-484d7ee0cd266", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal27(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd6b5b-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal28(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd6feb-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal29(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd75a8-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
       
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal30(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd7bef-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal31(_ sender: Any) {
 
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd8105-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal32(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd8495-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal33(_ sender: Any) {
        
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd883d-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal34(_ sender: Any) {
    
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25fd8c50-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal35(_ sender: Any) {
    
        DBUtil.sharedInstance.addAnswer(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "25f2f07a-6154-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date())
        print("Insert Success")
        
        //animalQuestion.remove(at: 0)
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
//        let animal1 = View("animal1")
//        let orderlist = Expression<Int64>("orderList")
//        let question = Expression<String>("question")
//        let questionid = Expression<String>("id")
//        let description = Expression<String>("description")
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            print("fulll")
            print(fullDestPathString)
            
            let stmt = try db.prepare("SELECT * FROM allquestion where allquestion.id= 'ac627cc3-607d-11e7-a7e0-484d7ee0cd26' and allquestion.id  NOT IN (select answerresult.questionId FROM answerresult where answerresult.userId='\((usersprofileid)!)')")
            
            for row in stmt {
                 
                let orderlist = row[4] as! Int64
                let questionid = row[0] as! String
                let question = row[6] as! String
                let description = row[7] as! String
                
                animalQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
            }
            print("Animal Question Success")
            
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
//        var userDefaults = UserDefaults.standard
//        var animalIndex = userDefaults.integer(forKey: "animalIndex")
//        print("ANIMAL INDEX")
        
        lblQuestion.text = animalQuestion[0].question
        print("question")
        print(animalQuestion[0].question)
        print(animalQuestion[0].questionid)
        
    }
    
    
 
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
