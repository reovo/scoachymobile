//
//  Activity414.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 22/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class Activity414: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var txtVision: UITextView!
    
    var savedImages: [UIImage] = [UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getImage(imagesNames: [String]) -> [UIImage]{
        
        for imageName in imagesNames{
            
            if let imagePath = getFilePath(fileName: imageName){
                savedImages.append(UIImage(contentsOfFile: imagePath)!)
            }
        }
        
        return savedImages
    }
    
    func getFilePath(fileName: String) -> String? {
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        var filePath: String?
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if paths.count > 0 {
            let dirPath = paths[0] as NSString
            filePath = dirPath.appendingPathComponent(fileName)
        }
        else {
            filePath = nil
        }
        
        return filePath
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        var savedPhoto = UserDefaults.standard.stringArray(forKey: "savedVision") ?? [String]()
        getImage(imagesNames: savedPhoto)
        
        imageView.image = savedImages.last
        
        var textVision = UserDefaults.standard.string(forKey: "txtVision")!
        txtVision.text = textVision
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
