//
//  Activity423.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 19/03/2018.
//  Copyright © 2018 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity423: UIViewController {
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
    
    var verbSelected = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            for row in try db.prepare("SELECT talentverb.verb FROM talentverb INNER JOIN talentselected ON talentverb.id = talentselected.verbId WHERE talentselected.userId = '\((usersprofileid)!)'"){
                
                let talentverb = row[0] as! String
                
                print("verb selected")
                verbSelected.append(talentverb)
                
            }
            
            label1.text = verbSelected[0]
            label2.text = verbSelected[1]
            label3.text = verbSelected[2]
            label4.text = verbSelected[3]
            label5.text = verbSelected[4]
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
        

        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
