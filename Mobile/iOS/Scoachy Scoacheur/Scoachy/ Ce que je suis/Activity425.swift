//
//  Activity425.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 28/03/2018.
//  Copyright © 2018 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity425: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var arrayValues = [(valueDescription: String, valueCount: Int)]()
    var selectedItems: [Int] = []
    var arrayVerbSelected = [String]()
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrayValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Activity425ViewCell
        
        cell.valueDescription.text = arrayValues[indexPath.row].valueDescription
        //cell.count.text = String(arrayValues[indexPath.row].valueCount)
        
        if selectedItems.contains(indexPath.row) {
            cell.accessoryType = .checkmark
            arrayVerbSelected.append(arrayValues[indexPath.row].valueDescription)
            print(arrayVerbSelected)
            
        } else {
            cell.accessoryType = .none
            print("deselected")
            if let idx = arrayVerbSelected.index(of:"\(arrayValues[indexPath.row].valueDescription)") {
                arrayVerbSelected.remove(at: idx)
            }
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedItems.contains(indexPath.row) {
            let index = selectedItems.index(of: indexPath.row)
            selectedItems.remove(at: index!)
            if let idx = arrayVerbSelected.index(of:"\(arrayValues[indexPath.row].valueDescription)") {
                arrayVerbSelected.remove(at: idx)
            }
            print(arrayVerbSelected)
            
        } else {
            selectedItems.append(indexPath.row)
        }
        
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    @IBAction func suivant(_ sender: Any) {
        
        if arrayVerbSelected.count == 6 {
            
            for item in arrayVerbSelected{
                
                DBUtil.sharedInstance.addSelectedValues(inputId: UUID().uuidString, inputValuesDescription: item, inputUserId: usersprofileid!)
                
    
            }
            print("success")
            performSegue(withIdentifier: "next", sender: self)
        }
            
        else{
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez choisir 6 valeurs", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            var userDefaults = UserDefaults.standard
            
            let stmt = try db.prepare("SELECT * FROM myvalues ORDER BY myvalueanswer DESC")
            
            for row in stmt {
                
                let valueDescription = row[3] as! String
                let values = row[4] as! Int64
                //arrayValueDescription.append(valueDescription)
                //arrayValues.append(Int(values))
                
                arrayValues.append((valueDescription: valueDescription, valueCount: Int(values)))
                
            }
            self.tableView.reloadData()
            print(arrayValues)
            
            
        }
            
        catch {
            print(error)
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
