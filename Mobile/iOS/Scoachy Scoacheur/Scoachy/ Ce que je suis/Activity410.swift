//
//  Activity410.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 20/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity410: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var txt1 : Int = 0
    var txt2 : Int = 0
    var txt3 : Int = 0
    var txt4 : Int = 0
    var txt5 : Int = 0
    var txt6 : Int = 0
    var txt7 : Int = 0
    var txt8 : Int = 0
    var txt9 : Int = 0
    var txt10 : Int = 0
    var txt11 : Int = 0
    var txt12 : Int = 0
    var txt13 : Int = 0
    var txt14 : Int = 0
    var txt15 : Int = 0
    var txt16 : Int = 0
    var txt17 : Int = 0
    var txt18 : Int = 0
    var txt19 : Int = 0
    var txt20 : Int = 0
    var txt21 : Int = 0
    var txt22 : Int = 0
    var txt23 : Int = 0
    var txt24 : Int = 0
    var txt25 : Int = 0
    var txt26 : Int = 0
    var txt27 : Int = 0
    var txt28 : Int = 0
    var txt29 : Int = 0
    var txt30 : Int = 0
    var txt31 : Int = 0
    var txt32 : Int = 0
    var txt33 : Int = 0
    var txt34 : Int = 0
    var txt35 : Int = 0
    var txt36 : Int = 0
    var txt37 : Int = 0
    var txt38 : Int = 0
    var txt39 : Int = 0
    var txt40 : Int = 0
    var txt41 : Int = 0
    var txt42 : Int = 0
    var txt43 : Int = 0
    var txt44 : Int = 0
    var txt45 : Int = 0
    var txt46 : Int = 0
    var txt47 : Int = 0
    var txt48 : Int = 0
    var txt49 : Int = 0
    var txt50 : Int = 0
    var txt51 : Int = 0
    var txt52 : Int = 0
    var txt53 : Int = 0
    var txt54 : Int = 0
    var txt55 : Int = 0
    var txt56 : Int = 0
    var txt57 : Int = 0
    var txt58 : Int = 0
    var txt59 : Int = 0
    var txt60 : Int = 0
    var txt61 : Int = 0
    var txt62 : Int = 0
    var txt63 : Int = 0
    var txt64 : Int = 0
    var txt65 : Int = 0
    var txt66 : Int = 0
    var txt67 : Int = 0
    var txt68 : Int = 0
    var txt69 : Int = 0
    var txt70 : Int = 0
    var txt71 : Int = 0
    var txt72 : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 12000)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var slider1: UILabel!
    
    @IBAction func slider1(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider1.text = value
        txt1 =  Int(slider1.text!)!
    }
    
    @IBOutlet weak var slider2: UILabel!
    
    @IBAction func slider2(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider2.text = value
        txt2 =  Int(slider2.text!)!
    }
    
    @IBOutlet weak var slider3: UILabel!
    
    @IBAction func slider3(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider3.text = value
        txt3 =  Int(slider3.text!)!
    }
    
    @IBOutlet weak var slider4: UILabel!
    
    @IBAction func slider4(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider4.text = value
        txt4 =  Int(slider4.text!)!
    }
    
    @IBOutlet weak var slider5: UILabel!
    
    @IBAction func slider5(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider5.text = value
        txt5 =  Int(slider5.text!)!
    }
    
    @IBOutlet weak var slider6: UILabel!
    
    @IBAction func slider6(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider6.text = value
        txt6 =  Int(slider6.text!)!
    }
    
    @IBOutlet weak var slider7: UILabel!
    
    @IBAction func slider7(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider7.text = value
        txt7 =  Int(slider7.text!)!
    }
    
    @IBOutlet weak var slider8: UILabel!
    
    @IBAction func slider8(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider8.text = value
        txt8 =  Int(slider8.text!)!
    }
    
    @IBOutlet weak var slider9: UILabel!
    
    @IBAction func slider9(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider9.text = value
        txt9 =  Int(slider9.text!)!
    }
    
    @IBOutlet weak var slider10: UILabel!
    
    @IBAction func slider10(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider10.text = value
        txt10 =  Int(slider10.text!)!
    }
    
    @IBOutlet weak var slider11: UILabel!
    
    @IBAction func slider11(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider11.text = value
        txt11 =  Int(slider11.text!)!
    }
    
    @IBOutlet weak var slider12: UILabel!
    
    @IBAction func slider12(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider12.text = value
        txt12 =  Int(slider12.text!)!
    }
    
    @IBOutlet weak var slider13: UILabel!
    
    @IBAction func slider13(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider13.text = value
        txt13 =  Int(slider13.text!)!
    }
    
    @IBOutlet weak var slider14: UILabel!
    
    @IBAction func slider14(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider14.text = value
        txt14 =  Int(slider14.text!)!
    }
    
    @IBOutlet weak var slider15: UILabel!
    
    @IBAction func slider15(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider15.text = value
        txt15 =  Int(slider15.text!)!
    }
    
    @IBOutlet weak var slider16: UILabel!
    
    @IBAction func slider16(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider16.text = value
        txt16 =  Int(slider16.text!)!
    }
    
    @IBOutlet weak var slider17: UILabel!
    
    @IBAction func slider17(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider17.text = value
        txt17 =  Int(slider17.text!)!
    }
    
    @IBOutlet weak var slider18: UILabel!
    
    @IBAction func slider18(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider18.text = value
        txt18 =  Int(slider18.text!)!
    }
    
    @IBOutlet weak var slider19: UILabel!
    
    @IBAction func slider19(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider19.text = value
        txt19 =  Int(slider19.text!)!
    }
    
    @IBOutlet weak var slider20: UILabel!
    
    @IBAction func slider20(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider20.text = value
        txt20 =  Int(slider20.text!)!
    }
    
    @IBOutlet weak var slider21: UILabel!
    
    @IBAction func slider21(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider21.text = value
        txt21 =  Int(slider21.text!)!
    }
    
    @IBOutlet weak var slider22: UILabel!
    
    @IBAction func slider22(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider22.text = value
        txt22 =  Int(slider22.text!)!
    }
    
    @IBOutlet weak var slider23: UILabel!
    
    @IBAction func slider23(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider23.text = value
        txt23 =  Int(slider23.text!)!
    }
    
    @IBOutlet weak var slider24: UILabel!
    
    @IBAction func slider24(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider24.text = value
        txt24 =  Int(slider24.text!)!
    }
    
    @IBOutlet weak var slider25: UILabel!
    
    @IBAction func slider25(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider25.text = value
        txt25 =  Int(slider25.text!)!
    }
    
    @IBOutlet weak var slider26: UILabel!
    
    @IBAction func slider26(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider26.text = value
        txt26 =  Int(slider26.text!)!
    }
    
    @IBOutlet weak var slider27: UILabel!
    
    @IBAction func slider27(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider27.text = value
        txt27 =  Int(slider27.text!)!
    }
    
    @IBOutlet weak var slider28: UILabel!
    
    @IBAction func slider28(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider28.text = value
        txt28 =  Int(slider28.text!)!
    }
    
    @IBOutlet weak var slider29: UILabel!
    
    @IBAction func slider29(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider29.text = value
        txt29 =  Int(slider29.text!)!
    }
    
    @IBOutlet weak var slider30: UILabel!
    
    @IBAction func slider30(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider30.text = value
        txt30 =  Int(slider30.text!)!
    }
    
    @IBOutlet weak var slider31: UILabel!
    
    @IBAction func slider31(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider31.text = value
        txt31 =  Int(slider31.text!)!
    }
    
    @IBOutlet weak var slider32: UILabel!
    
    @IBAction func slider32(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider32.text = value
        txt32 =  Int(slider32.text!)!
    }
    
    @IBOutlet weak var slider33: UILabel!
    
    @IBAction func slider33(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider33.text = value
        txt33 =  Int(slider33.text!)!
    }
    
    @IBOutlet weak var slider34: UILabel!
    
    @IBAction func slider34(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider34.text = value
        txt34 =  Int(slider34.text!)!
    }
    
    @IBOutlet weak var slider35: UILabel!
    
    @IBAction func slider35(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider35.text = value
        txt35 =  Int(slider35.text!)!
    }
    
    @IBOutlet weak var slider36: UILabel!
    
    @IBAction func slider36(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider36.text = value
        txt36 =  Int(slider36.text!)!
    }
    
    @IBOutlet weak var slider37: UILabel!
    
    @IBAction func slider37(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider37.text = value
        txt37 =  Int(slider37.text!)!
    }
    
    @IBOutlet weak var slider38: UILabel!
    
    @IBAction func slider38(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider38.text = value
        txt38 =  Int(slider38.text!)!
    }
    
    @IBOutlet weak var slider39: UILabel!
    
    @IBAction func slider39(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider39.text = value
        txt39 =  Int(slider39.text!)!
    }
    
    @IBOutlet weak var slider40: UILabel!
    
    @IBAction func slider40(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider40.text = value
        txt40 =  Int(slider40.text!)!
    }
    
    @IBOutlet weak var slider41: UILabel!
    
    @IBAction func slider41(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider41.text = value
        txt41 =  Int(slider41.text!)!
    }
    
    @IBOutlet weak var slider42: UILabel!
    
    @IBAction func slider42(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider42.text = value
        txt42 =  Int(slider42.text!)!
    }
    
    @IBOutlet weak var slider43: UILabel!
    
    @IBAction func slider43(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider43.text = value
        txt43 =  Int(slider43.text!)!
    }
    
    @IBOutlet weak var slider44: UILabel!
    
    @IBAction func slider44(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider44.text = value
        txt44 =  Int(slider44.text!)!
    }
    
    @IBOutlet weak var slider45: UILabel!
    
    @IBAction func slider45(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider45.text = value
        txt45 =  Int(slider45.text!)!
    }
    
    @IBOutlet weak var slider46: UILabel!
    
    @IBAction func slider46(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider46.text = value
        txt46 =  Int(slider46.text!)!
    }
    
    @IBOutlet weak var slider47: UILabel!
    
    @IBAction func slider47(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider47.text = value
        txt47 =  Int(slider47.text!)!
    }
    
    @IBOutlet weak var slider48: UILabel!
    
    @IBAction func slider48(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider48.text = value
        txt48 =  Int(slider48.text!)!
    }
    
    @IBOutlet weak var slider49: UILabel!
    
    @IBAction func slider49(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider49.text = value
        txt49 =  Int(slider49.text!)!
    }
    
    @IBOutlet weak var slider50: UILabel!
    
    @IBAction func slider50(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider50.text = value
        txt50 =  Int(slider50.text!)!
    }
    
    @IBOutlet weak var slider52: UILabel!
    
    @IBAction func slider52(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider52.text = value
        txt52 =  Int(slider52.text!)!
    }
    
    @IBOutlet weak var slider53: UILabel!
    
    @IBAction func slider53(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider53.text = value
        txt53 =  Int(slider53.text!)!
    }
    
    @IBOutlet weak var slider54: UILabel!
    
    @IBAction func slider54(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider54.text = value
        txt54 =  Int(slider54.text!)!
    }
    
    @IBOutlet weak var slider55: UILabel!
    
    @IBAction func slider55(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider55.text = value
        txt55 =  Int(slider55.text!)!
    }
    
    @IBOutlet weak var slider56: UILabel!
    
    @IBAction func slider56(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider56.text = value
        txt56 =  Int(slider56.text!)!
    }
    
    @IBOutlet weak var slider57: UILabel!
    
    @IBAction func slider57(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider57.text = value
        txt57 =  Int(slider57.text!)!
    }
    
    @IBOutlet weak var slider58: UILabel!
    
    @IBAction func slider58(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider58.text = value
        txt58 =  Int(slider58.text!)!
    }
    
    @IBOutlet weak var slider59: UILabel!
    
    @IBAction func slider59(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider59.text = value
        txt59 =  Int(slider59.text!)!
    }
    
    @IBOutlet weak var slider60: UILabel!
    
    @IBAction func slider60(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider60.text = value
        txt60 =  Int(slider60.text!)!
    }
    
    @IBOutlet weak var slider61: UILabel!
    
    @IBAction func slider61(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider61.text = value
        txt61 =  Int(slider61.text!)!
    }
    
    @IBOutlet weak var slider62: UILabel!
    
    @IBAction func slider62(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider62.text = value
        txt62 =  Int(slider62.text!)!
    }
    
    @IBOutlet weak var slider63: UILabel!
    
    @IBAction func slider63(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider63.text = value
        txt63 =  Int(slider63.text!)!
    }
    
    @IBOutlet weak var slider64: UILabel!
    
    @IBAction func slider64(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider64.text = value
        txt64 =  Int(slider64.text!)!
    }
    
    @IBOutlet weak var slider65: UILabel!
    
    @IBAction func slider65(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider65.text = value
        txt65 =  Int(slider65.text!)!
    }
    
    @IBOutlet weak var slider66: UILabel!
    
    @IBAction func slider66(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider66.text = value
        txt66 =  Int(slider66.text!)!
    }
    
    @IBOutlet weak var slider67: UILabel!
    
    @IBAction func slider67(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider67.text = value
        txt67 =  Int(slider67.text!)!
    }
    
    @IBOutlet weak var slider68: UILabel!
    
    @IBAction func slider68(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider68.text = value
        txt68 =  Int(slider68.text!)!
    }
    
    @IBOutlet weak var slider69: UILabel!
    
    @IBAction func slider69(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider69.text = value
        txt69 =  Int(slider69.text!)!
    }
    
    @IBOutlet weak var slider70: UILabel!
    
    @IBAction func slider70(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider70.text = value
        txt70 =  Int(slider70.text!)!
    }
    
    @IBOutlet weak var slider71: UILabel!
    
    @IBAction func slider71(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider71.text = value
        txt71 =  Int(slider71.text!)!
    }
    
    @IBOutlet weak var slider72: UILabel!
    
    @IBAction func slider72(_ sender: UISlider) {
        let value = String(Int(sender.value))
        slider72.text = value
        txt72 =  Int(slider72.text!)!
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            var userDefaults = UserDefaults.standard
            //var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")!
            
            let stmt1 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt1)' WHERE id ='1'")
            let stmt2 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt2)' WHERE id ='2'")
            let stmt3 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt3)' WHERE id ='3'")
            let stmt4 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt4)' WHERE id ='4'")
            let stmt5 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt5)' WHERE id ='5'")
            let stmt6 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt6)' WHERE id ='6'")
            let stmt7 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt7)' WHERE id ='7'")
            let stmt8 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt8)' WHERE id ='8'")
            let stmt9 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt9)' WHERE id ='9'")
            let stmt10 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt10)' WHERE id ='10'")
            let stmt11 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt11)' WHERE id ='11'")
            let stmt12 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt12)' WHERE id ='12'")
            let stmt13 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt13)' WHERE id ='13'")
            let stmt14 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt14)' WHERE id ='14'")
            let stmt15 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt15)' WHERE id ='15'")
            let stmt16 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt16)' WHERE id ='16'")
            let stmt17 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt17)' WHERE id ='17'")
            let stmt18 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt18)' WHERE id ='18'")
            let stmt19 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt19)' WHERE id ='19'")
            let stmt20 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt20)' WHERE id ='20'")
            let stmt21 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt21)' WHERE id ='21'")
            let stmt22 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt22)' WHERE id ='22'")
            let stmt23 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt23)' WHERE id ='23'")
            let stmt24 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt24)' WHERE id ='24'")
            let stmt25 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt25)' WHERE id ='25'")
            let stmt26 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt26)' WHERE id ='26'")
            let stmt27 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt27)' WHERE id ='27'")
            let stmt28 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt28)' WHERE id ='28'")
            let stmt29 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt29)' WHERE id ='29'")
            let stmt30 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt30)' WHERE id ='30'")
            let stmt31 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt31)' WHERE id ='31'")
            let stmt32 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt32)' WHERE id ='32'")
            let stmt33 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt33)' WHERE id ='33'")
            let stmt34 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt34)' WHERE id ='34'")
            let stmt35 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt35)' WHERE id ='35'")
            let stmt36 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt36)' WHERE id ='36'")
            let stmt37 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt37)' WHERE id ='37'")
            let stmt38 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt38)' WHERE id ='38'")
            let stmt39 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt39)' WHERE id ='39'")
            let stmt40 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt40)' WHERE id ='40'")
            let stmt41 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt41)' WHERE id ='41'")
            let stmt42 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt42)' WHERE id ='42'")
            let stmt43 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt43)' WHERE id ='43'")
            let stmt44 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt44)' WHERE id ='44'")
            let stmt45 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt45)' WHERE id ='45'")
            let stmt46 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt46)' WHERE id ='46'")
            let stmt47 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt47)' WHERE id ='47'")
            let stmt48 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt48)' WHERE id ='48'")
            let stmt49 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt49)' WHERE id ='49'")
            let stmt50 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt50)' WHERE id ='50'")
            let stmt51 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt51)' WHERE id ='51'")
            let stmt52 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt52)' WHERE id ='52'")
            let stmt53 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt53)' WHERE id ='53'")
            let stmt54 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt54)' WHERE id ='54'")
            let stmt55 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt55)' WHERE id ='55'")
            let stmt56 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt56)' WHERE id ='56'")
            let stmt57 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt57)' WHERE id ='57'")
            let stmt58 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt58)' WHERE id ='58'")
            let stmt59 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt59)' WHERE id ='59'")
            let stmt60 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt60)' WHERE id ='60'")
            let stmt61 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt61)' WHERE id ='61'")
            let stmt62 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt62)' WHERE id ='62'")
            let stmt63 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt63)' WHERE id ='63'")
            let stmt64 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt64)' WHERE id ='64'")
            let stmt65 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt65)' WHERE id ='65'")
            let stmt66 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt66)' WHERE id ='66'")
            let stmt67 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt67)' WHERE id ='67'")
            let stmt68 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt68)' WHERE id ='68'")
            let stmt69 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt69)' WHERE id ='69'")
            let stmt70 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt70)' WHERE id ='70'")
            let stmt71 = try db.run("UPDATE myvalues SET userId = '123', myvalueanswer = '\(txt71)' WHERE id ='71'")
           
            
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
        performSegue(withIdentifier: "next", sender: self)
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
