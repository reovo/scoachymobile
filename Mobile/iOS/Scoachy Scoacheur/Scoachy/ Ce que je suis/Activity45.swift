//
//  Activity45.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 15/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity45: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var savedImages: [UIImage] = [UIImage]()
    
    var arrayEventDescription = [String]()
    var arrayEmotion = [String]()
    var arrayEventDate = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrayEventDescription.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
       
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MaLigneDeVie
        
        cell.eventDescription.text = arrayEventDescription[indexPath.row]
        cell.eventDate.text = arrayEventDate[indexPath.row]
        cell.eventImage.image = savedImages[indexPath.row]
        
        //cell.eventImage.image = UIImage(data: arrayImage[indexPath.row] as Data)
    
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        UserDefaults.standard.set(indexPath.row, forKey: "indexPath")
        performSegue(withIdentifier: "next", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getImage(imagesNames: [String]) -> [UIImage]{
        
        
        
        for imageName in imagesNames{
            
            if let imagePath = getFilePath(fileName: imageName){
                savedImages.append(UIImage(contentsOfFile: imagePath)!)
            }
        }
        
        return savedImages
    }
    
    func getFilePath(fileName: String) -> String? {
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        var filePath: String?
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if paths.count > 0 {
            let dirPath = paths[0] as NSString
            filePath = dirPath.appendingPathComponent(fileName)
        }
        else {
            filePath = nil
        }
        
        return filePath
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        var savedPhoto = UserDefaults.standard.stringArray(forKey: "savedPhotoArray") ?? [String]()
        getImage(imagesNames: savedPhoto)
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            let stmt = try db.prepare("SELECT * FROM lifeline")
            
            for row1 in stmt {
                
                let eventDescription = row1[2] as! String
                arrayEventDescription.append(eventDescription)
                
            }
            
            for row2 in stmt {
                
                let eventDate = row2[3] as! String
                arrayEventDate.append(eventDate)
            }
            
            for row3 in stmt {
                
                let eventEmotion = row3[6] as! String
                arrayEmotion.append(eventEmotion)
            }
            
        
            //imageView.image = UIImage(data: array.last! as Data)
            

            
            self.tableView.reloadData()
        
    
        }
        
        catch {
            print(error)
            
        }
        
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    @IBAction func addEvent(_ sender: AnyObject){
        performSegue(withIdentifier: "back0", sender: self)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
