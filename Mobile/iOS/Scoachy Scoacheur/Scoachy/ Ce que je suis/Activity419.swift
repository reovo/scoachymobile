//
//  Activity419.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 13/03/2018.
//  Copyright © 2018 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity419: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    //var arrayVerb = [(verbid: "a712a1c9-2394-11e8-bb5d-000c29a56235", count: 5), (verbid: "a712a1c9-2394-11e8-bb5d-000c29a56235", count: 5), (verbid: "a712a1c9-2394-11e8-bb5d-000c29a56235", count: 5), (verbid: "a712a134-2394-11e8-bb5d-000c29a56235", count: 2), (verbid: "a712a134-2394-11e8-bb5d-000c29a56235", count: 2), (verbid: "a712a134-2394-11e8-bb5d-000c29a56235", count: 2),  (verbid: "a712a189-2394-11e8-bb5d-000c29a56235", count: 4), (verbid: "a712a189-2394-11e8-bb5d-000c29a56235", count: 4), (verbid: "a712a189-2394-11e8-bb5d-000c29a56235", count: 4)]
    
    var arrayVerb = [(verbid: String, verb:String, count: Int)]()
    var arrayVerbCount = [(verbid: String, verb:String, count: Int)]()
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        for index in stride(from: 0, to: arrayVerb.count, by: 3) {
//
//            print("\(arrayVerb[index].verbid): \((arrayVerb[index].count * arrayVerb[index+1].count * arrayVerb[index+2].count))")
//        }
        
//        for i in stride(from: 0, through: 10, by: 3 ) {
//            print(i)
//        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrayVerbCount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MesTalentsAnswersCell
        
        cell.verb.text = arrayVerbCount[indexPath.row].verb
        cell.count.text = String(arrayVerbCount[indexPath.row].count)
    
        
        return cell
        
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
      @IBAction func suivant(_ sender: Any) {
        performSegue(withIdentifier: "next", sender: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            for row in try db.prepare("SELECT talentanswer.talentVerbId, talentverb.verb, talentanswer.value FROM talentverb INNER JOIN talentanswer ON talentverb.id = talentanswer.talentVerbId AND talentanswer.userId = '\((usersprofileid)!)'"){
                
                let talentverbid = row[0] as! String
                let talentverb = row[1] as! String
                let value = row[2] as! Int64

                arrayVerb.append((verbid: talentverbid, verb:talentverb, count: Int(value)))
                
            }
            
            for index in stride(from: 0, to: arrayVerb.count, by: 3) {
                
                //print("\(arrayVerb[index].verbid): \((arrayVerb[index].count * arrayVerb[index+1].count * arrayVerb[index+2].count))")
                arrayVerbCount.append((verbid: arrayVerb[index].verbid, verb: arrayVerb[index].verb, count:((arrayVerb[index].count * arrayVerb[index+1].count * arrayVerb[index+2].count))))
            }
            
           
            arrayVerbCount = arrayVerbCount.sorted(by: {$0.count > $1.count})
            
            self.tableView.reloadData()
            
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
