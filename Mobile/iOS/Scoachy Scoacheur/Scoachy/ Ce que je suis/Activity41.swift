//
//  Activity41.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 25/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity41: UIViewController {
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var txtLoisir : Int = 0
    var txtSante : Int = 0
    var txtArgent : Int = 0
    var txtPersonnel : Int = 0
    var txtAmour : Int = 0
    var txtFamille : Int = 0
    var txtAmi : Int = 0
    var txtProfessionnel : Int = 0
    
    
    @IBOutlet weak var loisirs: UILabel!
    
    @IBAction func loisirsSlider(_ sender: UISlider) {
        let value = String(Int(sender.value))
        loisirs.text = value 
        txtLoisir =  Int(loisirs.text!)!
    }
    
    @IBOutlet weak var sante: UILabel!
    @IBAction func santeSlider(_ sender: UISlider) {
        let value = String(Int(sender.value))
        sante.text = value
        txtSante = Int(sante.text!)!
    }
    
    @IBOutlet weak var argent: UILabel!
    @IBAction func argentSlider(_ sender: UISlider) {
        let value = String(Int(sender.value))
        argent.text = value
        txtArgent = Int(argent.text!)!
        
    }
    
    @IBOutlet weak var personnel: UILabel!
    @IBAction func personnelSlider(_ sender: UISlider) {
        let value = String(Int(sender.value))
        personnel.text = value
        txtPersonnel = Int(personnel.text!)!
    }
    
    @IBOutlet weak var amour: UILabel!
    @IBAction func amourSlider(_ sender: UISlider) {
        let value = String(Int(sender.value))
        amour.text = value
        txtAmour = Int(amour.text!)!
    }
    
    @IBOutlet weak var famille: UILabel!
    @IBAction func familleSlider(_ sender: UISlider) {
        let value = String(Int(sender.value))
        famille.text = value
        txtFamille = Int(famille.text!)!
    }
    
    @IBOutlet weak var ami: UILabel!
    @IBAction func amiSlider(_ sender: UISlider) {
        let value = String(Int(sender.value))
        ami.text = value
        txtAmi = Int(ami.text!)!
    }
    
    @IBOutlet weak var professionnel: UILabel!
    @IBAction func professionnelSlider(_ sender: UISlider) {
        let value = String(Int(sender.value))
        professionnel.text = value
        txtProfessionnel = Int(professionnel.text!)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
          scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1800)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            var userDefaults = UserDefaults.standard
            //var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")!
            
            let stmt1 = try db.run("UPDATE  wheellife SET myvalues = '\(txtLoisir)', time ='\(Date())', userid='\((usersprofileid)!)' WHERE id = 'bf7ce71a-e0bb-11e7-80c1-9a214cf093ae'")
            let stmt2 = try db.run("UPDATE  wheellife SET myvalues = '\(txtSante)', time ='\(Date())', userid='\((usersprofileid)!)' WHERE id = 'bf7ceabc-e0bb-11e7-80c1-9a214cf093ae'")
            let stmt3 = try db.run("UPDATE  wheellife SET myvalues = '\(txtArgent)', time ='\(Date())', userid='\((usersprofileid)!)' WHERE id = 'bf7cf354-e0bb-11e7-80c1-9a214cf093ae'")
            let stmt4 = try db.run("UPDATE  wheellife SET myvalues = '\(txtPersonnel)', time ='\(Date())', userid='\((usersprofileid)!)' WHERE id = 'bf7cf5de-e0bb-11e7-80c1-9a214cf093ae'")
            let stmt5 = try db.run("UPDATE  wheellife SET myvalues = '\(txtAmour)', time ='\(Date())', userid='\((usersprofileid)!)' WHERE id = 'bf7cf822-e0bb-11e7-80c1-9a214cf093ae'")
            let stmt6 = try db.run("UPDATE  wheellife SET myvalues = '\(txtFamille)', time ='\(Date())', userid='\((usersprofileid)!)' WHERE id = 'bf7cfa52-e0bb-11e7-80c1-9a214cf093ae'")
            let stmt7 = try db.run("UPDATE  wheellife SET myvalues = '\(txtAmi)', time ='\(Date())', userid='\((usersprofileid)!)' WHERE id = 'bf7d001a-e0bb-11e7-80c1-9a214cf093ae'")
            let stmt8 = try db.run("UPDATE  wheellife SET myvalues = '\(txtProfessionnel)', time ='\(Date())', userid='\((usersprofileid)!)' WHERE id = 'bf7d0272-e0bb-11e7-80c1-9a214cf093ae'")
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
        performSegue(withIdentifier: "next", sender: self)
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
