//
//  Activity413.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 22/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class Activity413: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var txtVision: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveImage(image: UIImage) -> String {
        
        let imageData = NSData(data: UIImagePNGRepresentation(image)!)
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory,  FileManager.SearchPathDomainMask.userDomainMask, true)
        let docs = paths[0] as NSString
        let uuid = NSUUID().uuidString + ".png"
        var savedPhoto = UserDefaults.standard.stringArray(forKey: "savedVision") ?? [String]()
        print(uuid)
        savedPhoto.append(uuid)
        UserDefaults.standard.set(savedPhoto, forKey: "savedVision")
        let fullPath = docs.appendingPathComponent(uuid)
        _ = imageData.write(toFile: fullPath, atomically: true)
        return uuid
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[String: Any]){
        
        if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imageView.image = selectedImage
            
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func importImage(_ sender: AnyObject){
        
        var userDefaults = UserDefaults.standard
        userDefaults.set(txtVision.text, forKey: "txtVision")
        
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerControllerSourceType.photoLibrary
        image.allowsEditing = false
        
        self.present(image, animated: true){
            //AfTer it is complete
            print("success")
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func saveVision(_ sender: AnyObject){
        
        if imageView.image == nil {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez sélectionner une image", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        else if txtVision.text == "" {
            
            let alertController = UIAlertController(title: "Scoachy", message: "Veuillez remplir tous les champs", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        else {
            
            var userDefaults = UserDefaults.standard
            userDefaults.set(txtVision.text, forKey: "txtVision")
            
            saveImage(image: imageView.image!)
            performSegue(withIdentifier: "next", sender: self)
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        var textVision = UserDefaults.standard.string(forKey: "txtVision")
        
        txtVision.text = textVision
//        if textVision != nil {
//
//            txtVision.text = textVision
//        }
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
