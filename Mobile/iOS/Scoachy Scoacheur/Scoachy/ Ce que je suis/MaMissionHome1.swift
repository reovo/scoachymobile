//
//  MaMissionHome1.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 26/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class MaMissionHome1: UIViewController {
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            var missionSelected : Int64 = 0
    
            for row in try db.prepare("SELECT COUNT (missionid) FROM missionanswer WHERE userId = '\((usersprofileid)!)'"){
                
                missionSelected = row[0] as! Int64
                
            }
            
            if missionSelected == Int64(1) {
                performSegue(withIdentifier: "cible", sender: self)
            }
                
            else if missionSelected == Int64(2) {
                performSegue(withIdentifier: "mamission", sender: self)
            }
                
            else {
                performSegue(withIdentifier: "next0", sender: self)
            }
            
            print("Mission Selected")
            print(missionSelected)
            
            
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
        
        
        //performSegue(withIdentifier: "next0", sender: self)
        
    }
    
    @IBAction func description(_sender: Any) {
        
        performSegue(withIdentifier: "next", sender: self)
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
