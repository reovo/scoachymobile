//
//  Activity49.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 19/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity49: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var savedImages: [UIImage] = [UIImage]()
    
    var arrayModelName = [String]()
    var arrayModelDescription = [String]()
    var arrayModelValues = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrayModelName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MesModeles
        
        cell.modelName.text = arrayModelName[indexPath.row]
        cell.modelDescription.text = arrayModelDescription[indexPath.row]
        cell.modalImage.image = savedImages[indexPath.row]
        cell.modelValue.text = "\(arrayModelValues[indexPath.row])/5"
        
    
        return cell
        
    }
    
    func getImage(imagesNames: [String]) -> [UIImage]{
        
        for imageName in imagesNames{
            
            if let imagePath = getFilePath(fileName: imageName){
                savedImages.append(UIImage(contentsOfFile: imagePath)!)
            }
        }
        
        return savedImages
    }
    
    func getFilePath(fileName: String) -> String? {
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        var filePath: String?
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if paths.count > 0 {
            let dirPath = paths[0] as NSString
            filePath = dirPath.appendingPathComponent(fileName)
        }
        else {
            filePath = nil
        }
        
        return filePath
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            let stmt = try db.prepare("SELECT * FROM model")
            
            for row1 in stmt {
                
                let modelName = row1[1] as! String
                arrayModelName.append(modelName)
                
            }
            
            for row2 in stmt {
                
                let modelDescription = row2[3] as! String
                arrayModelDescription.append(modelDescription)
            }
            
            for row3 in stmt {
                
                let modelValues = row3[4] as! String
                arrayModelValues.append(modelValues)
            }
            
        }
            
        catch {
            print(error)
            
        }
        
        var savedPhoto = UserDefaults.standard.stringArray(forKey: "savedPhotoModel") ?? [String]()
        getImage(imagesNames: savedPhoto)
        self.tableView.reloadData()
        
        
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
