//
//  Activity418.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 13/03/2018.
//  Copyright © 2018 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity418: UIViewController {
    
    @IBOutlet weak var txtCategory: UILabel!
    @IBOutlet weak var txtVerb: UILabel!
    @IBOutlet weak var txtDescription: UILabel!
    
    @IBOutlet weak var slider1: UILabel!
    @IBOutlet weak var slider2: UILabel!
    @IBOutlet weak var slider3: UILabel!
    
    var txt1 : Int = 0
    var txt2 : Int = 0
    var txt3 : Int = 0
    
    var value1 : Int = 1
    var value2 : Int = 1
    var value3 : Int = 1
    
    var talentVerbs = [(verbid: String, category: String, verb: String, description: String)]()
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
    
    @IBAction func slider1(_ sender: UISlider) {
        var value = String(Int(sender.value))
        value1 = Int(sender.value)
        print(value1)
        slider1.text = value
        txt1 =  Int(slider1.text!)!
    }
    
    @IBAction func slider2(_ sender: UISlider) {
        var value = String(Int(sender.value))
        value2 = Int(sender.value)
        print(value2)
        slider2.text = value
        txt2 =  Int(slider2.text!)!
    }
    
    @IBAction func slider3(_ sender: UISlider) {
        var value = String(Int(sender.value))
        value3 = Int(sender.value)
        print(value3)
        slider3.text = value
        txt3 =  Int(slider3.text!)!
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        
        DBUtil.sharedInstance.addTalent(inputTId: UUID().uuidString, inputTalentVerbId: talentVerbs[0].verbid, inputTUserId: usersprofileid!, inputTQuestionId: "d49a5ea8-26a6-11e8-b467-0ed5f89f718b", inputValue: value1)
        
        DBUtil.sharedInstance.addTalent(inputTId: UUID().uuidString, inputTalentVerbId: talentVerbs[0].verbid, inputTUserId: usersprofileid!, inputTQuestionId: "d49a61aa-26a6-11e8-b467-0ed5f89f718b", inputValue: value2)
        
        DBUtil.sharedInstance.addTalent(inputTId: UUID().uuidString, inputTalentVerbId: talentVerbs[0].verbid, inputTUserId: usersprofileid!, inputTQuestionId: "d49a6308-26a6-11e8-b467-0ed5f89f718b", inputValue: value3)
        
        talentVerbs.remove(at: 0)
        
        if(talentVerbs.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                self.performSegue(withIdentifier: "next", sender: self)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
        else {
            txtCategory.text = talentVerbs[0].category
            txtVerb.text = talentVerbs[0].verb
            txtDescription.text = talentVerbs[0].description
        }
        
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)

            for row in try db.prepare("SELECT  tv.id, C.`category` AS CATEGORY, tv.verb AS VERB, C. `description` AS DESCRIPTION FROM talentverb tv LEFT JOIN talentcategory C ON tv.`categoryId` = C.Id WHERE tv.`id` NOT IN (SELECT talentVerbId FROM talentanswer where talentanswer.userId = '\((usersprofileid)!)') ORDER BY C.`category`, tv.VERB;"){

                let verbid = row[0] as! String
                let category = row[1] as! String
                let verb = row[2] as! String
                let description = row[3] as! String
    
                talentVerbs.append((verbid: verbid, category: category, verb: verb, description: description))
            
        }
            
            print("Talent Question Success")
            print(talentVerbs)
            print(talentVerbs[0].category)
            print(talentVerbs[0].verb)
            
            if(talentVerbs.isEmpty){
                let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                    self.performSegue(withIdentifier: "next", sender: self)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion: nil)
            }
            
            txtCategory.text = talentVerbs[0].category
            txtVerb.text = talentVerbs[0].verb
            txtDescription.text = talentVerbs[0].description
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
