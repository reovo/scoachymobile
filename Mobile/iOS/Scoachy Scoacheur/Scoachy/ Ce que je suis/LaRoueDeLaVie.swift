//
//  LaRoueDeLaVie.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 14/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import CircleProgressBar

class LaRoueDeLaVie: UITableViewCell {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var progress: UILabel!
    @IBOutlet weak var progressBar: CircleProgressBar!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        progressBar.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar.setProgress(0.7, animated: true, duration: 3.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
