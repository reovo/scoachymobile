//
//  Activity411.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 22/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity411: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!

    //var arrayValueDescription = [String]()
    //var arrayValues = [Int]()
    
    var arrayValues = [(valueDescription: String, valueCount: Int)]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func next(_sender: Any) {
        
        performSegue(withIdentifier: "next", sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrayValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Activity411ViewCell
        
        cell.valueDescription.text = arrayValues[indexPath.row].valueDescription
        cell.count.text = String(arrayValues[indexPath.row].valueCount)
        
        
        return cell
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            var userDefaults = UserDefaults.standard
            
            let stmt = try db.prepare("SELECT * FROM myvalues ORDER BY myvalueanswer DESC")
            
            for row in stmt {
                
                let valueDescription = row[3] as! String
                let values = row[4] as! Int64
                //arrayValueDescription.append(valueDescription)
                //arrayValues.append(Int(values))
                
                arrayValues.append((valueDescription: valueDescription, valueCount: Int(values)))
      
            }
            self.tableView.reloadData()
            print(arrayValues)
            
        
        }
            
        catch {
            print(error)
            
        }
        
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
