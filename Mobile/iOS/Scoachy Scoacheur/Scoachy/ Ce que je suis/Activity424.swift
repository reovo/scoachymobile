//
//  Activity424.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 21/03/2018.
//  Copyright © 2018 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity424: UIViewController {
    
    @IBOutlet weak var lblValue1: UILabel!
    @IBOutlet weak var lblValue2: UILabel!
    @IBOutlet weak var lblValue3: UILabel!
    @IBOutlet weak var lblValue4: UILabel!
    @IBOutlet weak var lblValue5: UILabel!
    @IBOutlet weak var lblValue6: UILabel!
    
    var valuesSelected = [String]()
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            for row in try db.prepare(" SELECT myvaluesdescription FROM myvaluesselected where userId = '\((usersprofileid)!)'"){
                
                let valSelected = row[0] as! String
                
                print("values selected")
                valuesSelected.append(valSelected)
                
            }
            
            if valuesSelected.count == 4 {
                
                lblValue1.text = valuesSelected[0]
                lblValue2.text = valuesSelected[1]
                lblValue3.text = valuesSelected[2]
                lblValue4.text = valuesSelected[3]
                
                lblValue5.isHidden = true
                lblValue6.isHidden = true
            }
            
            else if valuesSelected.count == 5 {
                
                lblValue1.text = valuesSelected[0]
                lblValue2.text = valuesSelected[1]
                lblValue3.text = valuesSelected[2]
                lblValue4.text = valuesSelected[3]
                lblValue5.text = valuesSelected[4]
                
                lblValue6.isHidden = true
            }
            
            else {
                
                lblValue1.text = valuesSelected[0]
                lblValue2.text = valuesSelected[1]
                lblValue3.text = valuesSelected[2]
                lblValue4.text = valuesSelected[3]
                lblValue5.text = valuesSelected[4]
                lblValue6.text = valuesSelected[5]
            }
            
            print(valuesSelected)
            
//            label1.text = verbSelected[0]
//            label2.text = verbSelected[1]
//            label3.text = verbSelected[2]
//            label4.text = verbSelected[3]
//            label5.text = verbSelected[4]
        }
            
        catch {
            print("Connection to database failed")
            
        }
        
        
        
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
