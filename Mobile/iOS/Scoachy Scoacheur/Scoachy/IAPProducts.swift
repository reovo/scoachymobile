//
//  IAPProducts.swift
//  Scoachy
//
//  Created by Jean Paul on 29/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import Foundation

enum IAPProducts: String {
   
    case nonConsumable = "com.lcc.scoachygratuit.ScoachyNonConsumable"
    
}
