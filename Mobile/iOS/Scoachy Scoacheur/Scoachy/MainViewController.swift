//
//  MainViewController.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 09/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    var tabs = [
        ViewPagerTab(title: "\nCE QUE\n JE PENSE\n DE MOI", image: UIImage(named: "fries")),
        ViewPagerTab(title: "\nCE QUE\n JE PENSE\n D'EUX", image: UIImage(named: "hamburger")),
        ViewPagerTab(title: "\nCE QU'ILS\n PENSENT\n DE MOI", image: UIImage(named: "pint")),
        ViewPagerTab(title: "\nCE QUE\n JE SUIS", image: UIImage(named: "pizza"))
    ]
    
    var viewPager:ViewPagerController!
    
    override func viewDidAppear(_ animated: Bool) {
        
        var x = UserDefaults.standard.object(forKey: "segmentIndex")
        viewPager.displayViewController(atIndex: x as! Int)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        
        let options = ViewPagerOptions(viewPagerWithFrame: self.view.bounds)
        options.tabType = ViewPagerTabType.basic
        options.tabViewHeight = 70
        options.tabViewTextFont = UIFont(name: "Farah", size: 13.0)!
        options.isEachTabEvenlyDistributed = true
        options.tabViewPaddingLeft = 20
        options.tabViewPaddingRight = 20
        options.isTabHighlightAvailable = false
        options.tabIndicatorViewBackgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1.0)
        options.tabViewBackgroundDefaultColor = UIColor(red:255, green:247, blue:246, alpha:1.0)
        
        
        viewPager = ViewPagerController()
        viewPager.options = options
        viewPager.dataSource = self
        viewPager.delegate = self
        
        self.addChildViewController(viewPager)
        self.view.addSubview(viewPager.view)
        viewPager.didMove(toParentViewController: self)
        
        viewPager.displayViewController(atIndex: 0)
        
    }
    
}


extension MainViewController: ViewPagerControllerDataSource {
    
    func numberOfPages() -> Int {
        return tabs.count
    }
    
    
    func viewControllerAtPosition(position: Int) -> UIViewController {
        
        if position == 0{
            print ("Position 0")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CeQueJePenseDeMoi") as! CeQueJePenseDeMoi
            return vc
        }
        
        if position == 1{
            print ("Position 1")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CeQueJePenseDeux") as! CeQueJePenseDeux
            return vc
        }
        
        if position == 2{
            print ("Position 2")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CeQuIlsPensentDeMoi") as! CeQuIlsPensentDeMoi
            return vc
        }
        
        if position == 3{
            print ("Position 3")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CeQueJeSuis") as! CeQueJeSuis
            return vc
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CeQueJePenseDeMoi") as! CeQuIlsPensentDeMoi
        return vc
        
        
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return tabs
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
}

extension MainViewController: ViewPagerControllerDelegate {
    
    func willMoveToControllerAtIndex(index:Int) {
        print("Moving to page \(index)")
        
    }
    
    func didMoveToControllerAtIndex(index: Int) {
        print("Moved to page \(index)")
    }
}

