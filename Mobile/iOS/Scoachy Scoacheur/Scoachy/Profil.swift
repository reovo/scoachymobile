//
//  Profil.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 08/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import CircleProgressBar

class Profil: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var progressBar1: CircleProgressBar!
    @IBOutlet weak var progressBar2: CircleProgressBar!
    @IBOutlet weak var progressBar3: CircleProgressBar!
    @IBOutlet weak var progressBar4: CircleProgressBar!
    
    @IBOutlet weak var monprofil: UILabel!
    @IBOutlet weak var quisommesnous: UILabel!
    @IBOutlet weak var video: UILabel!
    @IBOutlet weak var logut: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 2800)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        progressBar1.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar1.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar1.setProgress(1.0, animated: true, duration: 3.0)
        
        progressBar2.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar2.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar2.setProgress(1.0, animated: true, duration: 3.0)
        
        progressBar3.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar3.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar3.setProgress(1.0, animated: true, duration: 3.0)
        
        progressBar4.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar4.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar4.setProgress(1.0, animated: true, duration: 3.0)
        
        //UI Label Gesture Recognizer
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(Profil.tapMonProfil))
        monprofil.isUserInteractionEnabled = true
        monprofil.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(Profil.tapQueSommesNous))
        quisommesnous.isUserInteractionEnabled = true
        quisommesnous.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(Profil.tapVideo))
        video.isUserInteractionEnabled = true
        video.addGestureRecognizer(tap3)
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(Profil.tapLogout))
        logut.isUserInteractionEnabled = true
        logut.addGestureRecognizer(tap4)
        
    }
    
    @objc func tapMonProfil(sender:UITapGestureRecognizer) {
        performSegue(withIdentifier: "profil", sender: self)
    }
    
    @objc func tapQueSommesNous(sender:UITapGestureRecognizer){
        performSegue(withIdentifier: "cgu", sender: self)
    }
    
    @objc func tapVideo(sender:UITapGestureRecognizer) {
        performSegue(withIdentifier: "videoplayer", sender: self)
    }
    
    @objc func tapLogout(sender:UITapGestureRecognizer){
        
        var userDefaults = UserDefaults.standard
        userDefaults.set("loggedOut", forKey: "userLoggedIn")
        userDefaults.set(nil, forKey: "friendUserName")
        userDefaults.set(nil, forKey: "txtSaisie")
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(newViewController, animated: true, completion: nil)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
