//
//  Activity36.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 20/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity36: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    @IBOutlet var button4: UIButton!
    @IBOutlet var button5: UIButton!
    @IBOutlet var button6: UIButton!
    
    @IBOutlet var button7: UIButton!
    @IBOutlet var button8: UIButton!
    @IBOutlet var button9: UIButton!
    @IBOutlet var button10: UIButton!
    @IBOutlet var button11: UIButton!
    @IBOutlet var button12: UIButton!

    var fortesCompetences = [String] ()
    var faiblesCompetences = [String] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 2100)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        performSegue(withIdentifier: "next3", sender: self)
    }
    
    @IBAction func rightSwipe(_ sender: UISwipeGestureRecognizer) {
         performSegue(withIdentifier: "back2", sender: self)
    }
    
    @IBAction func closeButton(_ sender: Any) {
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(2, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
    }
    
     override func viewDidAppear(_ animated: Bool) {
        
        var userDefaults = UserDefaults.standard
        var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            let stmt1 = try db.run("select r.answerId as answerId ,r.questionId as questionId,  o.ratingtypeId as ratingtypeId , o.description as description,  a.description as rate FROM answerresult r, allquestion o, answer a WHERE r.questionId = o.id AND r.userid = '\((usersprofileid)!)' AND o.ratingtypeId ='d2cccf4b-233d-11e7-b36e-c03fd56ee88c' AND o.ratingtypeId = a.ratingtypeId GROUP BY r.answerId order by rate desc limit 6")
            
            let stmt2 = try db.run(" select r.answerId as answerId ,r.questionId as questionId, count(r.answerId) as cntresult, o.ratingtypeId as ratingtypeId , o.description as description FROM resultsummary r, allquestion_other o WHERE r.questionId = o.id AND r.userid = '\((usersprofileid)!)' AND o.ratingtypeId = 'd2cccf4b-233d-11e7-b36e-c03fd56ee88c' GROUP BY r.answerId order by cntresult desc limit 6")
            
            for row in stmt1 {
                
                let answer = row[3] as! String
                fortesCompetences.append(answer)
            }
            
            for row2 in stmt2 {
                
                let answer1 = row2[4] as! String
                faiblesCompetences.append(answer1)
            }
        }
        
        catch {
            print("Connection to database failed")
            
        }
        
        //
        var faiblesCompetencesCount = faiblesCompetences.count
        
        if (faiblesCompetencesCount == 1){
            var text = faiblesCompetences[0]
            button7.setTitle(text, for: .normal)
        }
        
        if (faiblesCompetencesCount == 2){
            var text = faiblesCompetences[0]
            var text1 = faiblesCompetences[1]
            button7.setTitle(text, for: .normal)
            button8.setTitle(text1, for: .normal)
        }
        
        if (faiblesCompetencesCount == 3){
            var text = faiblesCompetences[0]
            var text1 = faiblesCompetences[1]
            var text2 = faiblesCompetences[2]
            button7.setTitle(text, for: .normal)
            button8.setTitle(text1, for: .normal)
            button9.setTitle(text2, for: .normal)
        }
        
        if (faiblesCompetencesCount == 4){
            var text = faiblesCompetences[0]
            var text1 = faiblesCompetences[1]
            var text2 = faiblesCompetences[2]
            var text3 = faiblesCompetences[3]
            button7.setTitle(text, for: .normal)
            button8.setTitle(text1, for: .normal)
            button9.setTitle(text2, for: .normal)
            button10.setTitle(text3, for: .normal)
        }
        
        if (faiblesCompetencesCount == 5){
            var text = faiblesCompetences[0]
            var text1 = faiblesCompetences[1]
            var text2 = faiblesCompetences[2]
            var text3 = faiblesCompetences[3]
            var text4 = faiblesCompetences[4]
            button7.setTitle(text, for: .normal)
            button8.setTitle(text1, for: .normal)
            button9.setTitle(text2, for: .normal)
            button10.setTitle(text3, for: .normal)
            button11.setTitle(text4, for: .normal)
        }
        
        if (faiblesCompetencesCount == 6){
            var text = faiblesCompetences[0]
            var text1 = faiblesCompetences[1]
            var text2 = faiblesCompetences[2]
            var text3 = faiblesCompetences[3]
            var text4 = faiblesCompetences[4]
            var text5 = faiblesCompetences[5]
            button7.setTitle(text, for: .normal)
            button8.setTitle(text1, for: .normal)
            button9.setTitle(text2, for: .normal)
            button10.setTitle(text3, for: .normal)
            button11.setTitle(text4, for: .normal)
            button12.setTitle(text5, for: .normal)
        }
        
        //
        
        print(fortesCompetences)
        var fortesCompetencesCount = fortesCompetences.count
        
        if (fortesCompetencesCount == 1){
            var text6 = fortesCompetences[0]
            button1.setTitle(text6, for: .normal)
        }
        
        if (fortesCompetencesCount == 2){
            var text6 = fortesCompetences[0]
            var text7 = fortesCompetences[1]
            button1.setTitle(text6, for: .normal)
            button2.setTitle(text7, for: .normal)
        }
        
        if (fortesCompetencesCount == 3){
            var text6 = fortesCompetences[0]
            var text7 = fortesCompetences[1]
            var text8 = fortesCompetences[2]
            button1.setTitle(text6, for: .normal)
            button2.setTitle(text7, for: .normal)
            button3.setTitle(text8, for: .normal)
        }
        
        if (fortesCompetencesCount == 4){
            var text6 = fortesCompetences[0]
            var text7 = fortesCompetences[1]
            var text8 = fortesCompetences[2]
            var text9 = fortesCompetences[3]
            button1.setTitle(text6, for: .normal)
            button2.setTitle(text7, for: .normal)
            button3.setTitle(text8, for: .normal)
            button4.setTitle(text9, for: .normal)
        }
        
        if (fortesCompetencesCount == 5){
            var text6 = fortesCompetences[0]
            var text7 = fortesCompetences[1]
            var text8 = fortesCompetences[2]
            var text9 = fortesCompetences[3]
            var text10 = fortesCompetences[4]
            button1.setTitle(text6, for: .normal)
            button2.setTitle(text7, for: .normal)
            button3.setTitle(text8, for: .normal)
            button4.setTitle(text9, for: .normal)
            button5.setTitle(text10, for: .normal)
        }
        
        if (fortesCompetencesCount == 6){
            var text6 = fortesCompetences[0]
            var text7 = fortesCompetences[1]
            var text8 = fortesCompetences[2]
            var text9 = fortesCompetences[3]
            var text10 = fortesCompetences[4]
            var text11 = fortesCompetences[5]
            button1.setTitle(text6, for: .normal)
            button2.setTitle(text7, for: .normal)
            button3.setTitle(text8, for: .normal)
            button4.setTitle(text9, for: .normal)
            button5.setTitle(text10, for: .normal)
            button6.setTitle(text11, for: .normal)
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
