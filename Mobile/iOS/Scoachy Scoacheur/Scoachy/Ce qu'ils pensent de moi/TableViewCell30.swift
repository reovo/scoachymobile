//
//  TableViewCell30.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 10/11/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit

class TableViewCell30: UITableViewCell {
    
    @IBOutlet weak var userStatus: UIImageView!
    @IBOutlet weak var invitedDate: UILabel!
    @IBOutlet weak var name: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
