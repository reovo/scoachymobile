//
//  CeQueJeSuis.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 08/09/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite
import CircleProgressBar

class CeQueJeSuis: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var progressBar1: CircleProgressBar!
    @IBOutlet weak var progressBar2: CircleProgressBar!
    @IBOutlet weak var progressBar3: CircleProgressBar!
    @IBOutlet weak var progressBar4: CircleProgressBar!
    @IBOutlet weak var progressBar5: CircleProgressBar!
    @IBOutlet weak var progressBar6: CircleProgressBar!
    @IBOutlet weak var progressBar7: CircleProgressBar!
    
    @IBOutlet weak var larouedelavie: UILabel!
    @IBOutlet weak var malignedevie: UILabel!
    @IBOutlet weak var mesmodeles: UILabel!
    @IBOutlet weak var mesvaleurs: UILabel!
    @IBOutlet weak var mavision: UILabel!
    @IBOutlet weak var mestalents: UILabel!
    @IBOutlet weak var mamission: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 2500)
        
        IAPService.shared.getProducts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        progressBar1.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar1.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar1.setProgress(1.00, animated: true, duration: 3.0)
        
        progressBar2.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar2.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar2.setProgress(1.00, animated: true, duration: 3.0)
        
        progressBar3.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar3.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar3.setProgress(1.00, animated: true, duration: 3.0)
        
        progressBar4.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar4.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar4.setProgress(1.00, animated: true, duration: 3.0)
        
        progressBar5.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar5.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar5.setProgress(1.00, animated: true, duration: 3.0)
        
        progressBar6.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar6.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar6.setProgress(1.00, animated: true, duration: 3.0)
        
        progressBar7.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar7.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar7.setProgress(1.00, animated: true, duration: 3.0)
        
        //UI Label Gesture Recognizer
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(CeQueJeSuis.taplarouedelavie))
        larouedelavie.isUserInteractionEnabled = true
        larouedelavie.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(CeQueJeSuis.tapmalignedevie))
        malignedevie.isUserInteractionEnabled = true
        malignedevie.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(CeQueJeSuis.tapmesmodeles))
        mesmodeles.isUserInteractionEnabled = true
        mesmodeles.addGestureRecognizer(tap3)
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(CeQueJeSuis.tapmesvaleurs))
        mesvaleurs.isUserInteractionEnabled = true
        mesvaleurs.addGestureRecognizer(tap4)
        
        let tap5 = UITapGestureRecognizer(target: self, action: #selector(CeQueJeSuis.tapmavision))
        mavision.isUserInteractionEnabled = true
        mavision.addGestureRecognizer(tap5)
        
        let tap6 = UITapGestureRecognizer(target: self, action: #selector(CeQueJeSuis.tapmestalents))
        mestalents.isUserInteractionEnabled = true
        mestalents.addGestureRecognizer(tap6)
        
        let tap7 = UITapGestureRecognizer(target: self, action: #selector(CeQueJeSuis.tapmamission))
        mamission.isUserInteractionEnabled = true
        mamission.addGestureRecognizer(tap7)
        
    }
    
    @objc func taplarouedelavie(sender:UITapGestureRecognizer) {
        
        var userDefaults = UserDefaults.standard
        var userStatus = UserDefaults.standard.string(forKey: "userStatus")
        
        if !(userStatus == "purchased") {
            
            let alertController = UIAlertController(title: "Scoachy", message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Debloquez l'application", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.purchase(product: .nonConsumable)
            }
            alertController.addAction(OKAction)
            
            let purchaseAction = UIAlertAction(title: "Restaurer l'achat", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.restorePurchases()
            }
            alertController.addAction(purchaseAction)
            
            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else{
            performSegue(withIdentifier: "taplarouedelavie", sender: self)
            print("taplarouedelavie")
        }
    }
    
    @objc func tapmalignedevie(sender:UITapGestureRecognizer) {
        
        var userDefaults = UserDefaults.standard
        var userStatus = UserDefaults.standard.string(forKey: "userStatus")
        
        if !(userStatus == "purchased") {
            
            let alertController = UIAlertController(title: "Scoachy", message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Debloquez l'application", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.purchase(product: .nonConsumable)
            }
            alertController.addAction(OKAction)
            
            let purchaseAction = UIAlertAction(title: "Restaurer l'achat", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.restorePurchases()
            }
            alertController.addAction(purchaseAction)
            
            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else{
            
            performSegue(withIdentifier: "tapmalignedevie", sender: self)
            print("tapmalignedevie")
        }
    }
    
    @objc func tapmesmodeles(sender:UITapGestureRecognizer) {
        
        var userDefaults = UserDefaults.standard
        var userStatus = UserDefaults.standard.string(forKey: "userStatus")
        
        if !(userStatus == "purchased") {
            
            let alertController = UIAlertController(title: "Scoachy", message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Debloquez l'application", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.purchase(product: .nonConsumable)
            }
            alertController.addAction(OKAction)
            
            let purchaseAction = UIAlertAction(title: "Restaurer l'achat", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.restorePurchases()
            }
            alertController.addAction(purchaseAction)
            
            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else{
            
            performSegue(withIdentifier: "tapmesmodeles", sender: self)
            print("tapmesmodeles")
        }
    }
    
    @objc func tapmesvaleurs(sender:UITapGestureRecognizer) {
        
        var userDefaults = UserDefaults.standard
        var userStatus = UserDefaults.standard.string(forKey: "userStatus")
        
        if !(userStatus == "purchased") {
            
            let alertController = UIAlertController(title: "Scoachy", message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Debloquez l'application", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.purchase(product: .nonConsumable)
            }
            alertController.addAction(OKAction)
            
            let purchaseAction = UIAlertAction(title: "Restaurer l'achat", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.restorePurchases()
            }
            alertController.addAction(purchaseAction)
            
            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else{
            
            performSegue(withIdentifier: "tapmesvaleurs", sender: self)
            print("tapmesvaleurs")
        }
    }
    
    @objc func tapmavision(sender:UITapGestureRecognizer) {
        
        var userDefaults = UserDefaults.standard
        var userStatus = UserDefaults.standard.string(forKey: "userStatus")
        
        if !(userStatus == "purchased") {
            
            let alertController = UIAlertController(title: "Scoachy", message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Debloquez l'application", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.purchase(product: .nonConsumable)
            }
            alertController.addAction(OKAction)
            
            let purchaseAction = UIAlertAction(title: "Restaurer l'achat", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.restorePurchases()
            }
            alertController.addAction(purchaseAction)
            
            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else{
            
            performSegue(withIdentifier: "tapmavision", sender: self)
            print("tapmavision")
        }
    }
    
    @objc func tapmestalents(sender:UITapGestureRecognizer) {
        
        var userDefaults = UserDefaults.standard
        var userStatus = UserDefaults.standard.string(forKey: "userStatus")
        
        if !(userStatus == "purchased") {
            
            let alertController = UIAlertController(title: "Scoachy", message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Debloquez l'application", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.purchase(product: .nonConsumable)
            }
            alertController.addAction(OKAction)
            
            let purchaseAction = UIAlertAction(title: "Restaurer l'achat", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.restorePurchases()
            }
            alertController.addAction(purchaseAction)
            
            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else{
            
            performSegue(withIdentifier: "tapmestalents", sender: self)
            print("tapmestalents")
        }
    }
    
    @objc func tapmamission(sender:UITapGestureRecognizer) {
        
        var userDefaults = UserDefaults.standard
        var userStatus = UserDefaults.standard.string(forKey: "userStatus")
        
        if !(userStatus == "purchased") {
            
            let alertController = UIAlertController(title: "Scoachy", message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Debloquez l'application", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.purchase(product: .nonConsumable)
            }
            alertController.addAction(OKAction)
            
            let purchaseAction = UIAlertAction(title: "Restaurer l'achat", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.restorePurchases()
            }
            alertController.addAction(purchaseAction)
            
            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else{
            
            performSegue(withIdentifier: "tapmamission", sender: self)
            print("tapmamission")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
