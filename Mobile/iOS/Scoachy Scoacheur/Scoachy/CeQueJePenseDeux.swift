//
//  CeQueJePenseDeux.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 08/09/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite
import CircleProgressBar


class CeQueJePenseDeux: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var progressBar1: CircleProgressBar!
    @IBOutlet weak var progressBar2: CircleProgressBar!
    @IBOutlet weak var progressBar3: CircleProgressBar!
    @IBOutlet weak var progressBar4: CircleProgressBar!
    @IBOutlet weak var progressBar5: CircleProgressBar!
    @IBOutlet weak var progressBar6: CircleProgressBar!
    @IBOutlet weak var progressBar7: CircleProgressBar!
    @IBOutlet weak var progressBar8: CircleProgressBar!
    
    @IBOutlet weak var animal: UILabel!
    @IBOutlet weak var attributes: UILabel!
    @IBOutlet weak var element: UILabel!
    @IBOutlet weak var competences: UILabel!
    @IBOutlet weak var season: UILabel!
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var color: UILabel!
    @IBOutlet weak var motivation: UILabel!
    
    @IBOutlet weak var friendUserName: UIButton!
    
    var animalQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var attributesQuestion = [(questionid:String, question: String, description: String)]()
    
    var elementQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var competencesQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var seasonQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var roleQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var colorQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var motivationQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 2600)
        
        friendUserName.backgroundColor = .clear
        friendUserName.layer.cornerRadius = 5
        friendUserName.layer.borderWidth = 1
        friendUserName.layer.borderColor = UIColor(red: 0/255.0, green: 170/255.0, blue: 167/255.0, alpha: 1.0).cgColor
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        
        var friendUser = UserDefaults.standard.string(forKey: "friendUserName")
        friendUserName.setTitle(friendUser, for: .normal)
        
        if (friendUser == nil) {

            progressBar1.isUserInteractionEnabled = false
            progressBar2.isUserInteractionEnabled = false
            progressBar3.isUserInteractionEnabled = false
            progressBar4.isUserInteractionEnabled = false
            progressBar5.isUserInteractionEnabled = false
            progressBar6.isUserInteractionEnabled = false
            progressBar7.isUserInteractionEnabled = false
            progressBar8.isUserInteractionEnabled = false
            
            let alertController = UIAlertController(title: "Scoachy", message: "Scoachy Scoacheur peut être utilisé UNIQUEMENT sur invitation de vos amis. Pour bénéficier de toutes les fonctionnalités, vous devez acheter la version complète", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        //Circular Bar
        
        progressBar1.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar1.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar1.setProgress(0.01, animated: true, duration: 3.0)
        
        progressBar2.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar2.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar2.setProgress(0.01, animated: true, duration: 3.0)
        
        progressBar3.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar3.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar3.setProgress(0.01, animated: true, duration: 3.0)
        
        progressBar4.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar4.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar4.setProgress(0.01, animated: true, duration: 3.0)
        
        progressBar5.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar5.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar5.setProgress(0.01, animated: true, duration: 3.0)
        
        progressBar6.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar6.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar6.setProgress(0.01, animated: true, duration: 3.0)
        
        progressBar7.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar7.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar7.setProgress(0.01, animated: true, duration: 3.0)
        
        progressBar8.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar8.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar8.setProgress(0.01, animated: true, duration: 3.0)
        
        //UI Label Gesture Recognizer
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapAnimal))
        animal.isUserInteractionEnabled = true
        animal.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapAttributes))
        attributes.isUserInteractionEnabled = true
        attributes.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapElement))
        element.isUserInteractionEnabled = true
        element.addGestureRecognizer(tap3)
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapCompetences))
        competences.isUserInteractionEnabled = true
        competences.addGestureRecognizer(tap4)
        
        let tap5 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapSeason))
        season.isUserInteractionEnabled = true
        season.addGestureRecognizer(tap5)
        
        let tap6 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapRole))
        role.isUserInteractionEnabled = true
        role.addGestureRecognizer(tap6)
        
        let tap7 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapColor))
        color.isUserInteractionEnabled = true
        color.addGestureRecognizer(tap7)
        
        let tap8 = UITapGestureRecognizer(target: self, action: #selector(CeQueJePenseDeMoi.tapMotivation))
        motivation.isUserInteractionEnabled = true
        motivation.addGestureRecognizer(tap8)
        
        //DB Connection
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            
            let db = try Connection(fullDestPathString, readonly:false)
            
            var userDefaults = UserDefaults.standard
            var friendUserId = userDefaults.string(forKey: "friendUserId")
            
            if friendUserId == nil {
                
                print("No friend selected")
            }
            
            else {
                
                var friendUser = userDefaults.string(forKey: "friendUserId")!
        
                for row in try db.prepare("SELECT * FROM allquestion_other where allquestion_other.name= 'animalChoice' and allquestion_other.id NOT IN(select answerresult.questionId FROM answerresult where answerresult.friendUserId='\(friendUser)')"){
                    
                    let orderlist = row[4] as! Int64
                    let questionid = row[0] as! String
                    let question = row[6] as! String
                    let description = row[7] as! String
                    
                    animalQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                    
                }
                
                for row in try db.prepare("SELECT question.id, question.description AS DESCRIPTION, question.question, attribute.name AS ATTRIBUTENAME, activitycategory.id, ratingtype.name AS RNAME FROM question INNER JOIN ratingtype ON question.ratingTypeId = ratingtype.id INNER JOIN activity ON question.activityId = activity.id INNER JOIN attribute ON question.attributeId = attribute.id INNER JOIN activitycategory ON question.activityCategoryId = activitycategory.id WHERE activitycategory.id ='f33bfd80-2331-11e7-b36e-c03fd56ee88c' AND RNAME = 'attributeChoice' AND question.id NOT IN(select answerresult.questionId FROM answerresult where answerresult.friendUserId='\(friendUser)') ORDER BY ATTRIBUTENAME, DESCRIPTION"){

                    //let orderlist = row[4] as! Int64
                    let questionid = row[0] as! String
                    let question = row[2] as! String
                    let description = row[1] as! String
                    
                    attributesQuestion.append((questionid: questionid, question: question, description: description))
                    
                }
                
                for row in try db.prepare("SELECT * FROM allquestion_other where allquestion_other.name='elementChoice' and allquestion_other.id NOT IN(select answerresult.questionId FROM answerresult where answerresult.friendUserId='\(friendUser)')"){
                    
                    let orderlist = row[4] as! Int64
                    let questionid = row[0] as! String
                    let question = row[6] as! String
                    let description = row[7] as! String
                    
                   elementQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                    
                }
                
                for row in try db.prepare("SELECT * FROM allquestion_other where allquestion_other.name='competenceChoice' and allquestion_other.id NOT IN(select answerresult.questionId FROM answerresult where answerresult.friendUserId='\(friendUser)')"){
                    
                    let orderlist = row[4] as! Int64
                    let questionid = row[0] as! String
                    let question = row[6] as! String
                    let description = row[7] as! String
                    
                    competencesQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                    
                }
                
                for row in try db.prepare("SELECT * FROM allquestion_other where allquestion_other.name='seasonChoice' and allquestion_other.id NOT IN(select answerresult.questionId FROM answerresult where answerresult.friendUserId='\(friendUser)')"){
                    
                    let orderlist = row[4] as! Int64
                    let questionid = row[0] as! String
                    let question = row[6] as! String
                    let description = row[7] as! String
                    
                    seasonQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                    
                }
                
                for row in try db.prepare("SELECT * FROM allquestion_other where allquestion_other.name='roleChoice' and allquestion_other.id NOT IN(select answerresult.questionId FROM answerresult where answerresult.friendUserId='\(friendUser)')"){
                    
                    let orderlist = row[4] as! Int64
                    let questionid = row[0] as! String
                    let question = row[6] as! String
                    let description = row[7] as! String
                    
                    roleQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                    
                }
                
                for row in try db.prepare("SELECT * FROM allquestion_other where allquestion_other.name='colorChoice' and allquestion_other.id NOT IN(select answerresult.questionId FROM answerresult where answerresult.friendUserId='\(friendUser)')"){
                    
                    let orderlist = row[4] as! Int64
                    let questionid = row[0] as! String
                    let question = row[6] as! String
                    let description = row[7] as! String
                    
                    colorQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                    
                }
                
                for row in try db.prepare("SELECT * FROM allquestion_other where allquestion_other.name='motivationChoice' and allquestion_other.id NOT IN(select answerresult.questionId FROM answerresult where answerresult.friendUserId='\(friendUser)')"){
                    
                    let orderlist = row[4] as! Int64
                    let questionid = row[0] as! String
                    let question = row[6] as! String
                    let description = row[7] as! String
                    
                    motivationQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                    
                }
                
            }
        }
            
        catch {
            print("Database Error")
            
        }
        
    }
    
    @objc func tapAnimal(sender:UITapGestureRecognizer) {
        
        if(animalQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            performSegue(withIdentifier: "animalChoice2", sender: self)
        }
    }
    
    @objc func tapAttributes(sender:UITapGestureRecognizer){
        
        if(attributesQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            performSegue(withIdentifier: "attributeChoice2", sender: self)
        }
    }
    
    @objc func tapElement(sender:UITapGestureRecognizer){
        if(elementQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            performSegue(withIdentifier: "elementChoice2", sender: self)
        }
    }
    
    @objc func tapCompetences(sender:UITapGestureRecognizer){
        if(competencesQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            performSegue(withIdentifier: "competenceChoice2", sender: self)
        }
    }
    
    @objc func tapSeason(sender:UITapGestureRecognizer){
        if(seasonQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            performSegue(withIdentifier: "seasonChoice2", sender: self)
        }
    }
    
    @objc func tapRole(sender:UITapGestureRecognizer){
        if(roleQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            performSegue(withIdentifier: "roleChoice2", sender: self)
        }
    }
    
    @objc func tapColor(sender:UITapGestureRecognizer){
        if(colorQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            performSegue(withIdentifier: "colorChoice2", sender: self)
        }
    }
    
    @objc func tapMotivation(sender:UITapGestureRecognizer){
        if(motivationQuestion.isEmpty){
            let alertController = UIAlertController(title: "Scoachy", message: "Vous avez fini l'activité", preferredStyle: . alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else {
            performSegue(withIdentifier: "motivationChoice2", sender: self)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
