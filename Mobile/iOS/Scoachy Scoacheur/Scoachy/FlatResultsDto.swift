//
//  ShareMultipleResultsDto.swift
//  Scoachy
//
//  Created by Jean Paul on 24/11/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import Foundation


struct FlatResultsDto: Decodable
{
    var Id : String
    var UserId: String
    var FriendUserId : String
    var AnswerId : String
    var LastAnsweredDate : String
    var QuestionId : String
}

