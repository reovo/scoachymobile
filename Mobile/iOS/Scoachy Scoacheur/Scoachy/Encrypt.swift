//
//  Encrypt.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 28/11/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import Foundation

class Encryption
{
    public func Encrypt(plainText: String, salt: String)->String
    {
        let plainBytes = [UInt8](plainText.data(using:String.Encoding.utf8)!)
        let saltBytes = [UInt8](salt.data(using:String.Encoding.utf8)!)
        let mixedBytes = MixBytes(bytes1: plainBytes, bytes2: saltBytes)
        let textToEncrypt = String(bytes: mixedBytes, encoding: .utf8)
        let shaData = sha256(textToEncrypt:textToEncrypt!)
        let finalString = HexString(shaData:shaData)
        return finalString
    }

    private func sha256(textToEncrypt: String) -> Data? {
        guard let messageData = textToEncrypt.data(using:String.Encoding.utf8) else { return nil }
        var digestData = Data(count: Int(CC_SHA256_DIGEST_LENGTH))

        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_SHA256(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        return digestData
    }

    public func RandomGenerator(length: Int) -> String
    {
        //Valid Range : 48-57(0-9), 65-90(A-Z)
        var x: UInt8
        var num :UInt8
        var count = 0
        var bytesArray = [UInt8]()

        while (count < length)
        {
            x = UInt8(arc4random_uniform(_:2))
            if x == 0
            {
                num = 48 + UInt8(arc4random_uniform(_:10))
            }
            else
            {
                num = 65 + UInt8(arc4random_uniform(_:26))
            }
            bytesArray.append(num)
            count+=1
        }
        let randomString = String(bytes: bytesArray, encoding: String.Encoding.utf8)!
        return randomString
    }

    private func HexString(shaData: Data?) -> String
    {
        let shaHex =  shaData!.map { String(format: "%02hhx", $0) }.joined()
        return shaHex
    }

    private func MixBytes(bytes1: [UInt8], bytes2: [UInt8])-> [UInt8]
    {
        let bytes1Length = bytes1.count
        let bytes2Length = bytes2.count
        let totalLength = bytes1Length + bytes2Length
        var mixedBytes = [UInt8]()
        var a = 0
        var b = 0
        var x = 0

        while x < totalLength
        {
            if a < bytes1Length
            {
                mixedBytes.append(bytes1[a])
                x+=1
                a+=1
            }
            if  b < bytes2Length
            {
                mixedBytes.append(bytes2[b])
                x+=1
                b+=1
            }
        }

        return mixedBytes
    }
}

