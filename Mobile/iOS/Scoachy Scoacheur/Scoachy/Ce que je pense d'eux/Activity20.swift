//
//  Activity20.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 10/10/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity20: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblQuestion: UILabel!
    
     var animalQuestion = [(orderlist: Int, questionid:String, question: String, description: String)]()
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 2600)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(_ sender: Any) {
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(1, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
    }
    
    @IBAction func animal1(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e653d4-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal2(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e48d7e-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal3(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e5dbe7-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal4(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        
    
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e5f6b9-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
    
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal5(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e605c2-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal6(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e61492-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal7(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e62221-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal8(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e62d68-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal9(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
    
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e63a4a-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal10(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e64685-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal11(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e66160-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal12(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e66ca1-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal13(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
    
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6719e-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal14(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e67697-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal15(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
    
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e67b84-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal16(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e67ee9-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal17(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e68429-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal18(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e68c1f-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal19(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e68fb4-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
    
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal20(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e69366-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
    
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal21(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e69d70-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal22(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6a3de-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal23(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6aaec-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal24(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6b153-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal25(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6b73d-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal26(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6c25e-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal27(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
    
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6c90f-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal28(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6cee9-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
    
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal29(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6d37f-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal30(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6d82c-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal31(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6dcbc-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal32(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6e03e-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
    
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal33(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6e517-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
    
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal34(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e6e517-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
    
        performSegue(withIdentifier: "end", sender: nil)
    }
    
    @IBAction func animal35(_ sender: Any) {
        
        var userDefaults = UserDefaults.standard
        var friendUserId = userDefaults.string(forKey: "friendUserId")
        
        DBUtil.sharedInstance.addAnswer1(inputId: UUID().uuidString, inputUserId: usersprofileid!, inputAnswerId: "20e5eabb-614d-11e7-a7e0-484d7ee0cd26", inputQuestionId: animalQuestion[0].questionid, inputLastAnsweredDate: Date(), inputfriendUserId: friendUserId!)
        print("Insert Success")
        
        performSegue(withIdentifier: "end", sender: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        do {
            
            let db = try Connection(fullDestPathString, readonly:false)
            
            var userDefaults = UserDefaults.standard
            var friendUserId = userDefaults.string(forKey: "friendUserId")!
            
            let stmt = try db.prepare("SELECT * FROM allquestion_other where allquestion_other.name= 'animalChoice' and allquestion_other.id NOT IN (select answerresult.questionId FROM answerresult where answerresult.friendUserId='\(friendUserId)')")
 
            for row in stmt {
                
                let orderlist = row[4] as! Int64
                let questionid = row[0] as! String  
                let question = row[6] as! String
                let description = row[7] as! String
                
                animalQuestion.append((orderlist: Int(orderlist), questionid: questionid, question: question, description: description))
                }
            
            print("Animal Question Success")
            
        }
            
        catch {
            print("Connection to database failed")
            
        }

        lblQuestion.text = animalQuestion[0].question
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
