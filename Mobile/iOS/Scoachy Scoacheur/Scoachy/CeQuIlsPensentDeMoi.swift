//
//  CeQuIlsPensentDeMoi.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 08/09/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import CircleProgressBar
import SQLite

class CeQuIlsPensentDeMoi: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var progressBar1: CircleProgressBar!
    @IBOutlet weak var progressBar2: CircleProgressBar!
    @IBOutlet weak var progressBar3: CircleProgressBar!
    @IBOutlet weak var progressBar4: CircleProgressBar!
    
    @IBOutlet weak var contacts: UILabel!
    @IBOutlet weak var demandes: UILabel!
    @IBOutlet weak var activites: UILabel!
    @IBOutlet weak var debrief: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1700)
        
        // Hide Navigation Bar
        self.navigationController?.isNavigationBarHidden = true
        IAPService.shared.getProducts()
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        syncResult()
        
        progressBar1.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar1.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar1.setProgress(1.0, animated: true, duration: 3.0)
        
        progressBar2.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar2.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar2.setProgress(1.0, animated: true, duration: 3.0)
        
        progressBar3.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar3.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar3.setProgress(1.0, animated: true, duration: 3.0)
        
        progressBar4.setHintTextGenerationBlock { (progress) -> String? in
            return String.init("")
        }
        progressBar4.hintTextFont = UIFont.boldSystemFont(ofSize: 13)
        progressBar4.setProgress(1.0, animated: true, duration: 3.0)
        
        //UI Label Gesture Recognizer
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(CeQuIlsPensentDeMoi.tapContacts))
        contacts.isUserInteractionEnabled = true
        contacts.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(CeQuIlsPensentDeMoi.tapDemandes))
        demandes.isUserInteractionEnabled = true
        demandes.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(CeQuIlsPensentDeMoi.tapActivites))
        activites.isUserInteractionEnabled = true
        activites.addGestureRecognizer(tap3)
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(CeQuIlsPensentDeMoi.tapDebrief))
        debrief.isUserInteractionEnabled = true
        debrief.addGestureRecognizer(tap4)
        
    }
    
    @objc func tapContacts(sender:UITapGestureRecognizer) {
        
        var userDefaults = UserDefaults.standard
        var userStatus = UserDefaults.standard.string(forKey: "userStatus")
        
        if !(userStatus == "purchased") {
            
            let alertController = UIAlertController(title: "Scoachy", message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Debloquez l'application", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.purchase(product: .nonConsumable)
            }
            alertController.addAction(OKAction)
            
            let purchaseAction = UIAlertAction(title: "Restaurer l'achat", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.restorePurchases()
            }
            alertController.addAction(purchaseAction)
            
            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else{
            performSegue(withIdentifier: "mescontacts", sender: self)
        }
    }
    
    @objc func tapDemandes(sender:UITapGestureRecognizer){
        
        var userDefaults = UserDefaults.standard
        var userStatus = UserDefaults.standard.string(forKey: "userStatus")
        
        if !(userStatus == "purchased") {
            
            let alertController = UIAlertController(title: "Scoachy", message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Debloquez l'application", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.purchase(product: .nonConsumable)
            }
            alertController.addAction(OKAction)
            
            let purchaseAction = UIAlertAction(title: "Restaurer l'achat", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.restorePurchases()
            }
            alertController.addAction(purchaseAction)
            
            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else{
            performSegue(withIdentifier: "relances", sender: self)
        }
    }
    
    @objc func tapActivites(sender:UITapGestureRecognizer){
        
        var userDefaults = UserDefaults.standard
        var userStatus = UserDefaults.standard.string(forKey: "userStatus")
        
        if !(userStatus == "purchased") {
            
            let alertController = UIAlertController(title: "Scoachy", message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Debloquez l'application", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.purchase(product: .nonConsumable)
            }
            alertController.addAction(OKAction)
            
            let purchaseAction = UIAlertAction(title: "Restaurer l'achat", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.restorePurchases()
            }
            alertController.addAction(purchaseAction)
            
            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else{
            performSegue(withIdentifier: "activites", sender: self)
        }
    }
    
    @objc func tapDebrief(sender:UITapGestureRecognizer){
        
        var userDefaults = UserDefaults.standard
        var userStatus = UserDefaults.standard.string(forKey: "userStatus")
        
        if !(userStatus == "purchased") {
            
            let alertController = UIAlertController(title: "Scoachy", message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Debloquez l'application", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.purchase(product: .nonConsumable)
            }
            alertController.addAction(OKAction)
            
            let purchaseAction = UIAlertAction(title: "Restaurer l'achat", style: .default) { (action:UIAlertAction!) in
                IAPService.shared.restorePurchases()
            }
            alertController.addAction(purchaseAction)
            
            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else{
            performSegue(withIdentifier: "debrief", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //67ba5a10-67a0-4260-b70d-f5d9093afb42
    //http://192.168.6.57/scoachy/sync/67ba5a10-67a0-4260-b70d-f5d9093afb42/1
    
    func syncResult(){
        
        var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        guard let url = URL(string: "http://scoachybackend.com:8080/results/summary/\((usersprofileid)!)") else { return }
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, _, _) in
            guard let data = data else { return }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
                
                let users = try JSONDecoder().decode([AnswerResults].self, from: data)
                let db = try Connection(fullDestPathString, readonly:false)
            
                
                for user in users {
                    
                   let stmt1 = try db.run("INSERT INTO resultsummary ('id', 'friendUserId', 'userId', 'answerId', 'lastAnsweredDate', 'questionId') VALUES ('\((user.Id)!)', '\((user.FriendUserId)!)', '\((usersprofileid)!)', '\((user.AnswerId)!)', '\((user.LastAnsweredDate)!)', '\((user.QuestionId)!)')")
                
                }
            
                }
                
            catch {}
        }
        
        task.resume()
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
