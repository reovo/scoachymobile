//
//  ViewController.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 06/09/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import MXSegmentedPager

class ViewController: MXSegmentedPagerController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentedPager.backgroundColor = .white
        
        // Parallax Header
        segmentedPager.parallaxHeader.mode = .fill
        //segmentedPager.parallaxHeader.height = 20
        //segmentedPager.parallaxHeader.minimumHeight = 20
        
        // Segmented Control customization
        segmentedPager.segmentedControl.selectionIndicatorLocation = .down
        segmentedPager.segmentedControl.backgroundColor = UIColor(red: 254/255, green: 247/255, blue: 243/255, alpha: 1.0)
        segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black]
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor(red: 236/255, green: 102/255, blue: 8/255, alpha: 1.0)]
        segmentedPager.segmentedControl.selectionStyle = .fullWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorColor = .orange
        //segmentedPager.segmentedControl.selectionIndicatorBoxColor = .white
        segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Farah", size: 12.0)!]
        
        segmentedPager.segmentedControl.setSelectedSegmentIndex(3, animated: true)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return ["CE QUE\n JE PENSE\n DE MOI", "CE QUE\n JE PENSE\n D'EUX", "CE QU'ILS\n PENSENT\n DE MOI", "CE QUE\n JE SUIS"][index]
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
        print("progress \(parallaxHeader.progress)")
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didSelectViewWithTitle title: String) {
        
        if segmentedPager.segmentedControl.selectedSegmentIndex == 0 {
            print("Position 0")
            segmentedPager.segmentedControl.selectionIndicatorColor = UIColor(red: 236/255, green: 102/255, blue: 8/255, alpha: 1.0)
            segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor(red: 236/255, green: 102/255, blue: 8/255, alpha: 1.0)]
        }
        
        if segmentedPager.segmentedControl.selectedSegmentIndex == 1 {
            print("Position 1")
            segmentedPager.segmentedControl.selectionIndicatorColor = UIColor(red: 0/255, green: 170/255, blue: 167/255, alpha: 1.0)
            segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor(red: 0/255, green: 170/255, blue: 167/255, alpha: 1.0)]
        }
        
        if segmentedPager.segmentedControl.selectedSegmentIndex == 2 {
            print("Position 2")
            segmentedPager.segmentedControl.selectionIndicatorColor = UIColor(red: 241/255, green: 173/255, blue: 7/255, alpha: 1.0)
            segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor(red: 241/255, green: 173/255, blue: 7/255, alpha: 1.0)]
        }
        
        if segmentedPager.segmentedControl.selectedSegmentIndex == 3 {
            print("Position 3")
            segmentedPager.segmentedControl.selectionIndicatorColor = UIColor(red: 168/255, green: 200/255, blue: 19/255, alpha: 1.0)
            segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor(red: 168/255, green: 200/255, blue: 19/255, alpha: 1.0)]
        }
        
        
        print(title)
        
    }
    
}

