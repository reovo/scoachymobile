package com.reovo.scoachy_v2;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.reovo.scoachy_v2.Fragment.MesContactsAdapter;

public class MesContacts extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    GridView gv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mescontacts);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.otf");

        ListView contactlist = (ListView) findViewById(R.id.contactList);
        //Button addContact = (Button) findViewById(R.id.addContact);
        contactlist.setVerticalScrollBarEnabled(false);

        TextView titletext = (TextView) findViewById(R.id.titleText);
        titletext.setTypeface(font);


        String[] values = new String[] { "Donna HAYWARD", "Ben HORNE", "Laura PALMER",
                "Audrey HORNE", "Shelly JOHNSON","Donna HAYWARD", "Ben HORNE", "Laura PALMER",
                "Audrey HORNE", "Shelly JOHNSON"};
        MesContactsAdapter adapter = new MesContactsAdapter(this,values);
        contactlist.setAdapter(adapter);


        /*addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(this, Contacts.class);
                startActivity(i);
            }
        });*/





        bottomNavigationView =(BottomNavigationView)findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return true;
            }
        });

    }

}
