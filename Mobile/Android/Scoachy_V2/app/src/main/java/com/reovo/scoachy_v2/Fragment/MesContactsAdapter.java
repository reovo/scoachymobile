package com.reovo.scoachy_v2.Fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.reovo.scoachy_v2.R;

/**
 * Created by jp on 12/09/2017.
 */

public class MesContactsAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    public MesContactsAdapter(Context context, String[] values) {
        super(context, R.layout.custom_contactlist, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.custom_contactlist, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.name);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.status);
        textView.setText(values[position]);
        // Change the icon for Windows and iPhone
        String s = values[position];

        //change icon dependng on status
        if (s.startsWith("Donna") || s.startsWith("Laura")) {
            imageView.setImageResource(R.drawable.yellowempty_shape);
        } else {
            imageView.setImageResource(R.drawable.yellowfill_shape);
        }

        return rowView;
    }
}