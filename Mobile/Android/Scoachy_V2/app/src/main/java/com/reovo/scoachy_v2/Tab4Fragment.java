package com.reovo.scoachy_v2;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.reovo.scoachy_v2.R;

import flepsik.github.com.progress_ring.ProgressRingView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tab4Fragment extends Fragment {
    TextView textHeaderTitle;

    public Tab4Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View tab4View= inflater.inflate(R.layout.fragment_tab4, container, false);

        Typeface header = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-SemiboldItalic.ttf");
        textHeaderTitle =(TextView)tab4View.findViewById(R.id.headerTitle);
        textHeaderTitle.setTypeface(header);

        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-Bold.ttf");

        ProgressRingView progress1 = (ProgressRingView) tab4View.findViewById(R.id.progress1);
        ProgressRingView progress2 = (ProgressRingView) tab4View.findViewById(R.id.progress2);
        ProgressRingView progress3 = (ProgressRingView) tab4View.findViewById(R.id.progress3);
        ProgressRingView progress4 = (ProgressRingView) tab4View.findViewById(R.id.progress4);
        ProgressRingView progress5 = (ProgressRingView) tab4View.findViewById(R.id.progress5);
        ProgressRingView progress6 = (ProgressRingView) tab4View.findViewById(R.id.progress6);
        ProgressRingView progress7 = (ProgressRingView) tab4View.findViewById(R.id.progress7);


        TextView textView1 = (TextView) tab4View.findViewById(R.id.textView);
        TextView textView2 = (TextView) tab4View.findViewById(R.id.textView3);
        TextView textView3 = (TextView) tab4View.findViewById(R.id.textView4);
        TextView textView4 = (TextView) tab4View.findViewById(R.id.textView5);
        TextView textView5 = (TextView) tab4View.findViewById(R.id.textView6);
        TextView textView6 = (TextView) tab4View.findViewById(R.id.textView7);
        TextView textView7 = (TextView) tab4View.findViewById(R.id.textView8);

        textView1.setTypeface(face);
        textView2.setTypeface(face);
        textView3.setTypeface(face);
        textView4.setTypeface(face);
        textView5.setTypeface(face);
        textView6.setTypeface(face);
        textView7.setTypeface(face);



        setProgressAnim(progress1,0.5f);
        setProgressAnim(progress2,0.5f);
        setProgressAnim(progress3,0.5f);
        setProgressAnim(progress4,0.5f);
        setProgressAnim(progress5,0.5f);
        setProgressAnim(progress6,0.5f);
        setProgressAnim(progress7,0.5f);


        progress1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Test",Toast.LENGTH_SHORT).show();
            }
        });


        progress2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Test",Toast.LENGTH_SHORT).show();
            }
        });


        progress3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Test",Toast.LENGTH_SHORT).show();
            }
        });


        progress4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Test",Toast.LENGTH_SHORT).show();
            }
        });


        progress5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Test",Toast.LENGTH_SHORT).show();
            }
        });


        progress6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Test",Toast.LENGTH_SHORT).show();
            }
        });

        progress7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Test",Toast.LENGTH_SHORT).show();
            }
        });



        return tab4View;
    }

    public void setProgressAnim(ProgressRingView progress, float progressvalue){
        progress.setAnimated(true);
        progress.setAnimationDuration(5000);
        progress.setProgress(progressvalue);
        return;
    }

}
