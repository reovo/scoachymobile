package com.reovo.scoachy_v2;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Act1Color extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    TextView attributeText;
    Button rep_color1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act1_color);

        bottomNavigationView =(BottomNavigationView)findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return true;
            }
        });

        Typeface face = Typeface.createFromAsset(this.getAssets(),"fonts/Quicksand-Bold.ttf");
        attributeText = (TextView) findViewById(R.id.txt_Attribute);
        attributeText.setTypeface(face);

        Typeface answer = Typeface.createFromAsset(this.getAssets(),"fonts/OpenSans-Bold.ttf");
        rep_color1= (Button) findViewById(R.id.rep_color1);
        rep_color1.setTypeface(answer);

        rep_color1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rep_color1.setPressed(true);
            }
        });
    }
}
