package com.reovo.scoachy_v2;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

public class Act1Animal extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    GridView gv;
    String animalName[]= {"Corbeau","Aigle","Araignée","Carpe","Castor","Chat","Cheval","Chien","Chouette","Coq","Cygne","Dauphin","Éléphant","Fouine","Gazelle","Lézard","Libellule","Lion/Lionne","Loup/Louve","Marmotte","Moustique","Mouton","Mule","Ours","Paon","Papillon","Perroquet","Renard","Requin","Serpent","Singe","Souris","Tigre/Tigresse","Tortue","Caméléon"};
    int animalIcons[]= {R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,
                        R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,
                        R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,
                        R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,
                        R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,
                        R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,
                        R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,
                        R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,
                        R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,
                        R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,
                        R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,
                        R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01};


    TextView  attributeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act1_animal);

        bottomNavigationView =(BottomNavigationView)findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return true;
            }
        });
        attributeText = (TextView) findViewById(R.id.txt_Attribute);

        Typeface face = Typeface.createFromAsset(this.getAssets(),"fonts/Quicksand-Bold.ttf");
        attributeText = (TextView) findViewById(R.id.txt_Attribute);
        attributeText.setTypeface(face);


        gv = (GridView)findViewById(R.id.animalOrangeGridview);
        Typeface grid = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiboldItalic.ttf");

        GridAnimalAdapter animalAdapter = new GridAnimalAdapter(Act1Animal.this,animalIcons,animalName,grid);

        gv.setAdapter(animalAdapter);

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


            }
        });
    }
}
