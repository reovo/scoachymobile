package com.reovo.scoachy_v2;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.cuboid.cuboidcirclebutton.CuboidButton;

public class Act2Competence extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    TextView attributeText, descText,questionText,descAnswer;
    CuboidButton cbtn0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act2_competence);

        bottomNavigationView =(BottomNavigationView)findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return true;
            }
        });

        Typeface face = Typeface.createFromAsset(this.getAssets(),"fonts/Quicksand-Bold.ttf");
        attributeText = (TextView) findViewById(R.id.txt_Attribute);
        attributeText.setTypeface(face);

        Typeface description = Typeface.createFromAsset(this.getAssets(),"fonts/OpenSans-SemiboldItalic.ttf");
        descText= (TextView)findViewById(R.id.txt_Description);
        descText.setTypeface(description);

        questionText =(TextView) findViewById(R.id.txt_Question);
        questionText.setTypeface(description);

        Typeface desc_ans = Typeface.createFromAsset(this.getAssets(),"fonts/OpenSans-SemiboldItalic.ttf");
        descAnswer= (TextView)findViewById(R.id.descriptionAnswer);
        descAnswer.setTypeface(description);

        cbtn0 =(CuboidButton) findViewById(R.id.cbtn_0);
        cbtn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });
    }
}
