package com.reovo.scoachy_v2;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Act2Motivation extends AppCompatActivity {
    TextView attributeText, descText,questionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act2_motivation);

        Typeface face = Typeface.createFromAsset(this.getAssets(),"fonts/Quicksand-Bold.ttf");
        attributeText = (TextView) findViewById(R.id.txt_Attribute);
        attributeText.setTypeface(face);

        Typeface description = Typeface.createFromAsset(this.getAssets(),"fonts/OpenSans-SemiboldItalic.ttf");
        descText= (TextView)findViewById(R.id.txt_Description);
        descText.setTypeface(description);

    }
}
