package com.reovo.scoachy_v2;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class Act2EndActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;

    TextView attributeText,bravoText,nextActText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act2_end);

        bottomNavigationView =(BottomNavigationView)findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return true;
            }
        });


        Typeface face = Typeface.createFromAsset(this.getAssets(),"fonts/Quicksand-Bold.ttf");
        attributeText = (TextView) findViewById(R.id.txt_Attribute);
        attributeText.setTypeface(face);

        Typeface bravo = Typeface.createFromAsset(this.getAssets(),"fonts/OpenSans-Bold.ttf");
        bravoText =(TextView)findViewById(R.id.bravoTxt);
        bravoText.setTypeface(bravo);

        Typeface textNext = Typeface.createFromAsset(this.getAssets(),"fonts/OpenSans-SemiboldItalic.ttf");
        nextActText=(TextView)findViewById(R.id.textActNext);
        nextActText.setTypeface(textNext);
    }
}
