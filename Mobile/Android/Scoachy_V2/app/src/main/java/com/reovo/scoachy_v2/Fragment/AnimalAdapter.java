package com.reovo.scoachy_v2.Fragment;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.reovo.scoachy_v2.R;

/**
 * Created by DummyReovo on 25/09/2017.
 */

public class AnimalAdapter extends ArrayAdapter<String>{
    private int[]animalIcons;
    private  String[] values;
    private Activity context;


    public AnimalAdapter( Activity context,String[] values, int[]animalIcons) {
        super(context, R.layout.list_animal,values);
         this.context = context;
        this.values =values;
        this.animalIcons = animalIcons;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        View r= convertView;
        ViewHolder viewHolder = null;
        if(r==null){
            LayoutInflater layoutInflater = context.getLayoutInflater();
            r=layoutInflater.inflate(R.layout.list_animal,null,true);
            viewHolder=new ViewHolder(r);
            r.setTag(viewHolder);
        }

        else{
            viewHolder= (ViewHolder) r.getTag();

        }
        viewHolder.imgVw.setImageResource(animalIcons[position]);
        viewHolder.tvView.setText(animalIcons[position]);


        return r;

    }

    class ViewHolder{
        TextView tvView;
        ImageView imgVw;
        ViewHolder(View v){
            tvView = (TextView) v.findViewById(R.id.animal_name);
            imgVw = (ImageView) v.findViewById(R.id.animalImage);
        }
    }
}
