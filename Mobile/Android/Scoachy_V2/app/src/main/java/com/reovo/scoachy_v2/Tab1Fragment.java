package com.reovo.scoachy_v2;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import flepsik.github.com.progress_ring.ProgressRingView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tab1Fragment extends Fragment {



    TextView textHeaderTitle;

    public Tab1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tab1, container, false);

        Typeface header = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-SemiboldItalic.ttf");
        textHeaderTitle =(TextView)rootView.findViewById(R.id.headerTitle);
        textHeaderTitle.setTypeface(header);


        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-Bold.ttf");


        ProgressRingView progress_animal = (ProgressRingView) rootView.findViewById(R.id.progress_animal);
        ProgressRingView progress_attribut = (ProgressRingView) rootView.findViewById(R.id.progress_attribut);
        ProgressRingView progress_element = (ProgressRingView) rootView.findViewById(R.id.progress_element);
        ProgressRingView progress_competences = (ProgressRingView) rootView.findViewById(R.id.progress_competences);
        ProgressRingView progress_saison = (ProgressRingView) rootView.findViewById(R.id.progress_saison);
        ProgressRingView progress_role = (ProgressRingView) rootView.findViewById(R.id.progress_role);
        ProgressRingView progress_couleur = (ProgressRingView) rootView.findViewById(R.id.progress_couleur);
        ProgressRingView progress_motivations = (ProgressRingView) rootView.findViewById(R.id.progress_motivations);

        TextView text_attribut = (TextView) rootView.findViewById(R.id.text_attribut);
        TextView text_animal = (TextView) rootView.findViewById(R.id.text_animal);
        TextView text_element = (TextView) rootView.findViewById(R.id.text_element);
        TextView text_competence = (TextView) rootView.findViewById(R.id.text_competence);
        TextView text_saison = (TextView) rootView.findViewById(R.id.text_saison);
        TextView text_role = (TextView) rootView.findViewById(R.id.text_role);
        TextView text_couleur = (TextView) rootView.findViewById(R.id.text_couleur);
        TextView text_motivation = (TextView) rootView.findViewById(R.id.text_motivation);

        text_attribut.setTypeface(face);
        text_animal.setTypeface(face);
        text_element.setTypeface(face);
        text_competence.setTypeface(face);
        text_saison.setTypeface(face);
        text_role.setTypeface(face);
        text_couleur.setTypeface(face);
        text_motivation.setTypeface(face);






        setProgressAnim(progress_animal,0f);
        setProgressAnim(progress_attribut,0.5f);
        setProgressAnim(progress_element,0.5f);
        setProgressAnim(progress_competences,1.0f);
        setProgressAnim(progress_saison,0.5f);
        setProgressAnim(progress_role,0.5f);
        setProgressAnim(progress_couleur,0.5f);
        setProgressAnim(progress_motivations,0.5f);


        progress_animal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Animal",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act1Animal.class);
                startActivity(intent);
            }
        });


        progress_attribut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Attributs",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act1Attribut.class);
                startActivity(intent);
            }
        });


        progress_element.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Element",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act1Element.class);
                startActivity(intent);
            }
        });


        progress_competences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Competences",Toast.LENGTH_SHORT).show();
            }
        });


        progress_saison.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Saison",Toast.LENGTH_SHORT).show();
            }
        });


        progress_role.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Role",Toast.LENGTH_SHORT).show();
            }
        });

        progress_couleur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Couleur",Toast.LENGTH_SHORT).show();
            }
        });


        progress_motivations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Motivation",Toast.LENGTH_SHORT).show();
            }
        });





        return rootView;
    }


    public void setProgressAnim(ProgressRingView progress, float progressvalue){
        progress.setAnimated(true);
        progress.setAnimationDuration(5000);
        progress.setProgress(progressvalue);
        return;
    }
}

