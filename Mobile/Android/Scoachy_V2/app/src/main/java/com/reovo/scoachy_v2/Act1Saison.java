package com.reovo.scoachy_v2;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Act1Saison extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    TextView attributeText;
    Button rep_autumn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act1_saison);

        bottomNavigationView =(BottomNavigationView)findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return true;
            }
        });

        Typeface face = Typeface.createFromAsset(this.getAssets(),"fonts/Quicksand-Bold.ttf");
        attributeText = (TextView) findViewById(R.id.txt_Attribute);
        attributeText.setTypeface(face);

        Typeface answer = Typeface.createFromAsset(this.getAssets(),"fonts/OpenSans-Bold.ttf");
        rep_autumn= (Button) findViewById(R.id.rep_autumn);
        rep_autumn.setTypeface(answer);

        rep_autumn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rep_autumn.setPressed(true);
            }
        });
    }
}
