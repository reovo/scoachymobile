package com.reovo.scoachy_v2;


import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.reovo.scoachy_v2.R;

import flepsik.github.com.progress_ring.ProgressRingView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tab2Fragment extends Fragment {



    TextView textHeaderTitle;


    public Tab2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View tabView = inflater.inflate(R.layout.fragment_tab2, container, false);

        Typeface headerTitle = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-SemiboldItalic.ttf");
        textHeaderTitle =(TextView)tabView.findViewById(R.id.headerTitle);
        textHeaderTitle.setTypeface(headerTitle);

        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-Bold.ttf");
        Typeface button = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-Regular.ttf");



        TextView textView1 = (TextView) tabView.findViewById(R.id.textViewAttribut);
        TextView textView2 = (TextView) tabView.findViewById(R.id.textView3);
        TextView textView3 = (TextView) tabView.findViewById(R.id.textView4);
        TextView textView4 = (TextView) tabView.findViewById(R.id.textView5);
        TextView textView5 = (TextView) tabView.findViewById(R.id.textView6);
        TextView textView7 = (TextView) tabView.findViewById(R.id.textView7);
        TextView textView8 = (TextView) tabView.findViewById(R.id.textView10);
        TextView textView9 = (TextView) tabView.findViewById(R.id.textView11);

        textView1.setTypeface(face);
        textView2.setTypeface(face);
        textView3.setTypeface(face);
        textView4.setTypeface(face);
        textView5.setTypeface(face);
        textView7.setTypeface(face);
        textView8.setTypeface(face);
        textView9.setTypeface(face);




        ProgressRingView progress_animal = (ProgressRingView) tabView.findViewById(R.id.progress_animal);
        ProgressRingView progress_attribut = (ProgressRingView) tabView.findViewById(R.id.progress_attribut);
        ProgressRingView progress_element = (ProgressRingView) tabView.findViewById(R.id.progress_element);
        ProgressRingView progress_competences = (ProgressRingView) tabView.findViewById(R.id.progress_competences);
        ProgressRingView progress_saison = (ProgressRingView) tabView.findViewById(R.id.progress_saison);
        ProgressRingView progress_role = (ProgressRingView) tabView.findViewById(R.id.progress_role);
        ProgressRingView progress_couleur = (ProgressRingView) tabView.findViewById(R.id.progress_couleur);
        ProgressRingView progress_motivation = (ProgressRingView) tabView.findViewById(R.id.progress_motivation);


        setProgressAnim(progress_animal,0.5f);
        setProgressAnim(progress_attribut,0.5f);
        setProgressAnim(progress_element,0.5f);
        setProgressAnim(progress_competences,0.5f);
        setProgressAnim(progress_saison,0.5f);
        setProgressAnim(progress_role,0.5f);
        setProgressAnim(progress_couleur,0.25f);
        setProgressAnim(progress_motivation,0.75f);

        //Button liste contact
        Typeface liste = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-Regular.ttf");
        Button listeContacts = (Button) tabView.findViewById(R.id.listeContact);
        listeContacts.setTypeface(liste);
        listeContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(getActivity(),ListeContacts.class);
                startActivity(intent);
            }

        });


        progress_animal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Animal",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act2Animal.class);
                startActivity(intent);
            }
        });


        progress_attribut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Attribut",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act2Attribut.class);
                startActivity(intent);
            }
        });


        progress_element.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Element",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act2Element.class);
                startActivity(intent);
            }
        });


        progress_competences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Competences",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act2Competence.class);
                startActivity(intent);
            }
        });


        progress_saison.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Saison",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act2Saison.class);
                startActivity(intent);
            }
        });


        progress_role.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Role",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act2Role.class);
                startActivity(intent);
            }
        });

        progress_couleur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Couleur",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act2Color.class);
                startActivity(intent);
            }
        });
        progress_motivation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Motivation",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act2Motivation.class);
                startActivity(intent);
            }
        });



        return tabView;
    }


    public void setProgressAnim(ProgressRingView progress, float progressvalue){
        progress.setAnimated(true);
        progress.setAnimationDuration(5000);
        progress.setProgress(progressvalue);
        return;
    }

}
