package com.reovo.scoachy_v2.Fragment;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reovo.scoachy_v2.R;
import com.reovo.scoachy_v2.TabPagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ModulesFragment extends Fragment {


    TabLayout tabLayout;
    public ModulesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View result = inflater.inflate(R.layout.fragment_modules,container,false);


        tabLayout =
                (TabLayout) result.findViewById(R.id.tab_layout);

        tabLayout.setBackgroundColor(Color.parseColor("#fef8f3"));



        tabLayout.addTab(tabLayout.newTab().setText("CE QUE JE PENSE DE MOI"));
        tabLayout.addTab(tabLayout.newTab().setText("CE QUE JE PENSE D'EUX"));
        tabLayout.addTab(tabLayout.newTab().setText("CE  QU'ILS PENSENT DE MOI"));
        tabLayout.addTab(tabLayout.newTab().setText("CE QUE JE SUIS"));

        changeTabsFont();

        final ViewPager viewPager =
                (ViewPager) result.findViewById(R.id.pager);

        viewPager.setAdapter(buildAdapter());
//        final PagerAdapter adapter = new TabPagerAdapter
//                (getSupportFragmentManager(),
//                        tabLayout.getTabCount());
//        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new
                                                   TabLayout.OnTabSelectedListener() {
                                                       @Override
                                                       public void onTabSelected(TabLayout.Tab tab) {
                                                           viewPager.setCurrentItem(tab.getPosition());

                                                           switch (tab.getPosition()) {
                                                               case 0:
                                                                   //  Toast.makeText(getApplicationContext(),"CE QUE JE PENSE DE MOI", Toast.LENGTH_SHORT).show();
                                                                   tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#EC6608"));
                                                                   tabLayout.setTabTextColors (R.color.tabTextColor, Color.parseColor("#EC6608"));
                                                                   changeTabsFont();

                                                                   // tabLayout.setTabTextColors(getResources().getColorStateList(R.color.orangeIndicator));
                                                                   break;
                                                               case 1:
                                                                   //   Toast.makeText(getApplicationContext(),"CE QUE JE PENSE D'EUX", Toast.LENGTH_SHORT).show();
                                                                   tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#00AAA7"));
                                                                   tabLayout.setTabTextColors (R.color.tabTextColor, Color.parseColor("#00AAA7"));
                                                                   changeTabsFont();
                                                                   // tabLayout.setTabTextColors(getResources().getColorStateList(R.color.blueIndicator));

                                                                   break;
                                                               case 2:
                                                                   //  Toast.makeText(getApplicationContext(),"CE  QU'ILS PENSENT DE MOI", Toast.LENGTH_SHORT).show();
                                                                   tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#F1AD07"));
                                                                   tabLayout.setTabTextColors (R.color.tabTextColor, Color.parseColor("#F1AD07"));
                                                                   changeTabsFont();
                                                                   //tabLayout.setTabTextColors(getResources().getColorStateList(R.color.yellowIndicator));

                                                                   break;
                                                               case 3:
                                                                   //  Toast.makeText(getApplicationContext(),"CE QUE JE SUIS", Toast.LENGTH_SHORT).show();
                                                                   tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#A8C813"));
                                                                   tabLayout.setTabTextColors (R.color.tabTextColor, Color.parseColor("#A8C813"));
                                                                   changeTabsFont();
                                                                   //tabLayout.setTabTextColors(getResources().getColorStateList(R.color.greenIndicator));

                                                                   break;
                                                           }

                                                       }

                                                       @Override
                                                       public void onTabUnselected(TabLayout.Tab tab) {

                                                           switch (tab.getPosition()) {
                                                               case 0:
                                                                   tabLayout.setTabTextColors(getResources().getColorStateList(R.color.tabTextColor));
                                                                   changeTabsFont();

                                                                   break;
                                                               case 1:
                                                                   tabLayout.setTabTextColors(getResources().getColorStateList(R.color.tabTextColor));
                                                                   changeTabsFont();
                                                                   break;
                                                               case 2:
                                                                   tabLayout.setTabTextColors(getResources().getColorStateList(R.color.tabTextColor));
                                                                   changeTabsFont();
                                                                   break;
                                                               case 3:
                                                                   tabLayout.setTabTextColors(getResources().getColorStateList(R.color.tabTextColor));
                                                                   changeTabsFont();
                                                                   break;
                                                           }
                                                       }

                                                       @Override
                                                       public void onTabReselected(TabLayout.Tab tab) {


                                                       }

                                                   });

        return(result);


        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.fragment_modules, container, false);
    }

    private PagerAdapter buildAdapter() {
        return(new TabPagerAdapter( getChildFragmentManager(),tabLayout.getTabCount()));
    }

    private void changeTabsFont() {
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Quicksand_Bold.otf");
        int myint = 100;
        float newfloat = (float) myint;
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(face);
                }
            }
        }
    }

}

