package com.reovo.scoachy_v2;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Act2Attribut extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;

    TextView attributeText, descText,questionText;
    Button rep1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act2_attribut);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return true;
            }
        });


        Typeface face = Typeface.createFromAsset(this.getAssets(), "fonts/Quicksand-Bold.ttf");
        attributeText = (TextView) findViewById(R.id.txt_Attribute);
        attributeText.setTypeface(face);

        Typeface description = Typeface.createFromAsset(this.getAssets(), "fonts/OpenSans-SemiboldItalic.ttf");
        descText = (TextView) findViewById(R.id.txt_Description);
        descText.setTypeface(description);

        questionText = (TextView) findViewById(R.id.txt_Question);
        questionText.setTypeface(description);

        Typeface answer = Typeface.createFromAsset(this.getAssets(), "fonts/OpenSans-Bold.ttf");
        rep1 = (Button) findViewById(R.id.rep_1);
        rep1.setTypeface(answer);
        rep1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rep1.setPressed(true);
                Intent intent;
                intent = new Intent(Act2Attribut.this, Act2EndActivity.class);
                startActivity(intent);
            }
        });
    }
}
