package com.reovo.scoachy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Activity_4_18_objectif extends AppCompatActivity {
     ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4_18_objectif);
    }
    public void closeButton(View view) {

        Intent intent = new Intent(this,Activity_4_18.class);
        startActivity(intent);
    }
}
