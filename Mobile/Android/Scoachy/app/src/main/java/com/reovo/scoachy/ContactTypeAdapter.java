package com.reovo.scoachy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DummyReovo on 11/05/2017.
 */


public class ContactTypeAdapter extends ArrayAdapter {
    List list = new ArrayList();

    public ContactTypeAdapter(Context context, int resource) {
        super(context, resource);
    }

    static class DataHandler {
        ImageView Image;
        TextView contactname;
        TextView contactnum;

    }
    @Override
    public void  add(Object object){
        super.add(object);
        list.add(object);
    }
    @Override
    public int getCount(){
        return this.list.size();
    }
    @Override
    public Object getItem(int position){
        return this.list.get(position);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View rows;
        DataHandler handler;
        rows =convertView;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rows = inflater.inflate(R.layout.row,parent, false);
            handler = new DataHandler();
            handler.Image = (ImageView) rows.findViewById(R.id.contact);
            handler.contactname = (TextView) rows.findViewById(R.id.contactNom);
            handler.contactnum =(TextView) rows.findViewById(R.id.contactNumber);
            rows.setTag(handler);
        }
        else {
            handler = (DataHandler) rows.getTag();
        }

        ContactListProvider contactdataProvider;
        contactdataProvider = (ContactListProvider) this.getItem(position);
        handler.Image.setImageResource(contactdataProvider.getImg_contact());
        handler.contactnum.setText(contactdataProvider.getNumber());
        handler.contactname.setText(contactdataProvider.getContact());

        return rows;
    }

}
