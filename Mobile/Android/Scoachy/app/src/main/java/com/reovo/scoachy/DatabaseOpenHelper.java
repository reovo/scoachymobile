package com.reovo.scoachy;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class DatabaseOpenHelper extends SQLiteAssetHelper {


    private static final String DATABASE_NAME = "Scoachy.db";
    private static final int DATABASE_VERSION = 1;


    public DatabaseOpenHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


//    @Override
//    public void onCreate(SQLiteDatabase db) {
//        db.execSQL("CREATE TABLE IF NOT EXISTS answerresult(userID VARCHAR,answerId VARCHAR,questionId VARCHAR,lastAnswerDate DATETIME);");
//    }
//
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        db.execSQL("CREATE TABLE IF NOT EXISTS answerresult(userID VARCHAR,answerId VARCHAR,questionId VARCHAR,lastAnswerDate DATETIME);");
//    }


}
