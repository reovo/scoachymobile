package com.reovo.scoachy;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

public class Invite_3 extends AppCompatActivity {
    ListView listView;
    int [] img_contact = {R.drawable.avatarprofile, R.drawable.avatarprofile, R.drawable.avatarprofile, R.drawable.avatarprofile};

    String[] contacts;
    String[] numbers;

    ContactTypeAdapter adapter;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_3);

        listView = (ListView) findViewById(R.id.list_contacts);
        numbers = getResources().getStringArray(R.array.numbers);
        contacts = getResources().getStringArray(R.array.contacts);

        int i =0;

        adapter = new ContactTypeAdapter(getApplicationContext(), R.layout.row);
        listView.setAdapter(adapter);

        for (String contactList : contacts)
        {
            ContactListProvider dataProvider = new ContactListProvider(img_contact[i], contactList,numbers[i]);

            adapter.add(dataProvider);
                 i++;
        }



    }






}
