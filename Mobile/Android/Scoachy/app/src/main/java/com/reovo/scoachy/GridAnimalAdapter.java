package com.reovo.scoachy;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by DummyReovo on 06/06/2017.
 */

public class GridAnimalAdapter extends BaseAdapter {
    private int icons [];
    private String texts[];
    private Context context;
    private LayoutInflater inflater;
    private Typeface tf;

    public GridAnimalAdapter(Context context, int icons[], String texts[], Typeface tf){
        this.context =context;
        this.icons =icons;
        this.texts =texts;
         this.tf =tf;
    }






    @Override
    public int getCount() {
        return texts.length;
    }

    @Override
    public Object getItem(int position) {
        return texts[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       View gv =convertView;
        if(convertView == null){
            inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gv = inflater.inflate(R.layout.custom_animal,null);

        }
        ImageView icon=(ImageView)gv.findViewById(R.id.animal_image);
        TextView text =(TextView)gv.findViewById(R.id.animal_text);



        icon.setImageResource(icons[position]);
        text.setText(texts[position]);
         text.setTypeface(tf);

        return gv;
    }
}
