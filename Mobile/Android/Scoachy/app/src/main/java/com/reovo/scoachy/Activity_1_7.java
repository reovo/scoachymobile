package com.reovo.scoachy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import static android.widget.Toast.makeText;

public class Activity_1_7 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    GridView gridView;
    TextView txtQuestion, txtAttribute, txtDescription;
    ArrayList<QuestionModel> questionAct;
    int colours []={R.drawable.couleurs_scoachy_01,R.drawable.couleurs_scoachy_02,R.drawable.couleurs_scoachy_03,R.drawable.couleurs_scoachy_04,R.drawable.couleurs_scoachy_05,R.drawable.couleurs_scoachy_06,R.drawable.couleurs_scoachy_07,R.drawable.couleurs_scoachy_08,R.drawable.couleurs_scoachy_09};
    String colorIds[] = {"1","2","3","4","5","6","7","8","9",};
    Date date = new Date();
    public static final String PREFSHARE  = "preferenceOthers";
    private DatabaseAccess databaseAccess;
    DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1_7);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Ce que je pense de autres");




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View logoView =navigationView.getHeaderView(0);
        ImageView logoHeader = (ImageView)logoView.findViewById(R.id.logoNav);
        logoHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Home = new Intent(Activity_1_7.this, HomeActivity.class);
                // storeAnswer();
                startActivity(Home);
            }
        });
        dbHelper = new DatabaseHelper(this);
        databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        questionAct = databaseAccess.questionAct();

        txtQuestion = (TextView) findViewById(R.id.txt_Question);
        txtDescription = (TextView) findViewById(R.id.txt_Description);
        txtAttribute = (TextView) findViewById(R.id.txt_Attribute);

        //font
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.ttf");
        txtAttribute.setTypeface(typeface);

        Typeface question = Typeface.createFromAsset(getAssets(), "fonts/OpenSans_Regular.ttf");
        txtQuestion.setTypeface(question);


        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("QuestionAct");
        txtAttribute.setText(questionAct.get(data).getAttribute());
        txtQuestion.setText(questionAct.get(data).getQuest());
        txtDescription.setText(questionAct.get(data).getDescription());
        Log.d("Arraylist3", questionAct.get(data).getRatingType());


        Log.d("Arraylist3", questionAct.get(data).getRatingType() +" : "+questionAct.get(data).getID());
        final String idQuest = questionAct.get(data).getID();
        date.setTime(System.currentTimeMillis());
        final String dateAnswered = date.toString();
        Log.d("Database", dateAnswered);

        GridView gridView = (GridView)findViewById(R.id.gridviewColor);
        GridAdapter adapter = new GridAdapter(Activity_1_7.this,colours,colorIds);

        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //    Toast.makeText(Activity_1_7.this, "hey" +colours[position], Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(Activity_2_6.this,Activity_2_7.class);
//                startActivity(intent);

                boolean isInserted = dbHelper.insertAnswer( "5",colorIds[position],idQuest, dateAnswered);
                if(isInserted == true) {
              //      makeText(Activity_1_7.this, "Data Inserted 5", Toast.LENGTH_LONG).show();
                }
                else {
             //       makeText(Activity_1_7.this, "Data not Inserted", Toast.LENGTH_LONG).show();
                }

                moveNext();

            }
        });
    }


    private void storeAnswer() {
        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("QuestionAct");

        SharedPreferences preferenceQuestion = getSharedPreferences(PREFSHARE, 0);
        SharedPreferences.Editor editor = preferenceQuestion.edit();
        editor.putInt("questionAct1", data);

        editor.commit();
    }

    public void moveNext(){
        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("QuestionAct");
        int nextQuestion = data +1;
        storeAnswer();

        //1
        if (questionAct.get(nextQuestion).getRatingType().equals("competenceChoice")){
        //    Toast.makeText(this, "competenceChoice", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Activity_1_7.this,Activity_1_4.class);
            intent.putExtra("QuestionAct", nextQuestion);
            startActivity(intent);

        }
        //2
        else if (questionAct.get(nextQuestion).getRatingType().equals("attributeChoice")){
        //    Toast.makeText(this, "attributeChoice", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Activity_1_7.this,Activity_1_2.class);
            intent.putExtra("QuestionAct", nextQuestion);
            startActivity(intent);

        }
        //3
        else if (questionAct.get(nextQuestion).getRatingType().equals("animalChoice")) {
         //   Toast.makeText(this, "animalChoice", Toast.LENGTH_SHORT).show();
            //  questions.remove(0);
            Intent intent = new Intent(Activity_1_7.this, Activity_1_1.class);
            intent.putExtra("QuestionAct", nextQuestion);
            startActivity(intent);
        }
        //4
        else if (questionAct.get(nextQuestion).getRatingType().equals("elementChoice")){
        //    Toast.makeText(this, "elementChoice", Toast.LENGTH_SHORT).show();
            // questions.remove(0);
            Intent intent = new Intent(Activity_1_7.this, Activity_1_3.class);
            intent.putExtra("QuestionAct",nextQuestion);
            startActivity(intent);
        }
        //5

        else if (questionAct.get(nextQuestion).getRatingType().equals("motivationChoice")){
         //   Toast.makeText(this, "motivationChoice", Toast.LENGTH_SHORT).show();
            // questions.remove(0);
            Intent intent = new Intent(Activity_1_7.this, Activity_1_8.class);
            intent.putExtra("QuestionAct",nextQuestion);
            startActivity(intent);

        }
        //6
        else if (questionAct.get(nextQuestion).getRatingType().equals("seasonChoice")){
        //    Toast.makeText(this, "seasonChoice", Toast.LENGTH_SHORT).show();
            // questions.remove(0);
            Intent intent = new Intent(Activity_1_7.this, Activity_1_5.class);
            intent.putExtra("QuestionAct", nextQuestion);
            startActivity(intent);

        }
        //7
        else if (questionAct.get(nextQuestion).getRatingType().equals("roleChoice")){
        //    Toast.makeText(this, "roleChoice", Toast.LENGTH_SHORT).show();
            // questions.remove(0);
            Intent intent = new Intent(Activity_1_7.this, Activity_1_6.class);
            intent.putExtra("QuestionAct",nextQuestion);
            startActivity(intent);

        }
        //8
        else if (questionAct.get(nextQuestion).getRatingType().equals("colorChoice")){
        //    Toast.makeText(this, "colorChoice", Toast.LENGTH_SHORT).show();
            // questions.remove(0);
            Intent intent = new Intent(Activity_1_7.this, Activity_1_7.class);
            intent.putExtra("QuestionAct", nextQuestion);
            startActivity(intent);

        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.activity_1_7, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_settings) {
            Intent intentAccount = new Intent(this, Activity_UserAccount.class);
            startActivity(intentAccount);
            //storeAnswer();
            return true;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
