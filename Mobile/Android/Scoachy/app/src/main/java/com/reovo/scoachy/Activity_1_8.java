package com.reovo.scoachy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cuboid.cuboidcirclebutton.CuboidButton;

import java.util.ArrayList;
import java.util.Date;

public class Activity_1_8 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TextView txtQuestion, txtAttribute, txtDescription;
    ArrayList<QuestionModel> questionAct;

    CuboidButton rep2, rep1, rep3;

    Date date =new Date();

    public static final String PREFSHARE  = "preferenceOthers";
    private DatabaseAccess databaseAccess;
    DatabaseHelper dbHelper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1_8);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Ce que je pense de autres");


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View logoView =navigationView.getHeaderView(0);
        ImageView logoHeader = (ImageView)logoView.findViewById(R.id.logoNav);
        logoHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Home = new Intent(Activity_1_8.this, HomeActivity.class);
                // storeAnswer();
                startActivity(Home);
            }
        });
        dbHelper = new DatabaseHelper(this);
        databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        questionAct = databaseAccess.questionAct();

        txtQuestion = (TextView) findViewById(R.id.txt_Question);
        txtDescription = (TextView) findViewById(R.id.txt_Description);
        txtAttribute = (TextView) findViewById(R.id.txt_Attribute);
//        answer_faible=(CuboidButton)findViewById(R.id.cbtn_faible);
//        answer_moyen =(CuboidButton)findViewById(R.id.cbtn_moyenne);
//        answer_forte =(CuboidButton)findViewById(R.id.cbtn_forte);
        rep2 = (CuboidButton)findViewById(R.id.cbtn_moyenne);
        rep1 = (CuboidButton)findViewById(R.id.cbtn_faible);
        rep3 = (CuboidButton)findViewById(R.id.cuboidButton3);

        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("QuestionAct");
        txtAttribute.setText(questionAct.get(data).getAttribute());
        txtQuestion.setText(questionAct.get(data).getQuest());
        txtDescription.setText(questionAct.get(data).getDescription());
        Log.d("Arraylist3", questionAct.get(data).getRatingType());
        Log.d("Arraylist3", questionAct.get(data).getQuest());

        final  String id = questionAct.get(data).getID();

        final String answer_low = "fb906f94-6152-11e7-a7e0-484d7ee0cd26";

        date.setTime(System.currentTimeMillis());
        final String dateAnswered = date.toString();
        Log.d("Database", dateAnswered);

        int  SizeQuestion =questionAct.size();
        System.out.println("Heya  " + SizeQuestion);




        rep1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                moveNext();

            }
        });
        rep2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                moveNext();

            }
        });
        rep3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                moveNext();

            }
        });
//        answer_moyen.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//
//                moveNext();
//
//            }
//        });
//        answer_forte.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//
//                moveNext();
//
//            }
//        });

    }

    private void storeAnswer() {
        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("QuestionAct");

        SharedPreferences preferenceQuestion = getSharedPreferences(PREFSHARE, 0);
        SharedPreferences.Editor editor = preferenceQuestion.edit();
        editor.putInt("questionAct1", data);

        editor.commit();
    }
    public void moveNext(){
        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("QuestionAct");
        int nextQuestion = data +1;
        storeAnswer();

        int  SizeQuestion =questionAct.size() -1;

        System.out.println("Heya size " + SizeQuestion);

        System.out.println("Heya data  " + data);

        if (SizeQuestion == data){
        Toast.makeText(this, "You Have Completed the Activity", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Activity_1_8.this,HomeActivity.class);
            intent.putExtra("Question", nextQuestion);
            startActivity(intent);
        }
        else {

            //1
            if (questionAct.get(nextQuestion).getRatingType().equals("competenceChoice")) {
                // Toast.makeText(this, "competenceChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Activity_1_8.this, Activity_1_4.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);

            }
            //2
            else if (questionAct.get(nextQuestion).getRatingType().equals("attributeChoice")) {
                //  Toast.makeText(this, "attributeChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Activity_1_8.this, Activity_1_2.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);

            }
            //3
            else if (questionAct.get(nextQuestion).getRatingType().equals("animalChoice")) {
                //  Toast.makeText(this, "animalChoice", Toast.LENGTH_SHORT).show();
                //  questions.remove(0);
                Intent intent = new Intent(Activity_1_8.this, Activity_1_1.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);
            }
            //4
            else if (questionAct.get(nextQuestion).getRatingType().equals("elementChoice")) {
                //   Toast.makeText(this, "elementChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1_8.this, Activity_1_3.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);
            }
            //5

            else if (questionAct.get(nextQuestion).getRatingType().equals("motivationChoice")) {
                //  Toast.makeText(this, "motivationChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1_8.this, Activity_1_8.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);

            }
            //6
            else if (questionAct.get(nextQuestion).getRatingType().equals("seasonChoice")) {
                //   Toast.makeText(this, "seasonChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1_8.this, Activity_1_5.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);

            }
            //7
            else if (questionAct.get(nextQuestion).getRatingType().equals("roleChoice")) {
                //  Toast.makeText(this, "roleChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1_8.this, Activity_1_6.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);

            }
            //8
            else if (questionAct.get(nextQuestion).getRatingType().equals("colorChoice")) {
                //  Toast.makeText(this, "colorChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1_8.this, Activity_1_7.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);

            }
        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.activity_1_8, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_settings) {
            Intent intentAccount = new Intent(this, Activity_UserAccount.class);
            startActivity(intentAccount);
            //storeAnswer();
            return true;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
