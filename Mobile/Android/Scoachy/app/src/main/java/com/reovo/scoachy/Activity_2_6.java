package com.reovo.scoachy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import static android.widget.Toast.makeText;

public class Activity_2_6 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    GridView gridView;
    TextView txtQuestion, txtAttribute, txtDescription;
    ArrayList<QuestionModel> questions;

    int colours []={R.drawable.couleurs_scoachy_01,R.drawable.couleurs_scoachy_02,R.drawable.couleurs_scoachy_03,R.drawable.couleurs_scoachy_04,R.drawable.couleurs_scoachy_05,R.drawable.couleurs_scoachy_06,R.drawable.couleurs_scoachy_07,R.drawable.couleurs_scoachy_08,R.drawable.couleurs_scoachy_09};
    String colorIds[] = {"1","2","3","4","5","6","7","8","9",};
    Date date = new Date();
    private DatabaseAccess databaseAccess;
    public static final String PREFS  = "preferenceQuestion";
    DatabaseHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2_6);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Ce que je pense de moi");



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View logoView = navigationView.getHeaderView(0);
        ImageView logoHeader = (ImageView) logoView.findViewById(R.id.logoNav);
        logoHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Home = new Intent(Activity_2_6.this, HomeActivity.class);
                storeAnswer();
                startActivity(Home);
            }
        });
        gridView = (GridView)findViewById(R.id.gridview);

        dbHelper = new DatabaseHelper(this);

        databaseAccess = DatabaseAccess.getInstance(getApplicationContext());
        databaseAccess.open();
        questions = databaseAccess.questions();

        txtQuestion = (TextView) findViewById(R.id.txt_Question);
        txtDescription = (TextView) findViewById(R.id.txt_Description);
        txtAttribute = (TextView) findViewById(R.id.txt_Attribute);



        //font
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.ttf");
        txtAttribute.setTypeface(typeface);

        Typeface question = Typeface.createFromAsset(getAssets(), "fonts/OpenSans_Regular.ttf");
        txtQuestion.setTypeface(question);

        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("Question");
        txtAttribute.setText(questions.get(data).getAttribute());
        txtQuestion.setText(questions.get(data).getQuest());
        txtDescription.setText(questions.get(data).getDescription());


        Log.d("Arraylist3", questions.get(data).getRatingType() +" : "+questions.get(data).getID());
        final String idQuest = questions.get(data).getID();
        date.setTime(System.currentTimeMillis());
        final String dateAnswered = date.toString();
        Log.d("Database", dateAnswered);

        GridAdapter adapter = new GridAdapter(Activity_2_6.this,colours,colorIds);

        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
             //   Toast.makeText(Activity_2_6.this, "hey" +colours[position], Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(Activity_2_6.this,Activity_2_7.class);
//                startActivity(intent);

                boolean isInserted = dbHelper.insertAnswer( "5",colorIds[position],idQuest, dateAnswered);
                if(isInserted == true) {
             //       makeText(Activity_2_6.this, "Data Inserted 5", Toast.LENGTH_LONG).show();
                }
                else {
                //    makeText(Activity_2_6.this, "Data not Inserted", Toast.LENGTH_LONG).show();
                }

                moveNext();

            }
        });
    }


    private void storeAnswer() {
        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("Question");

        SharedPreferences preferenceQuestion = getSharedPreferences(PREFS, 0);
        SharedPreferences.Editor editor = preferenceQuestion.edit();
        editor.putInt("QuestionMessage", data);

        editor.commit();
    }



    public void moveNext(){

        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("Question");
        int nextQuestion = data +1;

        int  SizeQuestion =questions.size();

        System.out.println("Heya  " + data);

        if (SizeQuestion == data){
         //   Toast.makeText(this, "You Have Completed the Activity", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Activity_2_6.this,HomeActivity.class);
            intent.putExtra("Question", nextQuestion);
            startActivity(intent);
        }
        else
            //1
            if (questions.get(nextQuestion).getRatingType().equals("competenceChoice")){
           //     Toast.makeText(this, "competenceChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Activity_2_6.this,Activity_2_3.class);
                intent.putExtra("Question", nextQuestion);
                startActivity(intent);

            }
            //2
            else if (questions.get(nextQuestion).getRatingType().equals("attributeChoice")){
                Toast.makeText(this, "attributeChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Activity_2_6.this,Activity_2_1.class);
                intent.putExtra("Question", nextQuestion);
                startActivity(intent);

            }
            //3
            else if (questions.get(nextQuestion).getRatingType().equals("animalChoice")) {
           //     Toast.makeText(this, "animalChoice", Toast.LENGTH_SHORT).show();
                //  questions.remove(0);
                Intent intent = new Intent(Activity_2_6.this, Activity_2.class);
                intent.putExtra("Question", nextQuestion);
                startActivity(intent);
            }
            //4
            else if (questions.get(nextQuestion).getRatingType().equals("elementChoice")){
          //      Toast.makeText(Activity_2_6.this, "elementChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_2_6.this, Activity_2_2.class);
                intent.putExtra("Question", nextQuestion);
                startActivity(intent);
            }
            //5

            else if (questions.get(nextQuestion).getRatingType().equals("motivationChoice")){
           //     Toast.makeText(Activity_2_6.this, "motivationChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_2_6.this, Activity_2_7.class);
                intent.putExtra("Question", nextQuestion);
                startActivity(intent);

            }
            //6
            else if (questions.get(nextQuestion).getRatingType().equals("seasonChoice")){
            //    Toast.makeText(Activity_2_6.this, "seasonChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(this, Activity_2_4.class);
                startActivity(intent);

            }
            //7
            else if (questions.get(nextQuestion).getRatingType().equals("roleChoice")){
            //    Toast.makeText(Activity_2_6.this, "roleChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(this, Activity_2_5.class);
                startActivity(intent);

            }
            //8
            else if (questions.get(nextQuestion).getRatingType().equals("colorChoice")){
            //    Toast.makeText(Activity_2_6.this, "colorChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(this, Activity_2_6.class);
                startActivity(intent);

            }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.activity_2_6, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_settings) {
            Intent intentAccount = new Intent(this, Activity_UserAccount.class);
            startActivity(intentAccount);
            //  storeAnswer();
            return true;
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
