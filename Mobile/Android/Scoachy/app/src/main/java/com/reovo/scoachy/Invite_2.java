package com.reovo.scoachy;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Invite_2 extends AppCompatActivity {
    TextView textView;
    Button example_1, example_2, example_3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_2);
        textView = (TextView) findViewById(R.id.textinput);
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();

        Bundle bundle = getIntent().getExtras();

        //pass data to string variables
        String personName = bundle.getString("NAME");

        //assign xml objects for TextViews into variables
        TextView numero = (TextView) findViewById(R.id.contactName);
        numero.setText(personName);

    }
    public void setText(View v){

        if(v.getId() == R.id.example_1)

        textView.setText(getResources().getString(R.string.message));
//        if(v.getId() == R.id.example_2)
//            textView.setText("Lorem isum");
//        if(v.getId() == R.id.example_3)
//            textView.setText("Lorem  3");

    }

    // Send Invite Message
    public void validateMessage(View v){
        Toast.makeText(getBaseContext(),"Validation de Message",Toast.LENGTH_LONG).show();
        Bundle bundle = getIntent().getExtras();

        //pass data to string variables
        String phoneNumber = bundle.getString("PHONE");
        String personName = bundle.getString("personName");
        String phoneEmail= bundle.getString("EMAIL");

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        Intent chooser = Intent.createChooser(intent,"Send via");


        startActivity(chooser);
    }


}
