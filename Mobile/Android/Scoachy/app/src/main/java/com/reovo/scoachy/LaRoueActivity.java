package com.reovo.scoachy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class LaRoueActivity extends AppCompatActivity {

    float rainfall[] = {98.8f, 45.2f, 89.3f};
    //String monthNames[] = {"Jan", "Feb", "Mar"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_la_roue);

        setupPieChart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_account:
                Toast.makeText(LaRoueActivity.this, "Account", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item_language:
                Toast.makeText(LaRoueActivity.this, "Language", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void setupPieChart(){
        //Populating a list of PieEntries
        List<PieEntry> pieEntries = new ArrayList<>();
        for(int i = 0; i<rainfall.length; i++){
            pieEntries.add(new PieEntry(rainfall[i]));
        }

        PieDataSet dataSet = new PieDataSet(pieEntries, "Rainfall");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData data = new PieData(dataSet);

        //Get the chart:
        PieChart chart = (PieChart) findViewById(R.id.chart);
        chart.setData(data);
        chart.invalidate();

        //Animate Pie
        chart.animateY(1000);

        //Enable/Disable Chart Legend
        Legend l = chart.getLegend();
        l.setEnabled(false);

        //Enable/Disable Draw Hole
        chart.setDrawHoleEnabled(true);

        chart.setCenterText("SATISFACTION");
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, Grid4.class);
        startActivity(intent);
    }
}
