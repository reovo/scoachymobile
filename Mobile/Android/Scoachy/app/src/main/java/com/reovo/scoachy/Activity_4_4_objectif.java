package com.reovo.scoachy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Activity_4_4_objectif extends AppCompatActivity {
    ImageView close;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4_4_objectif);

        close =(ImageView)findViewById(R.id.btn_close);
        close.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent create = new Intent(Activity_4_4_objectif.this, Activity_4_4.class);
                startActivity(create);


            }


        });

    }


}
