package com.reovo.scoachy;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class Activity_4_6 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private RecyclerView mRecyclerView;
    private TimeLineAdapter mTimeLineAdapter;
    private List<TimeLineModel> mDataList = new ArrayList<>();

    private boolean mWithLinePadding;
     Button create;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4_6);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Ce que je suis - Ma ligne de vie");



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View logoView =navigationView.getHeaderView(0);
        ImageView logoHeader = (ImageView)logoView.findViewById(R.id.logoNav);
        logoHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Home = new Intent(Activity_4_6.this, HomeActivity.class);
                startActivity(Home);

            }
        });


        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(getLinearLayoutManager());
        mRecyclerView.setHasFixedSize(true);

        initView();

//         create =(Button)findViewById(R.id.btn_next);
//        create.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//                Intent valider = new Intent(Activity_4_6.this, Activity_4_7.class);
//                startActivity(valider);
//
//
//            }
//
//
//        });

    }


    private LinearLayoutManager getLinearLayoutManager() {

        return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

    }

    private void initView() {
        setDataListItems();
        mTimeLineAdapter = new TimeLineAdapter(mDataList,  mWithLinePadding);
        mRecyclerView.setAdapter(mTimeLineAdapter);
    }

    private void setDataListItems(){
        mDataList.add(new TimeLineModel("1980   Naissance ", "1980"));
        mDataList.add(new TimeLineModel("1990   Changement d'école", "1990"));
        mDataList.add(new TimeLineModel("1996   Déménagement", "1996"));
        mDataList.add(new TimeLineModel("1998   Changement d'école", "1998"));
        mDataList.add(new TimeLineModel("2000   Separation", "2000"));
        mDataList.add(new TimeLineModel("2001   Achat d'une automobile", "2001"));
        mDataList.add(new TimeLineModel("2003   Achat d'un appart", "2003"));
        mDataList.add(new TimeLineModel("2005   Lorem ipsum dolor", "2005"));
        mDataList.add(new TimeLineModel("2017   Lorem ipsum dolor", "2017"));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.activity_4_6, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_roue) {
            Intent intentRoue = new Intent(this, Activity_4.class);
            startActivity(intentRoue);

            return true;
            // Handle the camera action
        } else if (id == R.id.nav_ligne) {
            Intent intentligne = new Intent(this, Activity_4_4.class);
            startActivity(intentligne);

            return true;

        } else if (id == R.id.nav_modele) {
            Intent intentligne = new Intent(this, Activity_4_7.class);
            startActivity(intentligne);

            return true;

        } else if (id == R.id.nav_valeurs) {
            Intent intentvaleur = new Intent(this, Activity_4_11.class);
            startActivity(intentvaleur);

            return true;

        } else if (id == R.id.nav_vision) {
            Intent intentVision = new Intent(this, Activity_4_15.class);
            startActivity(intentVision);

            return true;

        } else if (id == R.id.nav_talents) {
            Intent intentTalent = new Intent(this, Activity_4_18.class);
            startActivity(intentTalent);

            return true;

        } else if (id == R.id.nav_mission) {
            Intent intentMission = new Intent(this, Activity_4_22.class);
            startActivity(intentMission);

            return true;

        } else if (id == R.id.nav_settings) {
            Intent intentAccount = new Intent(this, Activity_UserAccount.class);
            startActivity(intentAccount);

            return true;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
