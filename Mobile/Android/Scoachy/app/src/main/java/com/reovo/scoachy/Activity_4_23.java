package com.reovo.scoachy;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;

public class Activity_4_23 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4_23);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Ce que je suis - Ma mission");



        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View logoView =navigationView.getHeaderView(0);
        ImageView logoHeader = (ImageView)logoView.findViewById(R.id.logoNav);
        logoHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Home = new Intent(Activity_4_23.this, HomeActivity.class);
                startActivity(Home);

            }
        });
    }

    public void next(View view) {
        Intent intent = new Intent(this,Activity_4_24.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.activity_4_23, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_roue) {
            Intent intentRoue = new Intent(this, Activity_4.class);
            startActivity(intentRoue);

            return true;
            // Handle the camera action
        } else if (id == R.id.nav_ligne) {
            Intent intentligne = new Intent(this, Activity_4_4.class);
            startActivity(intentligne);

            return true;

        } else if (id == R.id.nav_modele) {
            Intent intentligne = new Intent(this, Activity_4_7.class);
            startActivity(intentligne);

            return true;

        } else if (id == R.id.nav_valeurs) {
            Intent intentvaleur = new Intent(this, Activity_4_11.class);
            startActivity(intentvaleur);

            return true;

        } else if (id == R.id.nav_vision) {
            Intent intentVision = new Intent(this, Activity_4_15.class);
            startActivity(intentVision);

            return true;

        } else if (id == R.id.nav_talents) {
            Intent intentTalent = new Intent(this, Activity_4_18.class);
            startActivity(intentTalent);

            return true;

        } else if (id == R.id.nav_mission) {
            Intent intentMission = new Intent(this, Activity_4_22.class);
            startActivity(intentMission);

            return true;

        } else if (id == R.id.nav_settings) {
            Intent intentAccount = new Intent(this, Activity_UserAccount.class);
            startActivity(intentAccount);

            return true;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
