package com.reovo.scoachy;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.Manifest;


import java.util.ArrayList;
import java.util.List;

public class Activity_1 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ArrayList<QuestionModel> questionAct;
    public static final String PREFSHARE  = "preferenceOthers";
    private DatabaseAccess databaseAccess;
    DatabaseHelper dbHelper;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private ListView contactlv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Ce que je pense de autres");



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View logoView =navigationView.getHeaderView(0);
        ImageView logoHeader = (ImageView)logoView.findViewById(R.id.logoNav);
        logoHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Home = new Intent(Activity_1.this, HomeActivity.class);

                startActivity(Home);
            }
        });


        dbHelper = new DatabaseHelper(this);

        databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        questionAct = databaseAccess.questionAct();

        int x = 0;
        for (QuestionModel name : questionAct)

            System.out.println("Quest" + (x++) + ": " + name.getRatingType() + ":" + name.getDescription());




        contactlv = (ListView) findViewById(R.id.listContact);
        showContacts();


        contactlv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                moveNextActivity();

            }
        });


    }

    /**
     * Show the contacts in the ListView.
     */
    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            List<String> contacts = getContactNames();
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contacts);
            contactlv.setAdapter(adapter);
        }
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }
    /**
     * Read the name of all the contacts.
     *
     * @return a list of names.
     */
    private List<String> getContactNames() {
        List<String> contacts = new ArrayList<>();
        // Get the ContentResolver
        ContentResolver cr = getContentResolver();
        // Get the Cursor of all the contacts
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        // Move the cursor to first. Also check whether the cursor is empty or not.
        if (cursor.moveToFirst()) {
            // Iterate through the cursor
            do {
                // Get the contacts name
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if(name==null){
                    name="Entrée vide";
                }
                contacts.add(name);
            } while (cursor.moveToNext());
        }
        // Close the curosor
        cursor.close();

        return contacts;
    }


    public void moveNextActivity() {
        SharedPreferences preferences = getSharedPreferences(PREFSHARE , 0);
        int Testing = preferences.getInt("questionAct1", 0);
        int  SizeQuestion = questionAct.size()-1;


        System.out.println("NextQuestion  Share " + Testing);
        System.out.println("ArraySIze Share" +SizeQuestion);
        if (SizeQuestion == Testing){
            //   Toast.makeText(this, "You Have Completed the Activity", Toast.LENGTH_SHORT).show();
        }
        else

        if (Testing != 0) {
            System.out.println("QuestionShared  " + Testing);

            if (questionAct.get(Testing).getRatingType().equals("competenceChoice")) {
                //  Toast.makeText(Activity_1.this, "competenceChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Activity_1.this, Activity_1_4.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
            //2
            else if (questionAct.get(Testing).getRatingType().equals("attributeChoice")) {
                //  Toast.makeText(Activity_1.this, "attributeChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Activity_1.this, Activity_1_2.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
            //3
            else if (questionAct.get(Testing).getRatingType().equals("animalChoice")) {
                // Toast.makeText(Activity_1.this, "animalChoice", Toast.LENGTH_SHORT).show();
                //  questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_1.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);
            }
            //4
            else if (questionAct.get(Testing).getRatingType().equals("elementChoice")) {
                // Toast.makeText(Activity_1.this, "elementChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_3.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);
            }
            //5

            else if (questionAct.get(Testing).getRatingType().equals("motivationChoice")) {
                //Toast.makeText(Activity_1.this, "motivationChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_8.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
            //6
            else if (questionAct.get(Testing).getRatingType().equals("seasonChoice")) {
                // Toast.makeText(Activity_1.this, "seasonChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_5.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
            //7
            else if (questionAct.get(Testing).getRatingType().equals("roleChoice")) {
                //  Toast.makeText(Activity_1.this, "roleChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_6.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
            //8
            else if (questionAct.get(Testing).getRatingType().equals("colorChoice")) {
                // Toast.makeText(Activity_1.this, "colorChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_7.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
        } else {


            //1
            if (questionAct.get(0).getRatingType().equals("competenceChoice")) {
                //  Toast.makeText(Activity_1.this, "competenceChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Activity_1.this, Activity_1_4.class);
                intent.putExtra("QuestionAct", 0);
                startActivity(intent);

            }
            //2
            else if (questionAct.get(0).getRatingType().equals("attributeChoice")) {
                // Toast.makeText(Activity_1.this, "attributeChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Activity_1.this, Activity_1_2.class);
                intent.putExtra("QuestionAct", 0);
                startActivity(intent);

            }
            //3
            else if (questionAct.get(0).getRatingType().equals("animalChoice")) {
                // Toast.makeText(Activity_1.this, "animalChoice", Toast.LENGTH_SHORT).show();
                //  questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_1.class);
                intent.putExtra("QuestionAct", 0);
                startActivity(intent);
            }
            //4
            else if (questionAct.get(0).getRatingType().equals("elementChoice")) {
                //   Toast.makeText(Activity_1.this, "elementChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_3.class);
                intent.putExtra("QuestionAct", 0);
                startActivity(intent);
            }
            //5

            else if (questionAct.get(0).getRatingType().equals("motivationChoice")) {
                // questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_8.class);
                intent.putExtra("QuestionAct", 0);
                startActivity(intent);

            }
            //6
            else if (questionAct.get(0).getRatingType().equals("seasonChoice")) {
                //  Toast.makeText(Activity_1.this, "seasonChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_5.class);
                intent.putExtra("QuestionAct", 0);
                startActivity(intent);

            }
            //7
            else if (questionAct.get(0).getRatingType().equals("roleChoice")) {
                //    Toast.makeText(Activity_1.this, "roleChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_6.class);
                intent.putExtra("QuestionAct", 0);
                startActivity(intent);

            }
            //8
            else if (questionAct.get(0).getRatingType().equals("colorChoice")) {
                //   Toast.makeText(Activity_1.this, "colorChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1.this, Activity_1_7.class);
                intent.putExtra("QuestionAct", 0);
                startActivity(intent);

            }
        }

    }


//       cursor1 =getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//               new String[] { ContactsContract.CommonDataKinds.Phone._ID,
//                       ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME ,ContactsContract.CommonDataKinds.Phone.NUMBER},
//               null, null,  null);
//       startManagingCursor(cursor1);
//        String [] from ={ ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME ,ContactsContract.CommonDataKinds.Phone.NUMBER};
//         int [] to = { R.id.lv_name ,R.id.lv_no};














    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.activity_1, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_contact) {
            Intent intentContact = new Intent(this, Activity_1.class);
            startActivity(intentContact);
            //storeAnswer();
            return true;
        }

        if (id == R.id.nav_settings) {
            Intent intentAccount = new Intent(this, Activity_UserAccount.class);
            startActivity(intentAccount);
            //storeAnswer();
            return true;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
