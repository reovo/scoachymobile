package com.reovo.scoachy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class Grid4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid4);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_account:
                Toast.makeText(Grid4.this, "Account", Toast.LENGTH_SHORT).show();
                Intent myintent = new Intent(Grid4.this, User_profile.class);
                startActivityForResult(myintent,1);
                return true;
            case R.id.item_language:
                Toast.makeText(Grid4.this, "Language", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, Grid4_1.class);
        startActivity(intent);
    }
}
