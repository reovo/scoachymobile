package com.reovo.scoachy;

/**
 * Created by DummyReovo on 11/07/2017.
 */

public class AnswerModel {

    private String AnswerID, description, ratingTypeId ;


    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String AnswerID) {
        this.AnswerID = AnswerID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRatingTypeId() {
        return ratingTypeId;
    }

    public void setRatingTypeId(String ratingTypeId) {
        this.ratingTypeId = ratingTypeId;
    }


}
