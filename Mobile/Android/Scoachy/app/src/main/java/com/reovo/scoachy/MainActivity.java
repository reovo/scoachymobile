package com.reovo.scoachy;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.Scope;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity  {
     EditText editUser, editPass;
    Button connect;

    private SignInButton google_login_button;
    private GoogleApiClient googleApiClient;
    public static final int SIGN_IN_CODE = 777;
    LoginButton login_button;
    CallbackManager callbackManager;

    private ImageView imgProfile;
    private TextView txtDetails;
    DatabaseHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        computePakageHash();

        dbHelper = new DatabaseHelper(this);
        dbHelper.getWritableDatabase();



        setContentView(R.layout.activity_main);


        editUser =(EditText)findViewById(R.id.username);
        editPass = (EditText)findViewById(R.id.password);
        connect = (Button) findViewById(R.id.connect);

//        connect.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (editUser.getText().toString().equals("admin")){
//                    editUser.setError("Username is not entered");
//                    editUser.requestFocus();
//                }
//                if(editPass.getText().toString().trim().length()==0){
//                    editPass.setError("Password is not entered");
//                    editPass.requestFocus();
//                }
//                else{
//                    Intent it=new Intent(getApplicationContext(), HomeActivity.class);
//                    startActivity(it);
//                }
//
//            }
//        });



        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editUser.getText().toString().equals("")) {
                    editUser.setError("L'identifiant est vide");
                    editUser.requestFocus();
                }

                else if(editPass.getText().toString().equals("")){
                    editPass.setError("Mot de passe est vide");
                    editPass.requestFocus();
                }

                else if(!editPass.getText().toString().equals("pascal"))
                {
                    editPass.setError("Mot de passe incorrecte");
                }

                else if(!editUser.getText().toString().equals("pascal"))
                {
                    editUser.setError("L'identifiant est incorrecte");
                }
                else if(!editUser.getText().toString().equals("pascal") && !editPass.getText().toString().equals("pascal"))
                {

                    editUser.setError("L'identifiant est incorrecte");
                    editPass.setError("Mot de passe incorrecte");
                }
                else
                {
                    Intent it=new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(it);
                }

            }
        });


    }

    private void computePakageHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.reovo.scoachy",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            Log.e("TAG",e.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);



        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);

    }









    // Build the list of member permissions our LinkedIn session requires
    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE, Scope.R_EMAILADDRESS);
    }




    public void onClick1(View view) {

        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    public void onClick2(View view) {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

//    public void click(View view) {
//        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
//        startActivity(intent);
//    }



}
