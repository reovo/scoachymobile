package com.reovo.scoachy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Grid2_2 extends AppCompatActivity {

    TextView txtQuestion, txtAttribute, txtDescription;

    ArrayList<QuestionModel> questions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid2_2);

        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        questions = databaseAccess.questions();

        txtQuestion = (TextView) findViewById(R.id.txt_Question);
        txtDescription = (TextView) findViewById(R.id.txt_Description);
        txtAttribute = (TextView) findViewById(R.id.txt_Attribute);

        txtAttribute.setText(questions.get(1).getAttribute());
        txtQuestion.setText(questions.get(1).getQuest());
        txtDescription.setText(questions.get(1).getDescription());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_account:
                Toast.makeText(Grid2_2.this, "Account", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item_language:
                Toast.makeText(Grid2_2.this, "Language", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }


    public void onClick(View view) {
        if (questions.get(2).getRatingType().equals("textChoice")){
            Toast.makeText(Grid2_2.this, "TextChoice", Toast.LENGTH_SHORT).show();
            questions.remove(1);
            Intent intent = new Intent(this, Grid2_2.class);
            startActivity(intent);
        }

        else if (questions.get(2).getRatingType().equals("textChoiceInfo")) {
            Toast.makeText(Grid2_2.this, "textChoiceInfo", Toast.LENGTH_SHORT).show();
            questions.remove(1);
            Intent intent = new Intent(this, Grid2_3.class);
            startActivity(intent);
        }


        else if (questions.get(2).getRatingType().equals("multipleChoice")){
            Toast.makeText(Grid2_2.this, "multipleChoice", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, Grid2.class);
            startActivity(intent);
        }

        else if (questions.get(2).getRatingType().equals("circularTextChoice")){
            Toast.makeText(Grid2_2.this, "circularTextChoice", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, Grid2_1.class);
            startActivity(intent);
        }
    }
}
