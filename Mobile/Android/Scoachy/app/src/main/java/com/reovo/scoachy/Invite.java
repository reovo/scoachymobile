package com.reovo.scoachy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class Invite extends AppCompatActivity {

    ListView listView;
    String []listIdeas ;
    ArrayAdapter <String> Listadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);

        listView = (ListView)findViewById(R.id.list_menu);
        listIdeas = getResources().getStringArray(R.array.listIdeas);
        Listadapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listIdeas);
        listView.setAdapter(Listadapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(),parent.getItemAtPosition(position)+ "is Selected",Toast.LENGTH_LONG).show();

            if(position==0){
                Intent myintent = new Intent(view.getContext(), Invite_1.class);
                startActivityForResult(myintent,1);
            }
                if(position==1){
                    Intent myintent = new Intent(view.getContext(), Invite_2.class);
                    startActivityForResult(myintent,1);
                }
                if(position==2){
                    Intent myintent = new Intent(view.getContext(), Invite_3.class);
                    startActivityForResult(myintent,2);
                }
                if(position==3){
                    Intent myintent = new Intent(view.getContext(), Invite_1.class);
                    startActivityForResult(myintent,2);
                }


            }



        });
    }





}
