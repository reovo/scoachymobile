package com.reovo.scoachy;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by DummyReovo on 13/07/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "ScoachyTable.db";
    private static final int DATABASE_VERSION =1;
   private static String DB_PATH = "";



    private static final String TABLE_NAME = "answerresult";
    private static final String ID ="id";
    private static final String USERID = "userId";
    private static final String ANSWERID ="answerId";
    private static final String LASTANSWEREDDATE = "lastAnsweredDate";
    private static final String QUESTIONID ="questionId";



   // SQLiteDatabase myDB;

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //this.myContext = context;
//        DB_PATH = "/data/data/"
//                + context.getApplicationContext().getPackageName()
//                + "/databases/";

        Log.d("DatabaseHelper", "DN");
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String  answerResult =  "create table if not exists " + TABLE_NAME +
                "("+
                ID + " INTEGER PRIMARY KEY, "+
                USERID + " TEXT, "+
                ANSWERID + " TEXT  ,"+
               LASTANSWEREDDATE +" TEXT , " +
                QUESTIONID +" TEXT " +

                ")";
        db.execSQL(answerResult);

        Log.d("DatabaseHelper", DATABASE_NAME);


    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


//    private void createTables(SQLiteDatabase db) {
//        String  answerResult =  "create table if not exists " + TABLE_NAME +
//                "("+
//                ID + " INTEGER PRIMARY KEY, "+
//                USERID + " TEXT NOT NULL, "+
//                ANSWERID + " TEXT NOT NULL, "+
//                LASTANSWEREDDATE +" TEXT NOT NULL, " +
//                QUESTIONID +" TEXT NOT NULL " +
//                ")";
//        db.execSQL(answerResult);
//
//    }


//    public void openDB(){
//        db = getWritableDatabase();
//    }
//
//    public void  closeDB(){
//        if(db != null && db.isOpen()) {
//            db.close();
//        }
//
//    }


    public boolean insertAnswer( String user_id,String answer_id, String question_id, String date_answered){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USERID,user_id);
        values.put(ANSWERID,answer_id);
        values.put(QUESTIONID,question_id);
        values.put(LASTANSWEREDDATE,date_answered);
        long result = db.insert(TABLE_NAME,null,values);

        if(result == -1)
            return false;
        else
            return true;



    }


}
