package com.reovo.scoachy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Activity_4_22_objectif extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4_22_objectif);
    }

    public void closeButton(View view) {
        Intent intent = new Intent(this,Activity_4_22.class);
        startActivity(intent);
    }
}
