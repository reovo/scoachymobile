package com.reovo.scoachy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.Profile;
import com.linkedin.platform.LISessionManager;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static android.content.SharedPreferences.*;

public class HomeActivity extends AppCompatActivity {
    ImageView grid2,grid1,grid3,grid4;
    TextView activityTitle, nietzche_quote,friedrich_accueil,activityTitle2,activityTitle3,activityTitle4;

    ArrayList<QuestionModel> questions;
    ArrayList<QuestionModel> questionAct;


    public static final String PREFS = "preferenceQuestion";
    public static final String PREFSHARE  = "preferenceOthers";
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


         activityTitle = (TextView)findViewById(R.id.activityTitle);
         activityTitle2 = (TextView)findViewById(R.id.activityTitle2);
         activityTitle3 = (TextView)findViewById(R.id.activityTitle3);
         activityTitle4 = (TextView)findViewById(R.id.activityTitle4);
         Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.ttf");
         activityTitle.setTypeface(typeface);
         activityTitle2.setTypeface(typeface);
         activityTitle3.setTypeface(typeface);
         activityTitle4.setTypeface(typeface);

         nietzche_quote=(TextView)findViewById(R.id.nietzche_quote);
         Typeface quote = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Medium.ttf");
         nietzche_quote.setTypeface(quote);

        friedrich_accueil=(TextView)findViewById(R.id.friedrich_accueil);
        Typeface friedrich = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Light_Oblique.otf");
        friedrich_accueil.setTypeface(friedrich);




        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
         //activity2
        questions = databaseAccess.questions();
         int x =0;
         for(QuestionModel name: questions)

             System.out.println("Question" + (x++)+": "+name.getRatingType() + ":" +name.getOrder());


        //activity 1
         questionAct = databaseAccess.questionAct();
         int y =0;
         for(QuestionModel act: questionAct)

             System.out.println("Question" + (x++)+": "+act.getRatingType() + ":" +act.getOrder());

         grid2 =(ImageView)findViewById(R.id.ce_que_je_pense_de_moi);
         grid1 =(ImageView)findViewById(R.id.ce_que_je_pense_des_autres);
         grid3 =(ImageView)findViewById(R.id.ce_que_les_autres_pensent_de_moi);
         grid4 =(ImageView) findViewById(R.id.ce_que_je_suis);


         SharedPreferences prefs = getSharedPreferences(PREFS , 0);
         int Testing = prefs.getInt("QuestionMessage", 0);
         System.out.println("Shared  " + Testing);

//         SharedPreferences preferences = getSharedPreferences(PREFSHARE , 0);
//         int TestingACT1 = preferences.getInt("questionAct1", 0);
//         System.out.println("ACTIVITY  " + Testing);


         grid2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                System.out.println("ArraySIze Share" +(questions.size()-1));

                moveNext();

                }


         });

         grid1.setOnClickListener(new View.OnClickListener(){

             @Override
             public void onClick(View v) {
                 System.out.println("ArraySIze Share" +(questionAct.size()-1));
//                 Intent Activity1 = new Intent(HomeActivity.this, Activity_1_1.class);
//                 Activity1.putExtra("QuestionAct", 0);
//                 startActivity(Activity1);
                 moveNextActivity();

             }


         });

         grid3.setOnClickListener(new View.OnClickListener(){

             @Override
             public void onClick(View v) {
                 System.out.println("ArraySIze Share" +(questionAct.size()-1));
                 Intent Activity3 = new Intent(HomeActivity.this, Activity_3_2.class);
               //  Intent Activity3 = new Intent(HomeActivity.this, Activity_3.class);
                 startActivity(Activity3);


             }


         });
         grid4.setOnClickListener(new View.OnClickListener(){

             @Override
             public void onClick(View v) {
                 System.out.println("ArraySIze Share" +(questionAct.size()-1));
                 Intent Activity4 = new Intent(HomeActivity.this, Activity_4.class);
                 startActivity(Activity4);


             }


         });



    }


    //Activity1Question
    public void moveNextActivity() {
        SharedPreferences preferences = getSharedPreferences(PREFSHARE , 0);
        int Testing = preferences.getInt("questionAct1", 0);
        int  SizeQuestion = questionAct.size()-1;


        System.out.println("NextQuestion  Share " + Testing);
        System.out.println("ArraySIze Share" +SizeQuestion);

        if (SizeQuestion == Testing){
            Intent intent = new Intent(HomeActivity.this, Activity_1.class);
            intent.putExtra("QuestionAct", 0);
            startActivity(intent);

        }
        else
        if (Testing != 0) {
            System.out.println("QuestionShared  " + Testing);

            if (questionAct.get(Testing).getRatingType().equals("competenceChoice")) {
          //      Toast.makeText(this, "competenceChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(HomeActivity.this, Activity_1_4.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
            //2
            else if (questionAct.get(Testing).getRatingType().equals("attributeChoice")) {
             //   Toast.makeText(this, "attributeChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(HomeActivity.this, Activity_1_1.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
            //3
            else if (questionAct.get(Testing).getRatingType().equals("animalChoice")) {
           //     Toast.makeText(this, "animalChoice", Toast.LENGTH_SHORT).show();
                //  questions.remove(0);
                Intent intent = new Intent(HomeActivity.this, Activity_1_1.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);
            }
            //4
            else if (questionAct.get(Testing).getRatingType().equals("elementChoice")) {
             //   Toast.makeText(this, "elementChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(HomeActivity.this, Activity_1_3.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);
            }
            //5

            else if (questionAct.get(Testing).getRatingType().equals("motivationChoice")) {
             //   Toast.makeText(this, "motivationChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(HomeActivity.this, Activity_1_8.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
            //6
            else if (questionAct.get(Testing).getRatingType().equals("seasonChoice")) {
            //    Toast.makeText(this, "seasonChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(HomeActivity.this, Activity_1_5.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
            //7
            else if (questionAct.get(Testing).getRatingType().equals("roleChoice")) {
             //   Toast.makeText(this, "roleChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(HomeActivity.this, Activity_1_6.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
            //8
            else if (questionAct.get(Testing).getRatingType().equals("colorChoice")) {
             //   Toast.makeText(this, "colorChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(HomeActivity.this, Activity_1_7.class);
                intent.putExtra("QuestionAct", Testing);
                startActivity(intent);

            }
        } else {
            Intent intent = new Intent(HomeActivity.this, Activity_1.class);
                intent.putExtra("QuestionAct", 0);
                startActivity(intent);

//
//            //1
//            if (questionAct.get(0).getRatingType().equals("competenceChoice")) {
//                Toast.makeText(this, "competenceChoice", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(HomeActivity.this, Activity_1_4.class);
//                intent.putExtra("QuestionAct", 0);
//                startActivity(intent);
//
//            }
//            //2
//            else if (questionAct.get(0).getRatingType().equals("attributeChoice")) {
//                Toast.makeText(this, "attributeChoice", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(HomeActivity.this, Activity_1_1.class);
//                intent.putExtra("QuestionAct", 0);
//                startActivity(intent);
//
//            }
//            //3
//            else if (questionAct.get(0).getRatingType().equals("animalChoice")) {
//                Toast.makeText(this, "animalChoice", Toast.LENGTH_SHORT).show();
//                //  questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_1_1.class);
//                intent.putExtra("QuestionAct", 0);
//                startActivity(intent);
//            }
//            //4
//            else if (questionAct.get(0).getRatingType().equals("elementChoice")) {
//                Toast.makeText(this, "elementChoice", Toast.LENGTH_SHORT).show();
//                // questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_1_3.class);
//                intent.putExtra("QuestionAct", 0);
//                startActivity(intent);
//            }
//            //5
//
//            else if (questionAct.get(0).getRatingType().equals("motivationChoice")) {
//                Toast.makeText(this, "motivationChoice", Toast.LENGTH_SHORT).show();
//                // questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_1_8.class);
//                intent.putExtra("QuestionAct", 0);
//                startActivity(intent);
//
//            }
//            //6
//            else if (questionAct.get(0).getRatingType().equals("seasonChoice")) {
//                Toast.makeText(this, "seasonChoice", Toast.LENGTH_SHORT).show();
//                // questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_1_5.class);
//                intent.putExtra("QuestionAct", 0);
//                startActivity(intent);
//
//            }
//            //7
//            else if (questionAct.get(0).getRatingType().equals("roleChoice")) {
//                Toast.makeText(this, "roleChoice", Toast.LENGTH_SHORT).show();
//                // questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_1_6.class);
//                intent.putExtra("QuestionAct", 0);
//                startActivity(intent);
//
//            }
//            //8
//            else if (questionAct.get(0).getRatingType().equals("colorChoice")) {
//                Toast.makeText(this, "colorChoice", Toast.LENGTH_SHORT).show();
//                // questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_1_7.class);
//                intent.putExtra("QuestionAct", 0);
//                startActivity(intent);
//
//            }
        }

    }


//Activity2 question
    public void moveNext() {
        SharedPreferences prefs = getSharedPreferences(PREFS , 0);
        int Testing = prefs.getInt("QuestionMessage", 0);
        int  SizeQuestion = questions.size()-1;


        if (SizeQuestion == Testing){
            Toast.makeText(this, "You Have Completed the Activity", Toast.LENGTH_SHORT).show();


        }
        else

       if (Testing != 0) {
//           System.out.println("QuestionShared  " + Testing);
//
//
//           System.out.println("NextQuestion  Share " + Testing);
//           System.out.println("ArraySIze Share" + SizeQuestion);
//           if (SizeQuestion == Testing) {
//               //    Toast.makeText(this, "You Have Completed the Activity", Toast.LENGTH_SHORT).show();
//           } else {

               if (questions.get(Testing).getRatingType().equals("competenceChoice")) {
                   //    Toast.makeText(this, "competenceChoice", Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(HomeActivity.this, Activity_2_3.class);
                   intent.putExtra("Question", Testing);
                   startActivity(intent);

               }
               //2
               else if (questions.get(Testing).getRatingType().equals("attributeChoice")) {
                   //  Toast.makeText(this, "attributeChoice", Toast.LENGTH_SHORT).show();
                   Intent intent = new Intent(HomeActivity.this, Activity_2_1.class);
                   intent.putExtra("Question", Testing);
                   startActivity(intent);

               }
               //3
               else if (questions.get(Testing).getRatingType().equals("animalChoice")) {
                   // Toast.makeText(this, "animalChoice", Toast.LENGTH_SHORT).show();
                   //  questions.remove(0);
                   Intent intent = new Intent(HomeActivity.this, Activity_2.class);
                   intent.putExtra("Question", Testing);
                   startActivity(intent);
               }
               //4
               else if (questions.get(Testing).getRatingType().equals("elementChoice")) {
                   //  Toast.makeText(this, "elementChoice", Toast.LENGTH_SHORT).show();
                   // questions.remove(0);
                   Intent intent = new Intent(HomeActivity.this, Activity_2_2.class);
                   intent.putExtra("Question", Testing);
                   startActivity(intent);
               }
               //5

               else if (questions.get(Testing).getRatingType().equals("motivationChoice")) {
                   //  Toast.makeText(this, "motivationChoice", Toast.LENGTH_SHORT).show();
                   // questions.remove(0);
                   Intent intent = new Intent(HomeActivity.this, Activity_2_7.class);
                   intent.putExtra("Question", Testing);
                   startActivity(intent);

               }
               //6
               else if (questions.get(Testing).getRatingType().equals("seasonChoice")) {
                   //   Toast.makeText(this, "seasonChoice", Toast.LENGTH_SHORT).show();
                   // questions.remove(0);
                   Intent intent = new Intent(HomeActivity.this, Activity_2_4.class);
                   intent.putExtra("Question", Testing);
                   startActivity(intent);

               }
               //7
               else if (questions.get(Testing).getRatingType().equals("roleChoice")) {
                   //    Toast.makeText(this, "roleChoice", Toast.LENGTH_SHORT).show();
                   // questions.remove(0);
                   Intent intent = new Intent(HomeActivity.this, Activity_2_5.class);
                   intent.putExtra("Question", Testing);
                   startActivity(intent);

               }
               //8
               else if (questions.get(Testing).getRatingType().equals("colorChoice")) {
                   //    Toast.makeText(this, "colorChoice", Toast.LENGTH_SHORT).show();
                   // questions.remove(0);
                   Intent intent = new Intent(HomeActivity.this, Activity_2_6.class);
                   intent.putExtra("Question", Testing);
                   startActivity(intent);

               }

       }else  {
           Intent intent = new Intent(HomeActivity.this, Activity_2.class);
                intent.putExtra("Question", 0);
                startActivity(intent);
//
//            //1
//            if (questions.get(0).getRatingType().equals("competenceChoice")) {
//            //    Toast.makeText(this, "competenceChoice", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(HomeActivity.this, Activity_2_3.class);
//                intent.putExtra("Question", 0);
//                startActivity(intent);
//
//            }
//            //2
//            else
//            if (questions.get(0).getRatingType().equals("attributeChoice")) {
//             //   Toast.makeText(this, "attributeChoice", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(HomeActivity.this, Activity_2_1.class);
//                intent.putExtra("Question", 0);
//                startActivity(intent);
//
//            }
//            //3
//            else
//                if (questions.get(0).getRatingType().equals("animalChoice")) {
//             //   Toast.makeText(this, "animalChoice", Toast.LENGTH_SHORT).show();
//                //  questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_2.class);
//                intent.putExtra("Question", 0);
//                startActivity(intent);
//            }
//            //4
//            else if (questions.get(0).getRatingType().equals("elementChoice")) {
//             //   Toast.makeText(this, "elementChoice", Toast.LENGTH_SHORT).show();
//                // questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_2_2.class);
//                intent.putExtra("Question", 0);
//                startActivity(intent);
//            }
//            //5
//
//            else if (questions.get(0).getRatingType().equals("motivationChoice")) {
//              //  Toast.makeText(this, "motivationChoice", Toast.LENGTH_SHORT).show();
//                // questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_2_7.class);
//                intent.putExtra("Question", 0);
//                startActivity(intent);
//
//            }
//            //6
//            else if (questions.get(0).getRatingType().equals("seasonChoice")) {
//              //  Toast.makeText(this, "seasonChoice", Toast.LENGTH_SHORT).show();
//             //   // questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_2_4.class);
//                intent.putExtra("Question", 0);
//                startActivity(intent);
//
//            }
//            //7
//            else if (questions.get(0).getRatingType().equals("roleChoice")) {
//          //      Toast.makeText(this, "roleChoice", Toast.LENGTH_SHORT).show();
//                // questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_2_5.class);
//                intent.putExtra("Question", 0);
//                startActivity(intent);
//
//            }
//            //8
//            else if (questions.get(0).getRatingType().equals("colorChoice")) {
//            //    Toast.makeText(this, "colorChoice", Toast.LENGTH_SHORT).show();
//                // questions.remove(0);
//                Intent intent = new Intent(HomeActivity.this, Activity_2_6.class);
//                intent.putExtra("Question", 0);
//                startActivity(intent);
//
//            }
        }

    }



//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.item_account:
//                Toast.makeText(HomeActivity.this, "Account", Toast.LENGTH_SHORT).show();
//                return true;
//            case R.id.item_language:
//                Toast.makeText(HomeActivity.this, "Language", Toast.LENGTH_SHORT).show();
//                return true;
////            case R.id.item_logout:
////                //Facebook Logout
////                AccessToken.setCurrentAccessToken(null);
////                Profile.setCurrentProfile(null);
////                //Gmail Logout
////                //LinkedIn Logout
////                LISessionManager.getInstance(getApplicationContext()).clearSession();
////                //Navigate to Login Screen
////                Intent intent = new Intent(this, MainActivity.class);
////                startActivity(intent);
////
////                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//
//        }
//    }
//

//        public void Activity1() {
//       Intent intent = new Intent(this, Activity_1_1.class);
//
//       //Intent intent = new Intent(this, Activity_1.class);
//        startActivity(intent);
//    }

//
//


}
