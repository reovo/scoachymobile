package com.reovo.scoachy;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class Invite_1 extends AppCompatActivity {
    ImageButton  contatList;
    private final int PICK_CONTACT =1;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_1);
        showContacts();
        contatList= (ImageButton)findViewById(R.id.imageContactList) ;
        contatList.setOnClickListener(new View.OnClickListener() {
            @Override
            //  RETRIEVE phone contact list
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK,ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent,PICK_CONTACT);
            }
        });
    }
    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data){
        super.onActivityResult(reqCode,resultCode,data);

        if(reqCode==PICK_CONTACT){
            if(resultCode== AppCompatActivity.RESULT_OK){
                Uri contactData =data.getData();
                ContentResolver resolver = getContentResolver();
                Cursor c = resolver.query(contactData,null,null,null,null);
                if (c.getCount()>0){
                    while(c.moveToNext()){
                        String id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String name= c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                        // Get display Name
                        TextView person_name = (TextView) findViewById(R.id.nom1);
                        if (person_name != null) {
                            person_name.setText(name);
                        }

                        // Pass  Data to next Activity
                        Intent secondActivityIntent = new Intent(this, Invite_2.class);
                        secondActivityIntent.putExtra("NAME", name);


                        if(Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0){
                            System.out.println("name : " + name + ", ID : " + id);
                            // get the phone number
                            Cursor phoneC = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +"=?", new String[]{id},null);
                            while(phoneC.moveToNext()){
                                String phone= phoneC.getString(phoneC.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                TextView person_num = (TextView) findViewById(R.id.phoneNumber1);
                                if (person_num != null) {
                                    person_num.setText(phone);
                                }
                                secondActivityIntent.putExtra("PHONE", phone);
                            }
                            phoneC.close();
                            //Get Email
                            Cursor emailC = resolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,null,ContactsContract.CommonDataKinds.Email.CONTACT_ID +"=?", new String[]{id},null);
                            while(emailC.moveToNext()){
                                String email= emailC.getString(emailC.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                                TextView person_email = (TextView) findViewById(R.id.email1);
                                if (person_email != null) {
                                    person_email.setText(email);
                                }
                                secondActivityIntent.putExtra("EMAIl", email);
                            }
                            emailC.close();
                        }

                        startActivity(secondActivityIntent);
                    }
                }
            }
        }
    }

}
