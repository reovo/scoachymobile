package com.reovo.scoachy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class Grid3_6 extends AppCompatActivity {

    float rainfall[] = {98.8f, 45.2f, 89.3f};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid3_6);

        setupPieChart();
    }

    private void setupPieChart(){
        //Populating a list of PieEntries
        List<PieEntry> pieEntries = new ArrayList<>();
        for(int i = 0; i<rainfall.length; i++){
            pieEntries.add(new PieEntry(rainfall[i]));
        }

        PieDataSet dataSet = new PieDataSet(pieEntries, "Rainfall");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData data = new PieData(dataSet);

        //Get the chart:
        PieChart chart = (PieChart) findViewById(R.id.chart);
        chart.setData(data);
        chart.invalidate();

        //Animate Pie
        chart.animateY(1000);

        //Enable/Disable Chart Legend
        Legend l = chart.getLegend();
        l.setEnabled(false);

        //Enable/Disable Draw Hole
        chart.setDrawHoleEnabled(true);

        //chart.setCenterText("SATISFACTION");
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, Grid3_7.class);
        startActivity(intent);
    }
}
