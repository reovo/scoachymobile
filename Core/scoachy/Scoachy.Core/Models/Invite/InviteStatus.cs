﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Models.Invite
{
    public enum InviteStatus
    {
        [Description("6b3edbb9-058c-11e7-ae28-c03fd56ee88c")]
        Pending,
        [Description("70bc1c3c-058c-11e7-ae28-c03fd56ee88c")]
        Accepted
    }
}
