﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Models.Scoachy
{
    public class Stats
    {        
        public string DatabaseConnectionStatus { get; set; }
        public int ActiveUsers { get; set; }
        public int InactiveUsers { get; set; }
        public int AwaitingConfirmationUsers { get; set; }
        public int ForgotPasswordUsers { get; set; }
        public int TotalUsers { get; set; }
        public int AcceptedInvites { get; set; }
        public int PendingInvites { get; set; }
        public int TotalInvites { get; set; }
        public int ActiveConnections { get; set; }
        public IList paths { get; set; }
        public string BackendVersionNumber { get; set; }
    }
}
