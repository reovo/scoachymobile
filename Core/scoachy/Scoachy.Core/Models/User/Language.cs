﻿using System.ComponentModel;


namespace Scoachy.Core.Models.User
{
    public enum Language
    {
        [Description("f4e15ba0-153c-11e7-a51d-c03fd56ee88c")]
        English = 1,
        [Description("f81d0011-153c-11e7-a51d-c03fd56ee88c")]
        French = 2
    }
}
