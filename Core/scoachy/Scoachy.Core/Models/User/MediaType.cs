﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Models.User
{
    public enum MediaType
    {
        [Description("db8db27f-058e-11e7-ae28-c03fd56ee88c")]
        Facebook,
        [Description("dcd26efb-058e-11e7-ae28-c03fd56ee88c")]
        Google,
        [Description("ee1304c1-058e-11e7-ae28-c03fd56ee88c")]
        LinkedIn,
        [Description("070d2129-0ae8-11e7-8999-c03fd56ee88c")]
        Email
    }
}
