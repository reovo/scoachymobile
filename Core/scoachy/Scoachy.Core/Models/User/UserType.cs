﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Models.User
{
    public enum UserType
    {
        [Description("3f60c714-1545-11e7-a51d-c03fd56ee88c")]
        Free,
        [Description("434b1c6f-1545-11e7-a51d-c03fd56ee88c")]
        Paid
    }
}
