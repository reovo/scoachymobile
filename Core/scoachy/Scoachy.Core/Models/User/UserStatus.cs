﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Models.User
{
    public enum UserStatus
    {
        [Description("0ffc9801-33ee-11e7-9426-c03fd56ee88c")]
        Active,
        [Description("15911129-33ee-11e7-9426-c03fd56ee88c")]
        Inactive,
        [Description("f63783a4-c549-11e7-9fbe-000c29a56235")]
        ForgotPassword,
        [Description("eba40fec-1625-11e8-9a8d-000c29a56235")]
        AwaitingConfirmation
    }
}
