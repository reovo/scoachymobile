﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoachy.Core.Models.Dto
{
    public class StatusDto
    {
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }
    }
}