﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Models.Dto
{
    public class SyncDataDto
    {
        public RegisterUserDto UserInfo { get; set; }
        public IList<FlatResultsDto> AnswerResults { get; set; }
        public string EncryptedSql { get; set; }
        public int LastDbVersion { get; set; }
        public StatusDto Status { get; set; }
    }
}
