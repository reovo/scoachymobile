﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Models.Dto
{
    public class SettingsDto
    {
        List<Setting> Settings { get; set; }
    }

    public class Setting
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set;  }
        public string SettingsType { get; set; }
        public string Value { get; set;  }
    }
}
