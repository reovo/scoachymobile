﻿using Reovo.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoachy.Core.Models.Dto
{
    public class ConfirmPaymentDto
    {
        public string UserId { get; set; }
        public string ScoachyTransactionId { get; set; }
        public string PaymentDate { get; set; }
        public string ApplicationSource { get; set;  }
        public string StoreTransactionId { get; set; }
        public string OrderId { get; set; }
    }
}