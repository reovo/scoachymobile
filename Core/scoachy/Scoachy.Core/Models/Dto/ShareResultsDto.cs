﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Scoachy.Core.Models.Dto
{
    public class ShareMultipleResultsDto
    {
        [JsonProperty(PropertyName = "ResultsList")]
        public List<ShareResultsDto> ResultsList { get; set; }
    }

    public class ShareResultsDto
    {
        [JsonProperty(PropertyName ="UserId")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "FriendUserId")]
        public string FriendUserId { get; set; }

        [JsonProperty(PropertyName = "TransferDate")]
        public string TransferDate { get; set; }

        [JsonProperty(PropertyName = "UserAnswers")]
        public List<UserAnswerDto> UserAnswers { get; set; }
    }

    public class UserAnswerDto
    {
        [JsonProperty(PropertyName = "AnswerId")]
        public string AnswerId { get; set; }

        [JsonProperty(PropertyName = "QuestionId")]
        public string QuestionId { get; set; }

        [JsonProperty(PropertyName = "LastAnsweredDate")]
        public string LastAnsweredDate { get; set; }
    }

    public class FlatResultsDto
    {
        [JsonProperty(PropertyName ="Id")]
        public string Id { get; set;  }

        [JsonProperty(PropertyName = "UserId")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "FriendUserId")]
        public string FriendUserId { get; set; }

        [JsonProperty(PropertyName = "AnswerId")]
        public string AnswerId { get; set; }

        [JsonProperty(PropertyName = "LastAnsweredDate")]
        public string LastAnsweredDate { get; set; }

        [JsonProperty(PropertyName = "QuestionId")]
        public string QuestionId { get; set; }

        [JsonProperty(PropertyName = "Version")]
        public int Version { get; set; }
    }
}