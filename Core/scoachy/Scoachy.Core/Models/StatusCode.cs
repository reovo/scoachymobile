﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Models
{
    public enum StatusCode
    {
        [Description("User account created successfully")]
        UserCreatedSuccessfully = 0,

        [Description("User already exists")]
        UserAlreadyExists = 1,

        [Description("User invited successfully")]
        UserInvitedSuccessfully = 2,

        [Description("User has already been invited")]
        UserHasAlreadyBeenInvited = 3,

        [Description("User Account not verified")]
        UserAccountNotVerified = 4,

        [Description("Username or Password is not correct")]
        UserAuthenticationFailed = 5,

        [Description("User forgot Password")]
        UserPasswordForget = 6,

        [Description("User request not formatted properly")]
        UserRequestInvalidFormat = 7,

        [Description("Duplicate User Email")]
        UserDuplicateEmail = 8,

        [Description("Mandatory field is empty")]
        UserMandatoryFieldEmpty = 9,

        [Description("Duplicate User Phone")]
        UserDuplicatePhone = 10,

        [Description("User does not exist")]
        UserDoesNotExist = 11,

        [Description("Scoachy Outdated")]
        ScoachyVersionOutdated = 12,

        [Description("User is Awaiting Confirmation(expired)")]
        UserAwaitingConfirmationExp = 13,

        [Description("User is Awaiting Confirmation(not expired)")]
        UserAwaitingConfirmationNotExp = 14,

        [Description("User is trying to register again(link expired)")]
        UserTryingRegistrationLinkExp = 15,

        [Description("User is trying to register again(link not expired)")]
        UserTryingRegistrationLinkNotExp = 16,

        [Description("Invalid Email")]
        InvalidEmail = 17,

        [Description("Invalid String. Invalid characters")]
        InvalidCharacters = 18

    }
}
