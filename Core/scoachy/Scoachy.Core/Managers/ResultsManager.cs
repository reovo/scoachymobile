﻿using Reovo.Lib;
using Scoachy.Core.Managers.Interfaces;
using Scoachy.Core.Models.Dto;
using Scoachy.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Managers
{
    public class ResultsManager : IResultsManager
    {
        private DBService dbService;

        public ResultsManager()
        {
            dbService = new DBService();
        }

        public ResultsManager(DBService dbService)
        {
            this.dbService = dbService;
        }


        public IList<FlatResultsDto> GetSummary(string userId)
        {
            IList<FlatResultsDto> flatResults = null;

            try
            {
                dbService.Connect();

                var resultsFromOthers = new ShareMultipleResultsDto();
                var getAllResultsSql = string.Format(
                    "SELECT id, userId, friendUserId, answerId, DATE_FORMAT(lastAnsweredDate,'%Y-%m-%d %T') as lastAnsweredDate,questionId, version FROM `answerresult` " +
                    "WHERE friendUserId = '{0}' UNION " +
                    "SELECT id, userId, friendUserId, answerId, DATE_FORMAT(lastAnsweredDate, '%Y-%m-%d %T') as lastAnsweredDate, questionId, version FROM `answerresult` " +
                    "WHERE userId = '{0}'", userId
                    );

                var resultData = dbService.RunSelect(getAllResultsSql);

                flatResults = resultData.AsEnumerable().Select(row =>
                    new FlatResultsDto
                    {
                        Id = row.Field<string>("id"),
                        UserId = row.Field<string>("userId"),
                        FriendUserId = row.Field<string>("friendUserId"),
                        AnswerId = row.Field<string>("answerId"),
                        LastAnsweredDate = row.Field<string>("lastAnsweredDate"),
                        QuestionId = row.Field<string>("questionId"),
                        Version = row.Field<int>("version")
                    }
                ).ToList();
            }
            catch(Exception e)
            {
                dbService.LogError(-1, "GetSummary", e.Message, dbService.Connection);
            }
            finally
            {
                dbService.Disconnect();
            }

            return flatResults;
        }

        public bool Send(ShareMultipleResultsDto results)
        {
            dbService.Connect();
            foreach (var user in results.ResultsList)
            {
                var userId = user.UserId;
                var friendUserId = user.FriendUserId;

                foreach (var answer in user.UserAnswers)
                {
                    var id = Guid.NewGuid().ToString();

                    var insertResultsSql = string.Format(
                        "INSERT INTO `answerresult` (`id`,`userId`,`friendUserId`,`answerId`,`lastAnsweredDate`,`questionId`) " +
                        "VALUES ('{0}','{1}', '{2}', '{3}', '{4}','{5}')",
                        id, userId, friendUserId, answer.AnswerId, answer.LastAnsweredDate, answer.QuestionId
                        );

                    dbService.RunCommand(insertResultsSql);
                }
            }
            dbService.Disconnect();
            return true;
        }

        public bool SendFlat(List<FlatResultsDto> results)
        {
            dbService.Connect();
            foreach (var result in results)
            {
                var id = Guid.NewGuid().ToString();

                var insertResultsSql = string.Format(
                    "INSERT INTO `answerresult` (`id`,`userId`,`friendUserId`,`answerId`,`lastAnsweredDate`,`questionId`) " +
                    "VALUES ('{0}','{1}', '{2}', '{3}', '{4}','{5}')",
                    id, result.Id, result.FriendUserId, result.AnswerId, result.LastAnsweredDate, result.QuestionId
                    );

                    dbService.RunCommand(insertResultsSql);             
            }
            dbService.Disconnect();
            return true;
        }
    }
}
