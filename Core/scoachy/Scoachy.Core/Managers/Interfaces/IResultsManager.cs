﻿using Scoachy.Core.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Managers.Interfaces
{
    public interface IResultsManager
    {
        bool Send(ShareMultipleResultsDto results);
        IList<FlatResultsDto> GetSummary(string userId);
    }
}
