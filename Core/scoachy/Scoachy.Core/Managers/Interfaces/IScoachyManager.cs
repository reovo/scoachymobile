﻿using Scoachy.Core.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Managers.Interfaces
{
    public interface IScoachyManager
    {
        bool ConfirmPayment(ConfirmPaymentDto confirmedData);
        HomeScreenStatsDto GetGlobalStatistics();
        SyncDataDto GetSyncData(string userId, int dbVersion);
        bool ReportIssue(ReportIssueDto issue);
    }
}
