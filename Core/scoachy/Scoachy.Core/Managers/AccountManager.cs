﻿using System;
using System.Data;
using Scoachy.Core.Models.User;
using Scoachy.Data;
using Scoachy.Core.Managers.Interfaces;
using Scoachy.Core.Models.Dto;
using Reovo.Lib;
using System.Collections.Generic;
using System.Linq;
using Scoachy.Core.Models;
using Scoachy.Core.Models.Invite;
using System.Resources;
using System.Reflection;
using System.Configuration;
using Reovo.Lib.Validators;

namespace Scoachy.Core.Managers
{

    public class AccountManager : IAccountManager
    {
        private DBService dbService;

        public AccountManager()
        {
            dbService = new DBService();
        }

        public AccountManager(DBService dbService)
        {
            this.dbService = dbService;
        }

        public RegisterUserReturnDto RegisterUser(RegisterUserDto user)
        {

            dbService.Connect();

            var valid = ValidateUserObject(user);

            if (!valid.Success)
            {
                dbService.Disconnect();
                return valid;
            }
            //Check if user is 


           
            //Check if user was invited before ? Or comes from an invite?
            var UserId = string.Empty;
            var friendUserId = UserAlreadyInvited(user.Email);

            if (friendUserId != string.Empty)
            {
                UserId = friendUserId;
                UpdateInviteStatus(friendUserId);
            }
            else
            {
                UserId = Guid.NewGuid().ToString();
            }

            user.Id = UserId;

            //insert new record in user table
            var insertNewUser = string.Format(
                "INSERT INTO `user` (`id`,`username`,`password`,`salt`,`createdOn`,`lastLogin`," +
                "`userTypeId`,`loginAttempt`,`languageId`,`userStatusId`,`mediaTypeId`,`deviceToken`,`os`,`osversion`,`appversion`) " +
                "VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', " +
                "'{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}','{13}',{14})",
                user.Id, user.Username, user.EncryptedPassword, user.Salt,
                ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now), ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now),
                GenericManager.ConvertToUserType(user.UserType), 0, GenericManager.GetLanguageCode(user.Language), user.UserStatus,
                GenericManager.ConvertToMediaType(user.RegistrationMode), user.DeviceToken,
                user.Os, user.OsVersion, user.AppVersion);

            valid.Success = dbService.RunCommand(insertNewUser);

            if (!valid.Success)
            {
                return valid;
            }

            //insert new record in user detail
            string insertUserDetail;

            insertUserDetail = string.Format(
                                "INSERT INTO `userdetail` (`id`,`userId`, `firstname`, `lastname`, `dateOfBirth`, `gender`) " +
                                "VALUES('{0}','{1}','{2}','{3}','{4}','{5}')",
                                Guid.NewGuid().ToString(), 
                                user.Id, 
                                user.FirstName, 
                                user.LastName,
                                user.BirthDate, 
                                user.Gender);

            insertUserDetail = insertUserDetail.Replace("'null'", "null");
            insertUserDetail = insertUserDetail.Replace("''", "null");

            valid.Success = dbService.RunCommand(insertUserDetail);


            if (!valid.Success)
            {
                return valid;
            }

            //make new entry in user phone table
            var insertPhone = string.Format(
                "INSERT INTO `userphone` (`id`,`userId`, `phoneNumber`, `phonePrefix`) " +
                "VALUES('{0}', '{1}','{2}', '{3}' )",
                Guid.NewGuid().ToString(), user.Id, user.Phone, user.PhonePrefix);

            insertPhone = insertPhone.Replace("'null'", "null");
            insertPhone = insertPhone.Replace("''", "null");

            valid.Success = dbService.RunCommand(insertPhone);

            if (!valid.Success)
            {
                return valid;
            }

            //make new entry in user email table
            var insertEmail = string.Format(
                "INSERT INTO `useremail` (`id`,`userId`, `email`) " +
                "VALUES('{0}', '{1}', '{2}')",
                Guid.NewGuid().ToString(), user.Id, user.Email);

            valid.Success = dbService.RunCommand(insertEmail);

            if (!valid.Success)
            {
                return valid;
            }

            //make new entry in user email table
            var insertAddress = string.Format(
                "INSERT INTO `useraddress` (`id`,`userId`, `code`,`countryId`) " +
                "VALUES('{0}', '{1}', '{2}', '{3}')",
                Guid.NewGuid().ToString(), user.Id, user.PostalCode, user.Country);


            insertAddress = insertAddress.Replace("'null'", "null");
            insertAddress = insertAddress.Replace("''", "null");

            valid.Success = dbService.RunCommand(insertAddress);

            if (!valid.Success)
            {
                return valid;
            }

            try
            {
                valid = UserActivation(user);
            }
            catch (Exception e)
            {
                dbService.LogError(-1, "Account-UserActivation", e.Message, dbService.Connection);
            }

            if (!valid.Success)
            {
                return valid;
            }

            dbService.Disconnect();

            //Successful Transaction
            var newUser = new RegisterUserReturnDto();
            newUser.Id = user.Id;

            return newUser;
        }

        private string UserAlreadyInvited(string email)
        {
            var checkInviteTable = string.Format("SELECT id, userId, friendUserId, email, inviteStatusId FROM invite WHERE " +
                "email = '{0}'", email
                );
            var inviteData = dbService.RunSelect(checkInviteTable);

            IList<ContactDto> dbContactList = inviteData.AsEnumerable().Select(row =>
            new ContactDto
            {
                Id = row.Field<string>("id"),
                UserId = row.Field<string>("userId"),
                FriendUserId = row.Field<string>("friendUserId"),
                Email = row.Field<string>("email"),
                InviteStatus = row.Field<string>("inviteStatusId")
            }).ToList();

            if (dbContactList.Count != 0)
            {
                return dbContactList.First().FriendUserId;
            }
            else
            {
                return string.Empty;
            }
        }

        private void UpdateInviteStatus(string userId)
        {
            var updateInviteTable = string.Format(
                "UPDATE `invite` SET inviteStatusId = '{0}' WHERE friendUserId = '{1}' ",
                EnumUtils.GetEnumDescription(InviteStatus.Accepted), userId);

            dbService.RunCommand(updateInviteTable);
        }

        private string CheckUserAwaitingConfirmation(string username)
        {
            var checkUserStatusSQL = string.Format("SELECT userStatusId FROM user WHERE username = '{0}'", username);
            var userStatusData = dbService.RunSelect(checkUserStatusSQL);

            IList<string> userStatusList = userStatusData.AsEnumerable().Select(row =>
                row.Field<string>("userStatusId")
            ).ToList();

            string userStatus = userStatusList.FirstOrDefault();

            if (userStatus == EnumUtils.GetEnumDescription(UserStatus.AwaitingConfirmation))
            {
                var retrieveUserId = string.Format("SELECT id FROM user WHERE username = '{0}'", username);
                var userIdData = dbService.RunSelect(retrieveUserId);
                IList<string> userIdList = userIdData.AsEnumerable().Select(row =>
                        row.Field<string>("id")
                ).ToList();
                string userId = "";
                if (userIdList == null) //check if data retrieved is null
                {
                    Console.WriteLine("Error!UserId is null");
                }
                else
                {
                    userId = userIdList.FirstOrDefault();
                }

                var checkLinkStatusSQL = string.Format("SELECT expired FROM useractivation WHERE userId = '{0}'", userId);
                var linkStatusData = dbService.RunSelect(checkLinkStatusSQL);
                IList<int> linkStatusList = linkStatusData.AsEnumerable().Select(row =>
                        row.Field<int>("expired")
                ).ToList();
                int linkStatus=-1;
                if (linkStatusList == null) //check if data retrieved is null
                {
                    Console.WriteLine("Error!expired is null");
                }
                else
                {
                    linkStatus = linkStatusList.FirstOrDefault();
                }

                if (linkStatus == 1)
                {
                    var retrieveLinkSQL = string.Format("SELECT link FROM useractivation WHERE userId = '{0}'", userId);
                    var linkData = dbService.RunSelect(retrieveLinkSQL);
                    IList<string> linkList = linkData.AsEnumerable().Select(row =>
                            row.Field<string>("link")
                    ).ToList();
                    string link = "";
                    if (linkList == null) //check if data retrieved is null
                    {
                        Console.WriteLine("Error!link is null");
                    }
                    else
                    {
                        link = linkList.FirstOrDefault();
                    }
                    var userlink = "http://" + ConfigurationManager.AppSettings["IP"] + "/account/activation/" + link;
                    ResourceManager rm = new ResourceManager("Scoachy.Core.EmailText", Assembly.GetExecutingAssembly());
                    var emailBody = Mailer.ReplaceTextWithResource("email.html", "@mytext@", rm.GetString("Activation") + userlink + rm.GetString("ActivationLink"));
                    Mailer.SendMail(username, emailBody); //sending email to user
                    var updateUserCommand = string.Format(
                        "UPDATE `user` SET createdOn='{0}' WHERE id='{1}';", ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now), userId);
                    dbService.RunCommand(updateUserCommand);
                    var updateCommand = string.Format(
                        "UPDATE useractivation SET expired='{0}' WHERE link='{1}';", 0,link);
                    dbService.RunCommand(updateCommand); //update table useractivation
                    return "Link sent again";
                }
                else if (linkStatus == 0)
                {
                    var retrieveDateCreated = string.Format("SELECT createdOn FROM user WHERE id = '" + userId + "' LIMIT 1;");
                    var dateCreatedData = dbService.RunSelect(retrieveDateCreated);
                    IList<DateTime> dateCreatedList = dateCreatedData.AsEnumerable().Select(row =>
                            row.Field<DateTime>("createdOn")
                    ).ToList();
                    DateTime dateCreated = DateTime.Now;
                    DateTime dateNow = DateTime.Now;
                    if (dateCreatedList == null)
                    {
                        Console.WriteLine("Error!Date Created is null");
                    }
                    else
                    {
                        dateCreated = dateCreatedList.FirstOrDefault();
                    }
                    int time = Convert.ToInt32(ConfigurationManager.AppSettings["ExpiryTime"]);
                    DateTime dateExpired = dateCreated.AddHours(time); //calculate expiry date
                    if (dateNow > dateExpired) //check if link has expired
                    {
                        var retrieveLinkSQL = string.Format("SELECT link FROM useractivation WHERE userId = '{0}'", userId);
                        var linkData = dbService.RunSelect(retrieveLinkSQL);
                        IList<string> linkList = linkData.AsEnumerable().Select(row =>
                                row.Field<string>("link")
                        ).ToList();
                        string link = "";
                        if (linkList == null) //check if data retrieved is null
                        {
                            Console.WriteLine("Error!link is null");
                        }
                        else
                        {
                            link = linkList.FirstOrDefault();
                        }
                        var userlink = "http://" + ConfigurationManager.AppSettings["IP"] + "/account/activation/" + link;
                        ResourceManager rm = new ResourceManager("Scoachy.Core.EmailText", Assembly.GetExecutingAssembly());
                        var emailBody = Mailer.ReplaceTextWithResource("email.html", "@mytext@", rm.GetString("Activation") + userlink + rm.GetString("ActivationLink"));
                        Mailer.SendMail(username, emailBody); //sending email to user
                        var updateUserCommand = string.Format(
                            "UPDATE `user` SET createdOn='{0}' WHERE id='{1}';", ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now), userId);
                        dbService.RunCommand(updateUserCommand);
                        var updateCommand = string.Format(
                            "UPDATE useractivation SET expired='{0}' WHERE link='{1}';", 0, link);
                        dbService.RunCommand(updateCommand); //update table useractivation
                        return "Link sent again";
                    }
                    else
                    {
                        //dbService.Disconnect();
                        return "Message to check email";
                    }
                }
            }
            return "ok";
        }

        public int ResendLink(string userId)
        {
            dbService.Connect();
            var retrieveLinkSQL = string.Format("SELECT link FROM useractivation WHERE userId = '{0}'", userId);
            var linkData = dbService.RunSelect(retrieveLinkSQL);
            IList<string> linkList = linkData.AsEnumerable().Select(row =>
                    row.Field<string>("link")
            ).ToList();
            string link = "";
            if (linkList == null) //check if data retrieved is null
            {
                Console.WriteLine("Error!link is null");
            }
            else
            {
                link = linkList.FirstOrDefault();
            }
            var accountManager = new AccountManager(dbService);
            var user = accountManager.GetUser(userId);
            var userlink = "http://" + ConfigurationManager.AppSettings["IP"] + "/account/activation/" + link; //link to be sent to user
            ResourceManager rm = new ResourceManager("Scoachy.Core.EmailText", Assembly.GetExecutingAssembly());
            var emailBody = Mailer.ReplaceTextWithResource("email.html", "@mytext@", rm.GetString("Activation") + userlink + rm.GetString("ActivationLink"));
            Mailer.SendMail(user.Username, emailBody); //sending email to user

            var updateUseractivationCommand = string.Format("UPDATE `useractivation` SET expired = '0' WHERE link = '" + link + "';");
            dbService.RunCommand(updateUseractivationCommand);

            var updateUserCommand = string.Format("UPDATE `user` SET createdOn = '" + ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now) + "' WHERE id = '" + userId + "';");
            dbService.RunCommand(updateUseractivationCommand);
            dbService.Disconnect();
            return 0;
        }

        private RegisterUserReturnDto ValidateUserObject(RegisterUserDto user)
        {
            if (string.IsNullOrEmpty(user.EncryptedPassword))
            {
                return UserEmptyField;
            }

            if (string.IsNullOrEmpty(user.Username))
            {
                return UserEmptyField;
            }

            if (!Validator.Email(user.Username))
            {
                return UserInvalidEmail;
            }

            if (Validator.SQLInjection(user.Username))
            {
                return UserInvalidCharacters;
            }

            if (Validator.SQLInjection(user.FirstName))
            {
                return UserInvalidCharacters;
            }

            if (Validator.SQLInjection(user.LastName))
            {
                return UserInvalidCharacters;
            }

            if (Validator.SQLInjection(user.Phone))
            {
                return UserInvalidCharacters;
            }

            //check for duplicate emails from `user`
            var checkEmailExistSQL = string.Format("SELECT count(username) FROM user WHERE username = '{0}' " +
                "AND userStatusId = '{1}'", user.Username, EnumUtils.GetEnumDescription(UserStatus.Active));

            int countMail = -1;

            try
            {
                countMail = dbService.RunCount(checkEmailExistSQL);
            }
            catch (Exception e)
            {
                dbService.LogError(-1, "ValidateUserObject", e.Message, dbService.Connection);
            }

            if (countMail != 0)
            {
                
                return UserDuplicateEmail;
            }

            var userStatus = CheckUserAwaitingConfirmation(user.Username);
            var userObj = GetUserDetails(user.Username);

            if (userStatus == "Message to check email")
            {
                return new RegisterUserReturnDto
                {
                    Id = (userObj?.Id),
                    ErrorCode = (int)StatusCode.UserTryingRegistrationLinkNotExp,
                    Message = "User trying to register again.Check email",
                    Success = false
                };
            }
            else if (userStatus == "Link sent again")
            {
                return new RegisterUserReturnDto
                {
                    Id = (userObj?.Id),
                    ErrorCode = (int)StatusCode.UserTryingRegistrationLinkExp,
                    Message = "User trying to register again.Link sent again",
                    Success = false
                };
            }
            return new RegisterUserReturnDto();
        }

        public RegisterUserReturnDto UpdateUser(RegisterUserDto user)
        {
            string updateUser;

            dbService.Connect();

            if (string.IsNullOrEmpty(user.EncryptedPassword) || string.IsNullOrEmpty(user.Salt))
            {
                // Update existing user in user 
                updateUser = string.Format(
                "UPDATE `user` SET lastLogin = '{0}'," +
                "languageId = '{1}',deviceToken = '{2}' ,os = '{3}',osversion = '{4}',appversion = {5} " +
                "WHERE Id = '{6}'",
                ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now),
                GenericManager.GetLanguageCode(user.Language), user.DeviceToken,
                user.Os, user.OsVersion, user.AppVersion, user.Id);
            }
            else
            {
                // Update existing user in user 
                updateUser = string.Format(
                "UPDATE `user` SET password = '{0}' , salt = '{1}' , lastLogin = '{2}'," +
                "languageId = '{3}',deviceToken = '{4}' ,os = '{5}',osversion = '{6}', appversion = {7} " +
                "WHERE Id = '{8}'",
                user.EncryptedPassword, user.Salt, ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now),
                GenericManager.GetLanguageCode(user.Language), user.DeviceToken,
                user.Os, user.OsVersion, user.AppVersion, user.Id);
            }


            dbService.RunCommand(updateUser);

            string updateUserDetail;

            // Update existing user in userdetails
            updateUserDetail = string.Format(
                "UPDATE `userdetail` SET firstname = '{0}', lastname = '{1}', dateOfBirth = '{2}', gender = '{3}' " +
                "WHERE userId = '{4}'",
                user.FirstName, user.LastName, user.BirthDate, user.Gender, user.Id);

            updateUserDetail = updateUserDetail.Replace("'null'", "null");
            updateUserDetail = updateUserDetail.Replace("''", "null");

            dbService.RunCommand(updateUserDetail);

            // Update existing user in user phone
            var updatePhone = string.Format(
                "UPDATE `userphone` SET phoneNumber = '{0}', phonePrefix = '{1}' " +
                "WHERE userId = '{2}'",
                user.Phone, user.PhonePrefix, user.Id);

            updatePhone = updatePhone.Replace("'null'", "null");
            updatePhone = updatePhone.Replace("''", "null");

            dbService.RunCommand(updatePhone);

            // Update existing user in user mail
            var updateAddress = string.Format(
                "UPDATE `useraddress` SET code = '{0}', countryId = '{1}' " +
                "WHERE userId = '{2}'",
                user.PostalCode, user.Country, user.Id);

            updateAddress = updateAddress.Replace("'null'", "null");
            updateAddress = updateAddress.Replace("''", "null");

            dbService.RunCommand(updateAddress);

            dbService.Disconnect();

            //Save Transaction
            var updatedUser = new RegisterUserReturnDto();
            updatedUser.Id = user.Id;

            return updatedUser;
        }


        public decimal getLowerAppVersion()
        {
            return 1.01m;
        }

        public decimal getHigherAppVersion()
        {
            return 2.0m;
        }

        public UserLoginReturnDto UserLogin(UserLoginDto user)
        {
            if (string.IsNullOrEmpty(user.Password))
            {
                return new UserLoginReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.UserMandatoryFieldEmpty,
                    Message = "Password is empty",
                    Success = false
                };
            }

            if (string.IsNullOrEmpty(user.UserName))
            {
                return new UserLoginReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.UserMandatoryFieldEmpty,
                    Message = "Username is empty",
                    Success = false
                };
            }

            if (!Validator.Email(user.UserName))
            {
                return new UserLoginReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.InvalidEmail,
                    Message = "Invalid email",
                    Success = false
                };
            }

            if (Validator.SQLInjection(user.UserName))
            {
                return new UserLoginReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.InvalidCharacters,
                    Message = "Invalid characters in Username",
                    Success = false
                };
            }

            dbService.Connect();
            bool passwordPassed = false;
            bool forgetPasswordPassed = false;
            UserLoginDto dbUser = new UserLoginDto();

            
            if (user.AppVersion < getLowerAppVersion()   || 
                user.AppVersion > getHigherAppVersion())
            {
                return new UserLoginReturnDto
                {
                    ErrorCode = (int)StatusCode.ScoachyVersionOutdated,
                    Id = null,
                    Message = "Scoachy version is outdated",
                    Success = false
                };
            }

            var userStatus = CheckUserAwaitingConfirmation(user.UserName);
            var userObj = GetUserDetails(user.UserName);

            if (userStatus == "Message to check email")
            {
                return new UserLoginReturnDto
                {
                    Id = (userObj?.Id),
                    ErrorCode = (int)StatusCode.UserAwaitingConfirmationNotExp,
                    Message = userStatus,
                    Success = false
                };
            }
            else if (userStatus == "Link sent again")
            {
                return new UserLoginReturnDto
                {
                    Id = (userObj?.Id),
                    ErrorCode = (int)StatusCode.UserAwaitingConfirmationExp,
                    Message = userStatus,
                    Success = false
                };
            }

            
            try
            {
                //Check if user already exists
                var userCheckSql = string.Format("SELECT id, username, password, salt, userStatusId FROM user WHERE username = '{0}'", user.UserName);

                var userList = dbService.RunSelect(userCheckSql);

                IList<UserLoginDto> dbUserList = userList.AsEnumerable().Select(row =>
                new UserLoginDto
                {
                    Id = row.Field<string>("id"),
                    UserName = row.Field<string>("username"),
                    Password = row.Field<string>("password"),
                    Salt = row.Field<string>("salt"),
                    UserStatus = row.Field<string>("userStatusId")
                }).ToList();



                if (dbUserList == null || dbUserList.Count == 0)
                {
                    dbService.Disconnect();
                    return UserFailedAuthentication;
                }

                dbUser = dbUserList.First();

                //Check Forget Password status in Database - if true
                var forgetPasswordStatus = EnumUtils.GetValueFromDescription<UserStatus>(dbUser.UserStatus);
                passwordPassed = true;
                forgetPasswordPassed = false;

                if (forgetPasswordStatus == UserStatus.ForgotPassword)
                {
                    forgetPasswordPassed = ForgetPasswordLogic(dbUser.Id, user.Password);
                    if (forgetPasswordPassed)
                    {
                        UpdateUserStatus(dbUser.Id, UserStatus.Active);
                    }
                }

                //Check if password and salt are correct
                if (Encryption.Encrypt(user.Password, dbUser.Salt) != dbUser.Password.Trim())
                {
                    passwordPassed = false;
                }
                else
                {
                    UpdateUserStatus(dbUser.Id, UserStatus.Active);
                }
            }
            catch (Exception e)
            {
                dbService.LogError(-1, "userlogin", e.Message, dbService.Connection);
            }
            finally
            { 
                dbService.Disconnect();
            }

            if (forgetPasswordPassed || passwordPassed)
            {
                var userLogin = new UserLoginReturnDto
                {
                    Id = dbUser.Id,
                    ErrorCode = 0,
                    Success = true,
                    Message = string.Empty
                };

                return userLogin;
            }
            else
            {
                return UserFailedAuthentication;
            }           
        }

        private bool ForgetPasswordLogic(string userId,string passwordEntered)
        {
            var updateServicedStatusSql = string.Format(
                "UPDATE `usertemppassword` SET serviced = {0} " +
                "WHERE userId = '{1}' AND expiryDate < '{2}'",
                2, userId, ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now));

            var result = dbService.RunCommand(updateServicedStatusSql);

            //Find data for that user from temp table
            var tempPasswordUserSql = string.Format("SELECT userId,randomGeneratedPassword, expiryDate, serviced FROM `usertemppassword` " +
                "WHERE serviced = 0 AND userId = '{0}' ORDER BY expiryDate DESC"
                ,userId);

            var tempUserTable = dbService.RunSelect(tempPasswordUserSql);

            IList<ForgetPasswordDto> userTempPasswordList = tempUserTable.AsEnumerable().Select(row =>
                new ForgetPasswordDto
                {
                    UserId = row.Field<string>("userId"),
                    RandomPassword = row.Field<string>("randomGeneratedPassword"),
                    ExpiryDate = row.Field<DateTime>("expiryDate"),
                    Serviced = row.Field<int>("serviced") == 0 ? false : true
                }
            ).ToList();

            var dbTempUser = userTempPasswordList.FirstOrDefault();

            if (dbTempUser == null)
            {
                return false;
            }

            if (DateTime.Now > dbTempUser.ExpiryDate )
            {
                return false;
            }

            if (dbTempUser.RandomPassword != passwordEntered)
            {
                return false;
            }

            //change status of temp password to serviced
            result = UpdateUserTempPasswordStatus(userId, passwordEntered, 1);

            if (result == false)
            {
                return false;
            }

            //encrypt changed password, generate salt and save to user table
            var salt = RandomGenerator.GenerateRandomPassword(18);
            var encryptedPassword = Encryption.Encrypt(passwordEntered, salt );
            var updateUserSql = string.Format("UPDATE `user` SET password = '{0}', salt = '{1}', userStatusId = '{2}' " + 
                "WHERE id  = '{3}'",  encryptedPassword, salt, EnumUtils.GetEnumDescription(UserStatus.Active), userId
                );

            result = dbService.RunCommand(updateUserSql);

            return result;
        }

        public bool UpdateUserTempPasswordStatus(string userId, string passwordEntered, int newStatus )
        {
            var updateTempPasswordStatusSql = string.Format(
                "UPDATE `usertemppassword` SET serviced = {0} " +
                "WHERE userId = '{1}' AND randomGeneratedPassword = '{2}'",
                newStatus, userId, passwordEntered);

            var result = dbService.RunCommand(updateTempPasswordStatusSql);

            return result;
        }

        public RegisterUserDto GetUser(string userId)
        {
            var userInfoSql = string.Format(
                "SELECT u.id AS Id, u.username AS Username,d.firstName AS FirstName,d.lastName AS LastName," +
                "d.gender AS Gender, d.dateOfBirth AS BirthDate, a.code AS PostalCode, u.username AS Email, " +
                "p.phoneNumber AS Phone, p.phonePrefix AS PhonePrefix,a.countryId AS Country, u.languageId AS `Language`," + 
                "u.userStatusId AS UserStatus, u.userTypeId AS UserType FROM `user` u " + 
                "LEFT JOIN useraddress a ON  u.id = a.`userId` " +
                "LEFT JOIN userphone p ON u.`id` = p.`userId` " + 
                "LEFT JOIN `userdetail` d ON u.`id` = d.`userId` WHERE u.id = '{0}' ", userId
            );
            var userList = dbService.RunSelect(userInfoSql);

            IList<RegisterUserDto> usersListData = userList.AsEnumerable().Select(row =>
                new RegisterUserDto
                {     
                    Id = row.Field<string>("Id"),
                    Username = row.Field<string>("Username"),
                    FirstName = row.Field<string>("FirstName"),
                    LastName = row.Field<string>("LastName"),
                    Gender = row.Field<string>("Gender"),
                    BirthDate = ConversionUtils.ConvertDateToMySqlFormat(row.Field<DateTime?>("BirthDate")),
                    PostalCode = row.Field<string>("PostalCode"),
                    Email = row.Field<string>("Email"),
                    Phone = row.Field<string>("Phone"),
                    PhonePrefix = row.Field<string>("PhonePrefix"),
                    Country = row.Field<string>("Country"),
                    Language = row.Field<int>("Language").ToString(),
                    ReceiveNewsletter = false,
                    UserStatus = row.Field<string>("UserStatus"),
                    UserType = row.Field<string>("UserType"),
                }
            ).ToList();

            if (usersListData.Count != 0)
            {
                return usersListData.First();
            }
            else
            {
                return null;
            }
        }

        public RegisterUserDto GetUserDetails(string userName)
        {
            var userInfoSql = string.Format(
                "SELECT u.id AS Id, u.username AS Username,d.firstName AS FirstName,d.lastName AS LastName," +
                "d.gender AS Gender, d.dateOfBirth AS BirthDate, a.code AS PostalCode, u.username AS Email, " +
                "p.phoneNumber AS Phone, p.phonePrefix AS PhonePrefix,a.countryId AS Country, u.languageId AS `Language`," +
                "u.userStatusId AS UserStatus, u.userTypeId AS UserType FROM `user` u " +
                "LEFT JOIN useraddress a ON  u.id = a.`userId` " +
                "LEFT JOIN userphone p ON u.`id` = p.`userId` " +
                "LEFT JOIN `userdetail` d ON u.`id` = d.`userId` WHERE u.username = '{0}' ", userName
            );
            var userList = dbService.RunSelect(userInfoSql);

            IList<RegisterUserDto> usersListData = userList.AsEnumerable().Select(row =>
                new RegisterUserDto
                {
                    Id = row.Field<string>("Id"),
                    Username = row.Field<string>("Username"),
                    FirstName = row.Field<string>("FirstName"),
                    LastName = row.Field<string>("LastName"),
                    Gender = row.Field<string>("Gender"),
                    BirthDate = ConversionUtils.ConvertDateToMySqlFormat(row.Field<DateTime?>("BirthDate")),
                    PostalCode = row.Field<string>("PostalCode"),
                    Email = row.Field<string>("Email"),
                    Phone = row.Field<string>("Phone"),
                    PhonePrefix = row.Field<string>("PhonePrefix"),
                    Country = row.Field<string>("Country"),
                    Language = row.Field<int>("Language").ToString(),
                    ReceiveNewsletter = false,
                    UserStatus = row.Field<string>("UserStatus"),
                    UserType = row.Field<string>("UserType"),
                }
            ).ToList();

            if (usersListData.Count != 0)
            {
                return usersListData.First();
            }
            else
            {
                return null;
            }
        }

        public UserLoginReturnDto ForgetPassword(UserLoginDto user)
        {
            //Connect
            dbService.Connect();
            var userStatus = CheckUserAwaitingConfirmation(user.UserName);
            var userObj = GetUserDetails(user.UserName);

            if (userStatus == "Message to check email")
            {
                return new UserLoginReturnDto
                {
                    Id = (userObj?.Id),
                    ErrorCode = (int)StatusCode.UserAwaitingConfirmationNotExp,
                    Message = userStatus,
                    Success = false
                };
            }
            else if (userStatus == "Link sent again")
            {
                return new UserLoginReturnDto
                {
                    Id = (userObj?.Id),
                    ErrorCode = (int)StatusCode.UserAwaitingConfirmationExp,
                    Message = userStatus,
                    Success = false
                };
            }

            if (!user.ForgetPassword || string.IsNullOrEmpty(user.UserName))
            {
                return new UserLoginReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.UserRequestInvalidFormat,
                    Message = "Incorrect Request Format",
                    Success = false
                };
            }


            //Get user details based on user id 
            var dbUser = GetUserDetails(user.UserName);

            if (dbUser == null )
            { return UserDoesNotExist; }

            var email = string.IsNullOrEmpty(dbUser.Username) ? null : dbUser.Email;

            //Change user status to Forget Password
            var success = UpdateUserStatus(dbUser.Id, UserStatus.ForgotPassword);

            //Generate Random Code 
            var randomPassword = RandomGenerator.GenerateRandomPassword(8);

            //Save entry for temp password in  DB
            success = SaveEntryForTempPassword(dbUser.Id, randomPassword);

            //Send Email with Random Generated Code 
            ForgottenPasswordMailTo(dbUser.Username,randomPassword);

            dbService.Disconnect();

            return new UserLoginReturnDto
            {
                Id = user.Id,
                ErrorCode = 0,
                Message = "Email sent successfully",
                Success = true
            };

        }

        private bool SaveEntryForTempPassword(string userId, string randomPassword)
        {
            var tempPasswordStoreSql = string.Format("INSERT INTO `usertemppassword` ( " +
                            "`id`,`userId`,`randomGeneratedPassword`,`expiryDate`,`serviced`)" +
                            " VALUES('{0}','{1}','{2}','{3}',{4})",
                            Guid.NewGuid().ToString(), userId, randomPassword, 
                            ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now.AddHours(1)), 0);

            var result = dbService.RunCommand(tempPasswordStoreSql);

            return result;
        }

        public bool UpdateUserStatus(string userId, UserStatus userStatus)
        {
            var updateUserSql = string.Format("UPDATE `user` SET userStatusId = '{0}' WHERE id = '{1}'",
                EnumUtils.GetEnumDescription(userStatus), userId
                );

            var result = dbService.RunCommand(updateUserSql);
            return result;
        }

        private void ForgottenPasswordMailTo(string userEmail, string randomCode)
        {
            ResourceManager rm = new ResourceManager("Scoachy.Core.EmailText", Assembly.GetExecutingAssembly());
            var emailBody = Mailer.ReplaceTextWithResource("email.html", "@mytext@", rm.GetString("ForgetPassword") + " " + randomCode);
            Mailer.SendMail(userEmail, emailBody );
        }

        private void RegisteredUserMailTo(string userEmail)
        {
            ResourceManager rm = new ResourceManager("Scoachy.Core.EmailText", Assembly.GetExecutingAssembly());
            var emailBody = Mailer.ReplaceTextWithResource("email.html", "@mytext@", rm.GetString("RegistrationComplete"));
            Mailer.SendMail(userEmail, emailBody);
        }

        private RegisterUserReturnDto UserActivation(RegisterUserDto user)
        {
            var valid = new RegisterUserReturnDto();
            var link = Guid.NewGuid().ToString();
            var userlink = "http://" + ConfigurationManager.AppSettings["IP"] + "/account/activation/" + link; //link to be sent to user

            ResourceManager rm = new ResourceManager("Scoachy.Core.EmailText", Assembly.GetExecutingAssembly());
            var emailBody = Mailer.ReplaceTextWithResource("email.html", "@mytext@", rm.GetString("Activation") + userlink + rm.GetString("ActivationLink"));
            Mailer.SendMail(user.Username, emailBody); //sending email to user
            var insertCommand = string.Format(
                "INSERT INTO useractivation(id, userId, link, expired) VALUES('{0}','{1}','{2}','{3}');", Guid.NewGuid().ToString(), user.Id, link, 0);

            valid.Success = dbService.RunCommand(insertCommand); //insert in table useractivation

            return valid;
        }

        public bool UserAfterActivation(string linkId) //called when user clicks on the activation link
        {
            dbService.Connect();
            var linkExpiredCommand = string.Format("UPDATE useractivation SET expired = '1' WHERE link = '" + linkId + "';");
            dbService.RunCommand(linkExpiredCommand);
            var retrieveUserId = string.Format("SELECT userId FROM useractivation WHERE link = '" + linkId + "' LIMIT 1;");
            var userIdData = dbService.RunSelect(retrieveUserId);
            IList<string> userIdList = userIdData.AsEnumerable().Select(row =>
                    row.Field<string>("userId")
            ).ToList();
            string userId = "";
            if (userIdList == null) //check if data retrieved is null
            {
                Console.WriteLine("Error!UserId is null");
            }
            else
            {
                userId = userIdList.FirstOrDefault();
            }
            var retrieveDateCreated = string.Format("SELECT createdOn FROM user WHERE id = '" + userId + "' LIMIT 1;");
            var dateCreatedData = dbService.RunSelect(retrieveDateCreated);
            IList<DateTime> dateCreatedList = dateCreatedData.AsEnumerable().Select(row =>
                    row.Field<DateTime>("createdOn")
            ).ToList();
            DateTime dateCreated = DateTime.Now;
            DateTime dateNow = DateTime.Now;
            if (dateCreatedList == null)
            {
                Console.WriteLine("Error!Date Created is null");
            }
            else
            {
                dateCreated = dateCreatedList.FirstOrDefault();
            }
            int time = Convert.ToInt32(ConfigurationManager.AppSettings["ExpiryTime"]);
            DateTime dateExpired = dateCreated.AddHours(time); //calculate expiry date
            if (dateNow > dateExpired) //check if link has expired
            {
                dbService.Disconnect();
                return true;
            }
            else
            {
                var updateUserCommand = string.Format("UPDATE `user` SET userStatusId = '" + EnumUtils.GetEnumDescription(UserStatus.Active) + "' WHERE id = '" + userId + "';");
                dbService.RunCommand(updateUserCommand);
                dbService.Disconnect();
                return false;
            }

        }

        private UserLoginReturnDto UserFailedAuthentication
        {
            get
            {
                return new UserLoginReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.UserAuthenticationFailed,
                    Message = "User doesn't exist",
                    Success = false
                };
            }
        }

        private UserLoginReturnDto UserDoesNotExist
        {
            get
            {
                return new UserLoginReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.UserDoesNotExist,
                    Message = "User does not exist",
                    Success = false
                };
            }
        }

        private RegisterUserReturnDto UserDuplicateEmail
        {
            get
            {
                return new RegisterUserReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.UserDuplicateEmail,
                    Message = "Email already exists",
                    Success = false
                };
            }
        }

        private RegisterUserReturnDto UserInvalidEmail
        {
            get
            {
                return new RegisterUserReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.InvalidEmail,
                    Message = "Invalid Email",
                    Success = false
                };
            }
        }

        private RegisterUserReturnDto UserInvalidCharacters
        {
            get
            {
                return new RegisterUserReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.InvalidCharacters,
                    Message = "Invalid Characters",
                    Success = false
                };
            }
        }
        private RegisterUserReturnDto UserDuplicatePhone
        {
            get
            {
                return new RegisterUserReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.UserDuplicatePhone,
                    Message = "Phone number already exists",
                    Success = false
                };
            }
        }

        private RegisterUserReturnDto UserEmptyField
        {
            get
            {
                return new RegisterUserReturnDto
                {
                    Id = null,
                    ErrorCode = (int)StatusCode.UserMandatoryFieldEmpty,
                    Message = "One of the mandatory fields is empty",
                    Success = false
                };
            }
        }

        private void InsertUser(){ }
        private void InsertUserEmail() { }
        private void InsertUserPhone() { }
        private void InsertUserAddress() { }
    }
}
