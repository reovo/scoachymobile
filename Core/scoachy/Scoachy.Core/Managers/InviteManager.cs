﻿using Scoachy.Core.Managers.Interfaces;
using System;
using Scoachy.Core.Models.Dto;
using Scoachy.Data;
using Reovo.Lib;
using Scoachy.Core.Models.Invite;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Resources;
using System.Reflection;
using Reovo.Lib.Validators;

namespace Scoachy.Core.Managers
{
    public class InviteManager : IInviteManager
    {
        private DBService dbService;

        public InviteManager()
        {
            dbService = new DBService();
        }

        public InviteContactDto GetFriendList(string userId)
        {
            dbService.Connect();

            var inviteListSql = string.Format(
                "SELECT i.id as id, u.id as userId, u.userName as email, p.phoneNumber as phone, p.phonePrefix as phonePrefix," +
                " s.name as inviteStatus, i.invitedOn as invitedOn, i.acceptedOn as acceptedOn " +
                "FROM invite i " +
                "LEFT JOIN user u ON i.userId = u.id " + 
                "LEFT JOIN userphone p ON p.userId = u.id " +
                "LEFT JOIN invitestatus s ON i.inviteStatusId = s.id " +
                "WHERE i.friendUserId = '{0}'", userId
            );

            var contactListData = dbService.RunSelect(inviteListSql);

            var contactListUpdate = new InviteContactDto
            {
                UserId = userId,
            };

            contactListUpdate.ContactsDto = contactListData.AsEnumerable().Select(row =>
                new ContactDto
                {
                    UserId = row.Field<string>("userId"),
                    Email = row.Field<string>("email"),
                    Phone = row.Field<string>("phone"),
                    PhonePrefix = row.Field<string>("phonePrefix"),
                    InviteStatus = row.Field<string>("inviteStatus"),
                    InvitedOn = ConversionUtils.ConvertDateToMySqlFormat(row.Field<DateTime?>("invitedOn")),
                    AcceptedOn = ConversionUtils.ConvertDateToMySqlFormat(row.Field<DateTime?>("acceptedOn"))
                }
            ).ToList();

            dbService.Disconnect();
            return contactListUpdate;
        }

        public int GetResendInvites(string userId)
        {
            int result = 0;
            try
            {
                dbService.Connect();

                //get list of invites that are pending for this user id
                var inviteListSql = string.Format(
                    "SELECT u.userName as email, d.firstname as firstName, d.lastName as lastName, i.email as inviteEmail " +
                    "FROM invite i " +
                    "LEFT JOIN user u ON i.userId = u.id " +
                    "LEFT JOIN userdetail d ON d.userId = u.id " +
                    "LEFT JOIN invitestatus s ON i.inviteStatusId = s.id " +
                    "WHERE i.userId = '{0}' AND i.inviteStatusId = '{1}'", 
                        userId, 
                        EnumUtils.GetEnumDescription(InviteStatus.Pending)
                );

                var contactListData = dbService.RunSelect(inviteListSql);

                var contactListUpdate = new InviteContactDto
                {
                    UserId = userId,
                };
                contactListUpdate.ContactsDto = contactListData.AsEnumerable().Select(row =>
                    new ContactDto
                    {
                        UserId = row.Field<string>("email"),
                        Email = row.Field<string>("inviteEmail"),
                        FirstName = row.Field<string>("firstName"),
                        LastName  = row.Field<string>("lastName"),
                    }
                ).ToList();

                foreach(ContactDto c in contactListUpdate.ContactsDto)
                {     
                    SendInviteMailTo(c.Email,contactListUpdate.UserId);
                    result++;
                }
            }
            catch(Exception e)
            {
                dbService.LogError(-1, "GetResendInvites", e.Message, dbService.Connection);
                return -1;
            }
            finally
            {
                dbService.Disconnect();
            }
            return result;
        }

        public InviteContactDto GetInviteList(string userId)
        {
            dbService.Connect();

            var inviteListSql = string.Format(
                "SELECT i.id as id, i.userId as userId, i.friendUserId as friendUserId, i.phone as phone, i.phonePrefix as phonePrefix, i.email as email, " +
                " s.name as inviteStatus, i.invitedOn as invitedOn, i.acceptedOn as acceptedOn " +
                "FROM invite i " +
                "LEFT JOIN invitestatus s ON i.inviteStatusId = s.id " +
                "WHERE userId = '{0}'", userId
            );

            var contactListData = dbService.RunSelect(inviteListSql);

            var contactListUpdate = new InviteContactDto {
                UserId = userId,
            };

            contactListUpdate.ContactsDto = contactListData.AsEnumerable().Select(row =>
            new ContactDto
            {
                FriendUserId = row.Field<string>("friendUserId"),
                Email = row.Field<string>("email"),
                Phone = row.Field<string>("phone"),
                PhonePrefix = row.Field<string>("phonePrefix"),
                InviteStatus = row.Field<string>("inviteStatus"),
                InvitedOn = ConversionUtils.ConvertDateToMySqlFormat(row.Field<DateTime?>("invitedOn")),
                AcceptedOn = ConversionUtils.ConvertDateToMySqlFormat(row.Field<DateTime?>("acceptedOn"))
            }).ToList();

            dbService.Disconnect();
            return contactListUpdate;
        }

        public IList<InvitesDto> GetFlatInvites(string userId)
        {
            throw new NotImplementedException();
        }

        public bool ChangeStatus(InviteStatusDto inviteStatus)
        {
            dbService.Connect();
            var changeStatusSql = string.Format("UPDATE `invite` SET inviteStatusId = '{0}' WHERE userId = '{1}' AND friendUserId = '{2}'",
                EnumUtils.GetEnumDescription(InviteStatus.Accepted), inviteStatus.FriendUserId, inviteStatus.UserId
                );

            dbService.RunCommand(changeStatusSql);
            dbService.Disconnect();
            return true;
        }

        public InviteContactDto SendInvites(InviteContactDto contactList)
        {
            InviteContactDto contactsReturnDto = new InviteContactDto();
            dbService.Connect();

            var userId = contactList.UserId;
            contactsReturnDto.UserId = userId;
            contactsReturnDto.ContactsDto = new List<ContactDto>();
            var accountManager = new AccountManager(dbService);
            var user = accountManager.GetUser(userId);

            foreach (var contact in contactList.ContactsDto)
            {
                string friendUserId = string.Empty;
                bool validEntry = true;
                bool sendMail = false;

                if (!string.IsNullOrEmpty(contact.Email))
                {
                    friendUserId = FindFriendIdForEmail(contact.Email);
                    sendMail = true;
                }
                else if (string.IsNullOrEmpty(contact.Email))
                {
                    validEntry = false;
                    friendUserId = string.Empty;
                }

                if (string.IsNullOrEmpty(friendUserId))
                {
                    friendUserId = Guid.NewGuid().ToString();
                }

                if (validEntry && CombinationExists(userId, friendUserId))
                {
                    validEntry = false;
                }

                if (!Validator.Email(contact.Email) || user.Username.ToUpper() == contact.Email.ToUpper())
                {
                    validEntry = false;
                    sendMail = false;
                }
                

                var insertInvite = string.Format("INSERT INTO `invite` (`id`,`userid`,`invitedOn`,`friendUserId`,`email`, " + "" +
                                   "`inviteStatusId`) " +
                                   "VALUES ('{0}', '{1}', '{2}', '{3}','{4}','{5}')",
                                   Guid.NewGuid().ToString(), userId, ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now), friendUserId, contact.Email,
                                   EnumUtils.GetEnumDescription(InviteStatus.Pending)
                                   );

                if (validEntry)
                {
                    try
                    {
                        dbService.RunCommand(insertInvite);                   
                    }
                    catch(Exception e)
                    {
                        dbService.LogError(-1, "SendInvites", e.Message, dbService.Connection);
                    }
                }

                if (sendMail)
                {

                    contactsReturnDto.ContactsDto.Add(
                        new ContactDto
                        {
                            Email = contact.Email
                        }    
                    );
                    SendInviteMailTo(contact.Email,userId);
                }
            }

            dbService.Disconnect();


            return contactsReturnDto;
        }

        private void SendInviteMailTo(string userEmail,string userId)
        {

            var retrieveUsernameSQL = string.Format("SELECT username FROM user WHERE id = '{0}'", userId);
            var usernameData = dbService.RunSelect(retrieveUsernameSQL);
            IList<string> usernameList = usernameData.AsEnumerable().Select(row =>
                    row.Field<string>("username")
            ).ToList();
            string username = "";
            if (usernameList == null) //check if data retrieved is null
            {
                Console.WriteLine("Error!username is null");
            }
            else
            {
                username = usernameList.FirstOrDefault();
            }

            var retrieveFnameSQL = string.Format("SELECT firstName FROM userdetail WHERE userId = '{0}'", userId);
            var FnameData = dbService.RunSelect(retrieveFnameSQL);
            IList<string> FnameList = FnameData.AsEnumerable().Select(row =>
                    row.Field<string>("firstName")
            ).ToList();
            string Fname = "";
            if (FnameList == null) //check if data retrieved is null
            {
                Fname = "null";
            }
            else
            {
                Fname = FnameList.FirstOrDefault();
            }

            var retrieveLnameSQL = string.Format("SELECT lastName FROM userdetail WHERE userId = '{0}'", userId);
            var LnameData = dbService.RunSelect(retrieveLnameSQL);
            IList<string> LnameList = LnameData.AsEnumerable().Select(row =>
                    row.Field<string>("lastName")
            ).ToList();
            string Lname = "";
            if (LnameList == null) //check if data retrieved is null
            {
                Lname = "null";
            }
            else
            {
                Lname = LnameList.FirstOrDefault();
            }
            
            ResourceManager rm = new ResourceManager("Scoachy.Core.EmailText", Assembly.GetExecutingAssembly());
            string text = rm.GetString("DownloadInvite1");
            if (Lname == "null")
            {
                if (Fname == "null")
                {
                    text = " " + text + username;
                }
                else
                {
                    text = " " + text + "<br/>" + Fname + "<br/>" + username + "<br/>";
                }
            }
            else
            {
                text = " " + text + "<br/>" + Fname + "   " + Lname + "<br/>" + username + "<br/>";
            }
            text = text + rm.GetString("DownloadInvite2");
            var emailBody = Mailer.ReplaceTextWithResource("email.html", "@mytext@", text);
            Mailer.SendMail(userEmail, emailBody);
            
        }

        private bool CombinationExists(string userId, string friendUserId)
        {
            var findCombination = string.Format("SELECT Count(id) FROM `invite` WHERE userId = '{0}' AND friendUserId = '{1}'",
                userId, friendUserId
                );

            var count = dbService.RunCount(findCombination);

            return count != 0;
        }

        private string FindFriendIdForPhone(string phone)
        {
            var findPhone = string.Format("SELECT friendUserId FROM `invite` WHERE phone = '{0}'", phone);

            var result = dbService.RunSelect(findPhone);

            var friendId = result.AsEnumerable().Select(row => row.Field<string>("friendUserId")).FirstOrDefault();

            if (friendId == null)
            {
                var findUserId = string.Format("SELECT userId FROM `userphone` WHERE phoneNumber = '{0}'", phone);

                var friendIdResult = dbService.RunSelect(findUserId);

                friendId = friendIdResult.AsEnumerable().Select(row => row.Field<string>("userId")).FirstOrDefault();

                return friendId;
            }

            return friendId;
        }

        private string FindFriendIdForEmail(string email)
        {
            var findEmail = string.Format("SELECT friendUserId FROM `invite` WHERE email = '{0}'", email);

            var result = dbService.RunSelect(findEmail);

            var friendId = result.AsEnumerable().Select(row => row.Field<string>("friendUserId")).FirstOrDefault();

            if (friendId == null)
            {
                var findUserId = string.Format("SELECT id FROM `user` WHERE username = '{0}'", email);

                var friendIdResult = dbService.RunSelect(findUserId);

                friendId = friendIdResult.AsEnumerable().Select(row => row.Field<string>("id")).FirstOrDefault();
                return friendId;
            }

            return friendId;
        }

        #region "Deprecated code"
        //public InviteContactDto SendInvites2(InviteContactDto contactList)
        //{
        //    InviteContactDto contactsReturnDto = new InviteContactDto();

        //    dbService.Connect();

        //    var userId = contactList.UserId;
        //    contactsReturnDto.UserId = userId;
        //    contactsReturnDto.ContactsDto = new List<ContactDto>();

        //    foreach (var contact in contactList.ContactsDto)
        //    {
        //        //TODO: If yes check if user is already using Scoachy, in that case, add user to invite tree as well
        //        var contactPresence = CheckContactExistence(contact,userId);
        //        InviteStatus inviteStatus = InviteStatus.Pending;
        //        string friendUserId;
        //        bool valid = true;

        //        //if the contact is not present generate a new user Id
        //        if (contactPresence == null)
        //        {
        //            friendUserId = Guid.NewGuid().ToString();
        //        }
        //        else if (contactList.UserId == contactPresence.UserId)
        //        {
        //            valid = false;
        //            friendUserId = string.Empty;
        //        }
        //        else //if the contact is already present, use the same friend id 
        //        {
        //            friendUserId = contactPresence.FriendUserId;
        //            contactsReturnDto.ContactsDto.Add(contactPresence);
        //        }

        //        if (contactPresence != null && contactPresence.InviteStatus == EnumUtils.GetEnumDescription(InviteStatus.Registered))
        //        {
        //            inviteStatus = InviteStatus.Registered;
        //        }

        //        var insertInvite = string.Format("INSERT INTO `invite` (`id`,`userid`,`invitedOn`,`friendUserId`,`email`,`phone`, " + "" +
        //            "`inviteStatusId`) " +
        //            "VALUES ('{0}', '{1}', '{2}', '{3}','{4}','{5}','{6}')",
        //            Guid.NewGuid().ToString(), userId, ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now), friendUserId, contact.Email, contact.Phone,
        //            EnumUtils.GetEnumDescription(inviteStatus)
        //            );

        //        if (valid)
        //        {
        //            dbService.RunCommand(insertInvite);
        //        }

        //        Mailer.SendMail(contact.Email);
        //    }

        //    dbService.Disconnect();


        //    return contactsReturnDto;
        //}

        //private ContactDto CheckContactExistence(ContactDto contact, string userId)
        //{
        //    string selectExistingUser;

        //    if (string.IsNullOrEmpty(contact.Email) &&
        //        (contact.Phone != string.Empty && contact.Phone != null))
        //    {
        //        selectExistingUser = string.Format(
        //            "SELECT userId,friendUserId, email,phone, inviteStatusId FROM invite WHERE " +
        //            "phone = '{0}'", contact.Phone);

        //    }
        //    else if (string.IsNullOrEmpty(contact.Phone) &&
        //             (contact.Email != string.Empty && contact.Email != null))
        //    {
        //        selectExistingUser = string.Format(
        //            "SELECT userId,friendUserId, email,phone, inviteStatusId FROM invite WHERE " +
        //            "email = '{0}'", contact.Email);

        //    }
        //    else if (string.IsNullOrEmpty(contact.Phone) && 
        //             string.IsNullOrEmpty(contact.Email))
        //    {
        //        throw new Exception();
        //    }
        //    else
        //    {
        //        selectExistingUser = string.Format(
        //                            "SELECT userId, friendUserId, email,phone, inviteStatusId FROM `invite` WHERE " +
        //                            "phone ='{0}' or email = '{1}'", contact.Phone , contact.Email);
        //    }

        //    var userFound =  dbService.RunSelect(selectExistingUser);


        //    var contactListFound = userFound.AsEnumerable().Select(row =>
        //        new ContactDto
        //        {
        //            UserId = row.Field<string>("userId"),
        //            FriendUserId = row.Field<string>("friendUserId"),
        //            Email = row.Field<string>("email"),
        //            Phone = row.Field<string>("phone"),
        //            InviteStatus = row.Field<string>("inviteStatusId")
        //        }
        //    ).Where(x => x.UserId == userId).ToList();

        //    return contactListFound.FirstOrDefault();
        //}
        #endregion
    }
}
