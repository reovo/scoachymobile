﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core
{
    interface ISchoachyService
    {
        void syncDatabase();
        void checkApplicationStatus();
    }
}
