﻿using Newtonsoft.Json;
using Scoachy.Core.Managers;
using Scoachy.Core.Models.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Scoachy.Core.Models.Scoachy;
using Reovo.Lib.Validators;

namespace scoachy.Controllers
{
    public class ScoachyController : ApiController
    {
        [Route("scoachy/test/{name}")]
        [HttpGet]
        public IHttpActionResult GetTest(string name)
        {
            var serviceMessage = "Scoachy BETA Service is online." + "Parameter Test: " + name.ToUpper() + ": Passed!";
           
            return Ok(serviceMessage);
        }

        [Route("scoachy/globalstats/")]
        [HttpGet]
        public IHttpActionResult GetGlobalStatistics()
        {
            var scoachyManager = new ScoachyManager();
            var globalStats = scoachyManager.GetGlobalStatistics();
            return Ok(globalStats);
        }

        [Route("scoachy/confirmpayment/")]
        [HttpPost]
        public IHttpActionResult ConfirmPayment([FromBody]ConfirmPaymentDto paymentConfirmation)
        {
            var scoachyManager = new ScoachyManager();
            var confirmed = scoachyManager.ConfirmPayment(paymentConfirmation);

            return Ok(confirmed);
        }

        [Route("scoachy/sendversion/{AppVersion}")]
        [HttpGet]
        public IHttpActionResult GetSendVersion(decimal AppVersion)
        {
            var scoachyManager = new ScoachyManager();
            var statusMsg = scoachyManager.SendVersion(AppVersion);

            return Ok(statusMsg);
        }

        [Route("scoachy/sync/{userId}/{dbVersion}")]
        [HttpGet]
        public IHttpActionResult GetSyncData(string userId,int dbVersion)
        {
            var scoachyManager = new ScoachyManager();
            var syncData = scoachyManager.GetSyncData(userId,dbVersion);
            return Ok(syncData);
        }

        [Route("scoachy/report/")]
        [HttpPost]
        public IHttpActionResult ReportIssue([FromBody] ReportIssueDto report)
        {
            var scoachyManager = new ScoachyManager();
            var result = scoachyManager.ReportIssue(report);
            return Ok(result);
        }

        [Route("scoachy/settings/")]
        [HttpGet]
        public IHttpActionResult GetGlobalSettings()
        {
            var scoachyManager = new ScoachyManager();
            var result = scoachyManager.GetSettings();
            return Ok(result);
        }

        [Route("scoachy/stats/")]
        [HttpGet]
        public IHttpActionResult Stats()
        {
            var scoachyManager = new ScoachyManager();
            Stats JSONData = scoachyManager.GetStats();
            var JSONWebServicesData = GetMethods();
            JSONData.paths = JSONWebServicesData.OrderBy(x => x.RelativePath).ToList();
            return Ok(JSONData);

        }

        public IEnumerable<HelpMethod> GetMethods()
        {
            // get the IApiExplorer registered automatically
            IApiExplorer ex = this.Configuration.Services.GetApiExplorer();

            // loop, convert and return all descriptions
            return ex.ApiDescriptions
                // ignore self
                .Where(d => d.ActionDescriptor.ControllerDescriptor.ControllerName != "ApiMethod")
                .Select(d =>
                {
                    // convert to a serializable structure
                    return new HelpMethod
                    {
                        //Method = d.HttpMethod.ToString(),
                        RelativePath = d.RelativePath,
                    };
                });
        }

        public class HelpMethod
        {
            //public string Method { get; set; }
            public string RelativePath { get; set; }
        }
    }
}
