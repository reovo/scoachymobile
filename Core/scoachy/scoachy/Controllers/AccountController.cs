﻿using Reovo.Lib;
using System.Configuration;
using Scoachy.Core.Managers;
using Scoachy.Core.Models.Dto;
using Scoachy.Core.Models.User;
using System.Web.Http;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Description;
using Newtonsoft.Json;
using System.Web.Hosting;
using System;

namespace scoachy.Controllers
{
    public class AccountController : ApiController
    {
        [Route("account/register/")]
        [HttpPost]
        public IHttpActionResult RegisterUser([FromBody]RegisterUserDto user)
        {
            var accountManager = new AccountManager();
            var returnUser = accountManager.RegisterUser(user);
            return Ok(returnUser);
        }


        [Route("account/login/")]
        [HttpPost]
        public IHttpActionResult LoginUser([FromBody]UserLoginDto user)
        {
            var accountManager = new AccountManager();
            var userLogin = accountManager.UserLogin(user);
            return Ok(userLogin);
        }

        [Route("account/forgetpassword/")]
        [HttpPost]
        public IHttpActionResult ForgetPassword([FromBody]UserLoginDto user)
        {
            var accountManager = new AccountManager();
            var userForget = accountManager.ForgetPassword(user);
            return Ok(userForget);
        }

        [Route("account/resendlink/{userId}")]
        [HttpGet]
        public IHttpActionResult ResendLink(string userId)
        {
            var accountManager = new AccountManager();
            var result = accountManager.ResendLink(userId);
            return Ok(result);
        }

        [Route("account/update/")]
        [HttpPost]
        public IHttpActionResult UpdateUser([FromBody]RegisterUserDto user)
        {
            var accountManager = new AccountManager();
            var updatedUser = accountManager.UpdateUser(user);
            return Ok(updatedUser);
        }

        [Route("account/activation/{linkId}")]
        [HttpGet]
        public HttpResponseMessage ActivateUser(string linkId)
        {
            var accountManager = new AccountManager();
            var expiryCheck = accountManager.UserAfterActivation(linkId);

            string path = HostingEnvironment.ApplicationPhysicalPath;
            var response = new HttpResponseMessage();

            try
            {
                if (expiryCheck == false) //check if link is expired
                {
                    path = path  + ConfigurationManager.AppSettings["ActivationRedirectUrl"];
                }
                else
                {
                    path = path + ConfigurationManager.AppSettings["ExpiredRedirectUrl"];
                }
                response.Content = new StringContent(File.ReadAllText(path));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            }
            catch (Exception e)
            {
                response.Content = new StringContent(e.Message);
                return response;
            }
            return response;
        }
    }
}
