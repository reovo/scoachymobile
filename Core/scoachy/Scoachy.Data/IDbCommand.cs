﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Data
{
    interface IDbCommand
    {
        void Connect();
        void Disconnect();
        bool RunCommand(string parsedSql);
        void Reconnect();
    }
}
