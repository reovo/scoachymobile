﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace Reovo.Lib
{
    public class Mailer
    {
        public static void SendMail(string email, string body)
        {
            if (string.IsNullOrEmpty(email))
                return;

            MailMessage mail = new MailMessage("scoachymanager@gmail.com", email);
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("scoachymanager@gmail.com", "R30v0123@");
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            mail.Subject = "Scoachy";
            mail.IsBodyHtml = true;
            mail.Body = body;
            client.Send(mail);
        }

        public static string ReplaceTextWithResource(string htmlFile, string textToReplace, string newText)
        {
            string text;
            using (var streamReader = new StreamReader(HostingEnvironment.ApplicationPhysicalPath + "\\bin\\EmailFormats\\"+ htmlFile, Encoding.UTF8))
            {
                text = streamReader.ReadToEnd();
            }
            text = text.Replace(textToReplace, newText);
            return text;
        }
    }
}
