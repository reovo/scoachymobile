﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reovo.Lib
{
    public class ConversionUtils
    {
        public static string ConvertDateToMySqlFormat(DateTime? dateTime)
        {
            if (dateTime == null)
            {
                return null;
            }
            else
            {
                var d =  ((DateTime)dateTime).ToString("yyyy-MM-dd HH:mm:ss");
                return d;
            }
            
        }
    }
}
