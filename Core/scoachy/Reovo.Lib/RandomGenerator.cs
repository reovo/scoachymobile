﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reovo.Lib
{
    public class RandomGenerator
    {
        public static string GenerateRandomPassword(int length)
        {
            //Range (A-Z), (a-z), (0-9)
            Random rnd = new Random();
            byte b;
            List<byte> randomlyGenerated = new List<byte>();

            for (int x = 0; x < length; x++)
            {
                var r = rnd.Next(0, 3);
                if (r == 0)
                {
                    b = (byte) rnd.Next(65, 91);  //A-Z
                }
                else if (r == 1)
                {
                    b = (byte) rnd.Next(97, 123); //a-z
                }
                else
                {
                    b = (byte) rnd.Next(48, 58); //0-9
                }
                randomlyGenerated.Add(b);
            }

            var randomString = Encoding.ASCII.GetString(randomlyGenerated.ToArray());

            return randomString;
        }

        public static int GenerateRandomNumber(int length)
        {
            return 0;
        }
    }
}
