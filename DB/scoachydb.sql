/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.7.21-0ubuntu0.16.04.1 : Database - scoachybeta
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`scoachybeta` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `scoachybeta`;

/*Table structure for table `activity` */

DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `name` varchar(64) NOT NULL,
  `activityNumber` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `activitycategory` */

DROP TABLE IF EXISTS `activitycategory`;

CREATE TABLE `activitycategory` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `categoryNumber` int(11) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `localeid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `answer` */

DROP TABLE IF EXISTS `answer`;

CREATE TABLE `answer` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `photoid` varchar(64) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `ratingtypeid` varchar(64) DEFAULT NULL,
  `activityCategoryId` varchar(64) DEFAULT NULL,
  `localeid` varchar(64) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ratingtypeid` (`ratingtypeid`),
  CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`ratingtypeid`) REFERENCES `ratingtype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `answerresult` */

DROP TABLE IF EXISTS `answerresult`;

CREATE TABLE `answerresult` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `userId` varchar(64) DEFAULT NULL,
  `answerId` varchar(64) DEFAULT NULL,
  `lastAnsweredDate` datetime DEFAULT NULL,
  `questionId` varchar(64) DEFAULT NULL,
  `friendUserId` varchar(64) DEFAULT NULL,
  `version` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `answerId` (`answerId`),
  CONSTRAINT `answerresult_ibfk_1` FOREIGN KEY (`answerId`) REFERENCES `answer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `attribute` */

DROP TABLE IF EXISTS `attribute`;

CREATE TABLE `attribute` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `name` varchar(64) DEFAULT NULL,
  `localeid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `country` */

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonePrefix` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;

/*Table structure for table `dbupdates` */

DROP TABLE IF EXISTS `dbupdates`;

CREATE TABLE `dbupdates` (
  `id` varchar(64) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `sqltext` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `emotion` */

DROP TABLE IF EXISTS `emotion`;

CREATE TABLE `emotion` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `error` */

DROP TABLE IF EXISTS `error`;

CREATE TABLE `error` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) DEFAULT NULL,
  `origin` varchar(512) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `errorDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6225 DEFAULT CHARSET=utf8;

/*Table structure for table `eventtype` */

DROP TABLE IF EXISTS `eventtype`;

CREATE TABLE `eventtype` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `feedback` */

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `userid` varchar(64) DEFAULT NULL,
  `localeid` varchar(64) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `feedbackquestion` */

DROP TABLE IF EXISTS `feedbackquestion`;

CREATE TABLE `feedbackquestion` (
  `feedbackquestionid` varchar(64) NOT NULL DEFAULT 'uuid()',
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`feedbackquestionid`),
  CONSTRAINT `feedbackquestion_ibfk_1` FOREIGN KEY (`feedbackquestionid`) REFERENCES `feedback` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `invite` */

DROP TABLE IF EXISTS `invite`;

CREATE TABLE `invite` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `userId` varchar(64) NOT NULL,
  `invitedOn` datetime DEFAULT NULL,
  `acceptedOn` datetime DEFAULT NULL,
  `friendUserId` varchar(64) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `phonePrefix` varchar(5) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `inviteStatusId` varchar(64) DEFAULT NULL,
  `mediaTypeId` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mediaTypeId` (`mediaTypeId`),
  KEY `inviteStatusId` (`inviteStatusId`),
  CONSTRAINT `invite_ibfk_1` FOREIGN KEY (`mediaTypeId`) REFERENCES `mediatype` (`id`),
  CONSTRAINT `invite_ibfk_2` FOREIGN KEY (`inviteStatusId`) REFERENCES `invitestatus` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `invitestatus` */

DROP TABLE IF EXISTS `invitestatus`;

CREATE TABLE `invitestatus` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `invitetree` */

DROP TABLE IF EXISTS `invitetree`;

CREATE TABLE `invitetree` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `userId` varchar(64) DEFAULT NULL,
  `friendUserId` varchar(64) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `issue` */

DROP TABLE IF EXISTS `issue`;

CREATE TABLE `issue` (
  `id` varchar(64) NOT NULL,
  `userId` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `category` varchar(32) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `issueDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `language` */

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `language` varchar(64) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `lifeline` */

DROP TABLE IF EXISTS `lifeline`;

CREATE TABLE `lifeline` (
  `id` varchar(64) DEFAULT 'uuid()',
  `userId` varchar(64) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `eventDate` datetime DEFAULT NULL,
  `photoId` varchar(64) DEFAULT NULL,
  `eventTypeId` varchar(64) DEFAULT NULL,
  `emotionId` varchar(64) DEFAULT NULL,
  `creationDate` datetime DEFAULT NULL,
  KEY `photoId` (`photoId`),
  KEY `lifeline_ibfk_2` (`emotionId`),
  KEY `lifeline_ibfk_3` (`eventTypeId`),
  CONSTRAINT `lifeline_ibfk_1` FOREIGN KEY (`photoId`) REFERENCES `photo` (`id`),
  CONSTRAINT `lifeline_ibfk_2` FOREIGN KEY (`emotionId`) REFERENCES `emotion` (`id`),
  CONSTRAINT `lifeline_ibfk_3` FOREIGN KEY (`eventTypeId`) REFERENCES `eventtype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `locale` */

DROP TABLE IF EXISTS `locale`;

CREATE TABLE `locale` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `languageId` varchar(64) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `translatedText` varchar(1024) DEFAULT NULL,
  `translatedTextDescription` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`,`languageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `mediatype` */

DROP TABLE IF EXISTS `mediatype`;

CREATE TABLE `mediatype` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `mission` */

DROP TABLE IF EXISTS `mission`;

CREATE TABLE `mission` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `missionquestionid` varchar(64) DEFAULT NULL,
  `missionanswerid` varchar(64) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `missionanswerid` (`missionanswerid`),
  CONSTRAINT `mission_ibfk_2` FOREIGN KEY (`missionanswerid`) REFERENCES `missionanswer` (`missionanswerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `missionanswer` */

DROP TABLE IF EXISTS `missionanswer`;

CREATE TABLE `missionanswer` (
  `missionanswerid` varchar(64) NOT NULL DEFAULT 'uuid()',
  `answer` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`missionanswerid`),
  CONSTRAINT `missionanswer_ibfk_1` FOREIGN KEY (`missionanswerid`) REFERENCES `mission` (`missionanswerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `missionquestion` */

DROP TABLE IF EXISTS `missionquestion`;

CREATE TABLE `missionquestion` (
  `missionquestionid` varchar(64) NOT NULL DEFAULT 'uuid()',
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`missionquestionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `model` */

DROP TABLE IF EXISTS `model`;

CREATE TABLE `model` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `name` varchar(64) DEFAULT NULL,
  `photoId` varchar(64) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `photo` */

DROP TABLE IF EXISTS `photo`;

CREATE TABLE `photo` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `name` varchar(128) DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `question` */

DROP TABLE IF EXISTS `question`;

CREATE TABLE `question` (
  `id` varchar(64) NOT NULL,
  `activityId` varchar(64) DEFAULT NULL,
  `attributeId` varchar(64) DEFAULT NULL,
  `activityCategoryId` varchar(64) DEFAULT NULL,
  `orderList` int(11) DEFAULT NULL,
  `ratingTypeId` varchar(64) DEFAULT NULL,
  `question` varchar(512) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `localeid` varchar(64) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `appVersion` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ratingtype` */

DROP TABLE IF EXISTS `ratingtype`;

CREATE TABLE `ratingtype` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `name` varchar(64) DEFAULT NULL,
  `style` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` varchar(64) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `settingsTypeId` varchar(64) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `talentanswer` */

DROP TABLE IF EXISTS `talentanswer`;

CREATE TABLE `talentanswer` (
  `id` varchar(64) DEFAULT NULL,
  `talentVerbId` varchar(64) DEFAULT NULL,
  `userId` varchar(64) DEFAULT NULL,
  `questionId` varchar(64) DEFAULT NULL,
  `value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `talentcategory` */

DROP TABLE IF EXISTS `talentcategory`;

CREATE TABLE `talentcategory` (
  `id` varchar(64) DEFAULT NULL,
  `category` varchar(64) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `talentquestion` */

DROP TABLE IF EXISTS `talentquestion`;

CREATE TABLE `talentquestion` (
  `id` text,
  `question` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `talentselected` */

DROP TABLE IF EXISTS `talentselected`;

CREATE TABLE `talentselected` (
  `id` varchar(64) DEFAULT NULL,
  `verbeId` varchar(64) DEFAULT NULL,
  `userId` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `talentverb` */

DROP TABLE IF EXISTS `talentverb`;

CREATE TABLE `talentverb` (
  `id` varchar(64) DEFAULT NULL,
  `categoryId` varchar(64) DEFAULT NULL,
  `verb` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` varchar(64) NOT NULL DEFAULT 'UUID()',
  `userNumber` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `salt` varchar(256) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `userTypeId` varchar(64) DEFAULT NULL,
  `loginAttempt` int(11) DEFAULT NULL,
  `languageId` int(11) DEFAULT NULL,
  `userStatusId` varchar(64) DEFAULT NULL,
  `mediaTypeId` varchar(64) DEFAULT NULL,
  `deviceToken` varchar(160) DEFAULT NULL,
  `os` varchar(64) DEFAULT NULL,
  `osversion` varchar(64) DEFAULT NULL,
  `appversion` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usernameUnique` (`username`),
  KEY `userid` (`userNumber`),
  KEY `userTypeId` (`userTypeId`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`userTypeId`) REFERENCES `usertype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=711 DEFAULT CHARSET=utf8;

/*Table structure for table `useractivation` */

DROP TABLE IF EXISTS `useractivation`;

CREATE TABLE `useractivation` (
  `id` varchar(64) NOT NULL,
  `userId` varchar(64) NOT NULL,
  `link` varchar(64) DEFAULT NULL,
  `expired` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `useraddress` */

DROP TABLE IF EXISTS `useraddress`;

CREATE TABLE `useraddress` (
  `id` varchar(64) NOT NULL DEFAULT 'UUID()',
  `userId` varchar(64) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `countryId` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `useraddress_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userdetail` */

DROP TABLE IF EXISTS `userdetail`;

CREATE TABLE `userdetail` (
  `id` varchar(64) NOT NULL DEFAULT 'UUID()',
  `userId` varchar(64) DEFAULT NULL,
  `userphotoId` varchar(64) DEFAULT NULL,
  `firstName` varchar(256) DEFAULT NULL,
  `lastName` varchar(256) DEFAULT NULL,
  `dateOfBirth` datetime DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `userdetail_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `useremail` */

DROP TABLE IF EXISTS `useremail`;

CREATE TABLE `useremail` (
  `id` varchar(64) NOT NULL DEFAULT 'UUID()',
  `userId` varchar(64) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `useremail_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `useroption` */

DROP TABLE IF EXISTS `useroption`;

CREATE TABLE `useroption` (
  `id` varchar(64) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `useroptionvalues` */

DROP TABLE IF EXISTS `useroptionvalues`;

CREATE TABLE `useroptionvalues` (
  `id` varchar(64) NOT NULL,
  `userId` varchar(64) DEFAULT NULL,
  `userOptionId` varchar(64) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userphone` */

DROP TABLE IF EXISTS `userphone`;

CREATE TABLE `userphone` (
  `id` varchar(64) NOT NULL DEFAULT 'UUID()',
  `userId` varchar(64) DEFAULT NULL,
  `phoneNumber` varchar(64) DEFAULT NULL,
  `phonePrefix` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `userphone_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userphoto` */

DROP TABLE IF EXISTS `userphoto`;

CREATE TABLE `userphoto` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `userId` varchar(64) DEFAULT NULL,
  `urlId` varchar(64) DEFAULT NULL,
  `url` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `userphoto_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userreport` */

DROP TABLE IF EXISTS `userreport`;

CREATE TABLE `userreport` (
  `id` varchar(64) DEFAULT NULL,
  `userId` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `category` varchar(64) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `issueDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userstatus` */

DROP TABLE IF EXISTS `userstatus`;

CREATE TABLE `userstatus` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `name` varchar(32) DEFAULT NULL,
  `description` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `usertemppassword` */

DROP TABLE IF EXISTS `usertemppassword`;

CREATE TABLE `usertemppassword` (
  `id` varchar(64) DEFAULT NULL,
  `userId` varchar(64) DEFAULT NULL,
  `randomGeneratedPassword` varchar(256) DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `serviced` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `usertransaction` */

DROP TABLE IF EXISTS `usertransaction`;

CREATE TABLE `usertransaction` (
  `id` varchar(64) DEFAULT NULL,
  `userId` varchar(64) DEFAULT NULL,
  `appSource` varchar(20) DEFAULT NULL,
  `transactionDate` datetime DEFAULT NULL,
  `storeTransactionId` varchar(512) DEFAULT NULL,
  `orderId` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `usertype` */

DROP TABLE IF EXISTS `usertype`;

CREATE TABLE `usertype` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `number` int(11) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `values` */

DROP TABLE IF EXISTS `values`;

CREATE TABLE `values` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `valuesquestionid` varchar(64) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `valuesanswerid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `valuesquestionid` (`valuesquestionid`),
  KEY `valuesanswerid` (`valuesanswerid`),
  CONSTRAINT `values_ibfk_1` FOREIGN KEY (`valuesquestionid`) REFERENCES `valuesquestion` (`valuesquestionid`),
  CONSTRAINT `values_ibfk_2` FOREIGN KEY (`valuesanswerid`) REFERENCES `valuesanswer` (`valuesanswerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `valuesanswer` */

DROP TABLE IF EXISTS `valuesanswer`;

CREATE TABLE `valuesanswer` (
  `valuesanswerid` varchar(64) NOT NULL DEFAULT 'uuid()',
  `answer` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`valuesanswerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `valuesquestion` */

DROP TABLE IF EXISTS `valuesquestion`;

CREATE TABLE `valuesquestion` (
  `valuesquestionid` varchar(64) NOT NULL DEFAULT 'uuid()',
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`valuesquestionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `version` */

DROP TABLE IF EXISTS `version`;

CREATE TABLE `version` (
  `dbVersion` int(11) DEFAULT NULL COMMENT 'database version',
  `appVersion` varchar(8) DEFAULT NULL COMMENT 'application version'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `vision` */

DROP TABLE IF EXISTS `vision`;

CREATE TABLE `vision` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `description` varchar(512) DEFAULT NULL,
  `photoid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `wheeloflife` */

DROP TABLE IF EXISTS `wheeloflife`;

CREATE TABLE `wheeloflife` (
  `id` varchar(64) NOT NULL DEFAULT 'uuid()',
  `attributeid` varchar(64) DEFAULT NULL,
  `attributename` varchar(512) DEFAULT NULL,
  `values` int(3) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `questionid` varchar(64) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `userid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
